var nightwatch_config = {
    src_folders : [ "nightwatch/tests" ],
    output_folder : "nightwatch/logs",
    selenium : {
      "start_process" : false,
      "host" : "hub-cloud.browserstack.com",
      "port" : 80
    },

    common_capabilities: {
        'browserstack.user': 'bsuser38392',
        'browserstack.key': 'gF1GWCHbKDNC46g9h4HJ',
        'browserstack.debug': true,
        'browserstack.local': true,
    },

    test_settings: {
      default: {},
      chrome: {
        desiredCapabilities: {
          browser: "chrome"
        }
      },
      firefox: {
        desiredCapabilities: {
          browser: "firefox"
        }
      },
      safari: {
        desiredCapabilities: {
          browser: "safari"
        }
      },
      ie: {
        desiredCapabilities: {
            'os': 'Windows',
            'os_version': '7',
            'browser': 'IE',
            'browser_version': '11.0',
            'resolution': '1024x768'
        }
      }
    }
  };

// Code to support common capabilites
for(var i in nightwatch_config.test_settings){
    var config = nightwatch_config.test_settings[i];
    config['selenium_host'] = nightwatch_config.selenium.host;
    config['selenium_port'] = nightwatch_config.selenium.port;
    config['desiredCapabilities'] = config['desiredCapabilities'] || {};
    for(var j in nightwatch_config.common_capabilities){
      config['desiredCapabilities'][j] = config['desiredCapabilities'][j] || nightwatch_config.common_capabilities[j];
    }
  }

  module.exports = nightwatch_config;

var request = require("request");

module.exports = test => ({...test,
        afterEach : (browser, done) => {
            let status = browser.currentTest.results.failed === 0 ? "completed" : "error"
            let sessionId = browser.capabilities['webdriver.remote.sessionid']
            request({uri: `https://bsuser38392:gF1GWCHbKDNC46g9h4HJ@www.browserstack.com/automate/sessions/${sessionId}.json`, method:"PUT", form:{"status":status,"reason":""}})
            done()
          }
    });


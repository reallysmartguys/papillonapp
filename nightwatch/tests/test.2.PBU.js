const global = require("../nightwatch.global")

const byId = id => `[data-e2b-id="${id}"]`
const byContent = str => `//*[contains(text(),'${str}')]`

module.exports = global({
    'Logging as Héllène, create a folder' : browser => {
        let caseName = "test-"+Date.now()
        browser
            .url('localhost')

            // Login
            .waitForElementVisible(byId("user-2"), 1000)
            .click(byId("user-2"))

            // Check if loged
            .waitForElementVisible(byId("greetings-text"), 1000)
            .assert.containsText(byId("greetings-text"), "Bonjour H. GARCIA")

            // Check defects
            .click(byId('top-tab-defects'))
            .useXpath().waitForElementVisible(byContent("Defaillances actives"), 1000).useCss()
            .waitForElementNotPresent(byId("loading-screen"), 5000)
            .assert.elementPresent(".rt-tr-group")

            // Check case creation
            .click(byId('plus-button'))
            .waitForElementVisible(byId("plus-button-list"), 1000)
            .click(byId('plus-button-list')+" .list-item:first-child")

            .waitForElementVisible(byId('modal'), 1000)
            .setValue(byId('modal')+' input', caseName)
            .click(byId('modal-confirm'))

            .waitForElementVisible('input[name="title"]', 1000)

            // Check basic edit and save
            .assert.attributeEquals(byId("save-case-form"),"disabled","true")

            .setValue('textarea#detection', "Elements de détection ajoutés par HELLENE le ROBOT !")

            .assert.elementNotPresent(byId("save-case-form")+'[disabled="true"]')
            .click(byId("save-case-form"))

            .pause(500)
            .assert.attributeEquals(byId("save-case-form"),"disabled","true")

            // Check files addition
            .setValue("input"+byId("add-file"), "C:\\Users\\hello\\Documents\\images\\chart2.png")
            .waitForElementNotPresent(byId("loading-screen"), 5000)
            .useXpath().waitForElementVisible(byContent("chart2.png"), 1000).useCss()

            // Check adding defect Id
            .click(byId("tabs-blockage"))
            .click(byId('bqvId-button'))
            .setValue(byId("bqvId"), "70")
            .waitForElementVisible(byId('bqvId-list-item-0'), 5000)
            .click(byId('bqvId-list-item-0'))
            .waitForElementNotPresent(byId("loading-screen"), 5000)
            .assert.elementPresent(byId("bqv.nbveh"))

            // Test share
            .click(byId('share-case-button'))
            .waitForElementVisible(byId('modal'), 1000)
            .click(byId('radio-option-warning'))
            .click(byId('modal-confirm'))

            .click(byId("tabs-communication"))
            .assert.valueContains(byId("shared"), "OUI")

            // TODO : More case modifications

            .click(byId('validate-case-form'))
            .useXpath().waitForElementPresent(byContent(caseName), 1000).useCss()



          .end();
      }
  })

import { connect } from "react-redux";

const findMatch = (a,b) => a.some(role => b.includes(role))

const Block = ({ roles, cazeType, authRole, corporateCaze, factoryCaze, cazeShared, shared, children }) => {
    let shouldReturn = true
    if (authRole && !Array.isArray(authRole)) authRole = [authRole]
    if (authRole)
        shouldReturn = findMatch(authRole, roles)
    if (shouldReturn && (corporateCaze || factoryCaze))
        shouldReturn = (corporateCaze && cazeType === "corporate") || (factoryCaze && cazeType === "factory")
    if (shouldReturn && shared)
        shouldReturn =  cazeShared
    return shouldReturn ? children : null
}

export default connect(
    ({ ui, caze }) => ({
        roles: ui.roles,
        cazeType: caze && caze.corporate ? "corporate" : "factory",
        cazeShared : caze && caze.shared
    })
)(Block)

import React from 'react'
import ReactTable from 'react-table'
import styled from "styled-components"
import moment from "moment"

import * as globalConfig from "config"
import {formatName, formatDate} from "tools"
import { Select } from "components/renault-ui"

import 'react-table/react-table.css'

const Icon = styled.span`
    padding-right : 10px;
`

const SeeMore = styled.div`
    font-size: 15px;
    padding: 9px;
    color : #ccc;
    border: 1px solid;
    border-radius: 100%;
    left: 50%;
    transform: translateX(-50%);
    position: absolute;
    cursor : pointer;
    margin-top : 20px;
    &:hover {
        color : black;
    }
`

const Table = styled.div`
    .ReactTable {
        border : none;
        display : block;
    }
    .rt-tr-group .rt-tr{
        cursor : pointer;
        padding : 5px 0px;
        &:hover {
            color : #0076cc;
        }
    }
    .rt-th.-sort-asc {
        box-shadow: inset 0 -3px 0 0 rgba(255,255,255,0.6);
    }
    .rt-thead.-header{
        background-color : #000;
        color : white;
        .rt-tr {
            padding : 5px 0px;
        }
    }

    .rt-thead.-filters{
        background-color : #ffcc33;
        .rt-th {
            padding : 5px 15px;
            overflow : visible !important;
        }
        input {
            background-color : transparent;
            outline : none;
            border : none;
            border-bottom : 1px solid rgba(0,0,0,1);
            padding : 5px 0;
            font-family : "Read", monospace;
            &:hover {
                border-bottom : 1px solid rgba(0,0,0,1);
            }
            &:focus {
                border-bottom : 1px solid rgba(0,0,0,1);
            }
        }
    }

    .rt-table {
        overflow : visible !important;
    }

    .ReactTable .rt-thead.-header .rt-th{
        &:after {
            content: "\\E9FD";
            font-family: 'icomoon' !important;
            font-size : 15px;
            position: absolute;
            right: 10px;
            display: block;
            top: 7px;
        }
    }

    .ReactTable .rt-thead .rt-th.-sort-asc{
        box-shadow : none;
        &:after {
            content: "\\E9FD";
            font-family: 'icomoon' !important;
            font-size : 15px;
            position: absolute;
            right: 10px;
            display: block;
            top: 7px;
        }
    }

    .ReactTable .rt-thead .rt-th.-sort-desc{
        box-shadow : none;
        &::after {
            content: "\\e9ff";
            font-family: 'icomoon' !important;
            font-size : 15px;
            position: absolute;
            right: 10px;
            display: block;
            top: 7px;
        }
    }
`

class CaseTable extends React.Component {
    state = {
        seeAll: false
    }

    render() {
        const { cases = [], navigateTo, shortView, view } = this.props
        const { seeAll } = this.state


        const columns = [
            { accessor: 'id', Header: 'Dossier n°' },
            { id: "creation", Header: 'Création', accessor: row => formatDate(row.createdDate, "DD/MM/YYYY") },
            { accessor: 'title', Header: 'Libellé' },
            { accessor: 'author', Header: 'PBU', Cell: ({ value }) => <span>{formatName(value)}</span> },
            {
                id: "classification",
                Header: 'Classification',
                accessor: "classification",

                Cell: ({ value }) => {
                    const { label, icon } = globalConfig.classifications(value)
                    return <span><Icon className={icon} />{label}</span>
                },

                filterMethod: (filter, row) => filter.value === "all" || filter.value === row[filter.id],

                Filter: ({ filter, onChange }) =>
                    <Select
                        onChange={value => onChange(value)}
                        value={filter ? filter.value : "all"}
                        ultraLight
                        items={[
                            { label: "Tous", value: "all" },
                            ...[...new Set(cases.map(caze => caze.classification))].map(globalConfig.classifications)
                        ]} />
            },

            {
                id: "status",
                Header: 'Statut',
                accessor: "status",

                Cell: ({ value }) => globalConfig.status(value).label,

                filterMethod: (filter, row) => filter.value === "all" || filter.value === row[filter.id],

                Filter: ({ filter, onChange }) =>
                    <Select
                        onChange={value => onChange(value)}
                        value={filter ? filter.value : "all"}
                        ultraLight
                        items={[
                            { label: "Tous", value: "all" },
                            ...[...new Set(cases.map(caze => caze.status))].map(globalConfig.status)
                        ]} />
            },
        ];

        if(view === 'PBC') columns.splice(3,1,{ accessor: 'assignee', Header: 'PBC', Cell: ({ value }) => <span>{formatName(value)}</span> })

        return (
            <Table>
                <ReactTable
                    minRows={5}
                    filterable={true}
                    className="-striped -highlight"
                    data={cases}
                    columns={columns}
                    pageSize={seeAll && shortView ? 100 : shortView}
                    showPagination={false}
                    defaultSorted={[{ id: "id", desc: false }]}

                    defaultFilterMethod={(filter, row, column) => {
                        const id = filter.pivotId || filter.id
                        return row[id] !== undefined ?
                            String(row[id]).match(new RegExp(filter.value, 'i') ) : true
                    }}

                    getTdProps={(state, rowInfo, column, instance) => ({
                            onClick: (e, handleOriginal) => {
                                rowInfo && navigateTo(`/cases/${rowInfo.row.id}`)
                                if (handleOriginal) {
                                    handleOriginal()
                                }
                            }
                        }
                    )}
                />
                {cases.length > shortView && shortView && <SeeMore onClick={e => this.setState({ seeAll: !seeAll })} className={seeAll ? "icon-ICON_ARROW_06" : "icon-ICON_ARROW_08"} />}
            </Table>
        )
    }
}


export default CaseTable;

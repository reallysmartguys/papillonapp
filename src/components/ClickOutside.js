import React from 'react'

export default class extends React.PureComponent{
    componentDidMount() {
        document.addEventListener('click', this.trigger, false);
    }
    componentWillUnmount() {
        document.removeEventListener('click', this.trigger, false);
    }

    trigger = event => {
        if (this.div && !this.div.contains(event.target)) {
            this.props.trigger();
        }
    }

    render(){
        return <div ref={div => this.div = div}>
                    {this.props.children}
                </div>
    }
}
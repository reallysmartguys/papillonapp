import React from "react"
import { connect } from "react-redux";
import { push } from 'react-router-redux';

const Link = ({push, to, children}) => (
    <a style={{cursor : "pointer"}} onClick={()=>push(to)}>
        {children}
    </a>
)

export default connect(null, { push })(Link)

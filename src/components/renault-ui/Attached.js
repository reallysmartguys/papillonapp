import React from 'react';
import styled from "styled-components"
import { DropDown } from "./index"

import { connect } from 'react-redux'
import { uploadFile, deleteFile } from "state/ducks/caze"
import { toggleModal } from "state/ducks/ui"

const config = {
    fileIcons: {
        jpg: 'icon-ICON_MEDIA_01',
        jpeg: 'icon-ICON_MEDIA_01',
        png: 'icon-ICON_MEDIA_01',
        txt: 'icon-ICON_MEDIA_04',
        pdf: 'icon-ICON_CUSTOMBOOKLET_04',
        avi: 'icon-ICON_PLAYER_01',
        xxx: 'icon-ICON_CUSTOMBOOKLET_02'
    }
}

const openInNewTab = url => {
    var win = window.open(url, '_blank');
    win.focus();
}

const FileList = styled.div`
    width : 100%;
`
const FileIcon = styled.div`
    position : relative;
    width: 100%;
    display: flex;
    border: 1px solid #f0f0f0;
    box-sizing: border-box;
    align-items: center;
    justify-content: center;
    font-size : 40px;
    &::after {
        content : "";
        display : block;
        padding-bottom : 100%;
    }
    &>div {
        width : 100%;
        height : 100%;
        position : absolute;
    }
`
const Menu = styled.div`
    display : flex;
    flex-direction : row;
    padding : 5px;
`

const MenuIcon = styled.div`
    cursor : pointer;
    position : relative;
    width: 100%;
    padding : 5px;
    font-size : 20px;
    color : #ccc;
    transition : .1s;
    &:hover {
        color : black;
    }
`
const FileIconContainer = styled.div`
    padding : 5px;
    width : 20%;
    max-width : 100px;
    text-align : center;
    cursor : pointer;
    display : inline-block;
    vertical-align : top;
    color: inherit;
    text-decoration: none;
    &:hover ${FileIcon} {
        border-color : #ffcc33;
        background-color : #f0f0f0;
    }
`

const AddFileIconContainer = FileIconContainer.extend`
    cursor : pointer;
    position: relative;
    &:hover ${FileIcon} {
        border-color : #cccccc;
        background-color : #f0f0f0;
    }
`

const FileName = styled.div`
    margin-top : 10px;
    font-size : 10px;
    opacity : .5;
    overflow: hidden;
    text-overflow: ellipsis;
`

const Container = styled.div`
   position : relative;
`
const Label = styled.label`
   font-size : 14px;
   margin-bottom :5px;
   display : block;
`

const Attached = ({ files, caseId, name, label, uploadFile, deleteFile, disabled, toggleModal, group = "" }) => {

    const deleteFileMethod = id => {
        toggleModal({
            show: true,
            type: "default",
            props: {
                title: "Supprimer",
                description: "Etes-vous sûr de vouloir supprimer ce fichier ?",
                onConfirm: () => deleteFile(caseId, id)
            }
        });
    };

    const filteredFiles = group ? files.filter(file => file.group === group) : files

    return (
    <Container id={name}>
        <Label for={name}>{label}</Label>
        <FileList>
            <input
                style={{ display: "none" }}
                ref={inputFile => this.inputFile = inputFile}
                data-e2b-id = "add-file"
                type="file"
                onChange={e => {
                    let fileToUpload = e.target.files[0]
                    this.inputFile.value = ""
                    if(fileToUpload) uploadFile(caseId, fileToUpload, group)
                }
            }/>
            {!disabled && <AddFileIconContainer onClick={e => this.inputFile.click()}>
                <FileIcon><span className="icon-ICON_MENU_15" /></FileIcon>
                <FileName>Ajouter</FileName>
            </AddFileIconContainer>}
            {filteredFiles && filteredFiles.map((file, id) => (
                <FileIconContainer data-e2b-id={file.name} key={id} target="_blank">

                    <input
                        style={{ display: "none" }}
                        ref={inputFile => this["inputFile" + id] = inputFile}
                        type="file"
                        onChange={e => {
                            let fileToUpload = e.target.files[0]
                            this["inputFile" + id].value = ""
                            if(fileToUpload) uploadFile(caseId, fileToUpload, group, file.id)
                        }
                    }/>

                    <DropDown top style={{ width: "100%" }} button={<div>
                        <FileIcon><span className={config.fileIcons[file.extension] || 'icon-ICON_CUSTOMBOOKLET_02'} /></FileIcon>
                        <FileName>{file.name}</FileName>
                    </div>}>
                        <Menu>
                            <MenuIcon className='icon-ICON_MENU_11' onClick={() => openInNewTab(`/api/files?caseId=${caseId}&name=${file.name}&type=${file.type}`)} />
                            {!disabled && <MenuIcon className='icon-ICON_COMMUNICATION_03' onClick={e => this["inputFile" + id].click()} />}
                            {!disabled && <MenuIcon className='icon-ICON_EDIT_03' onClick={e => deleteFileMethod(file.id)} />}
                        </Menu>
                    </DropDown>
                </FileIconContainer>
            ))}
        </FileList>
    </Container>
)}

export default connect(
    ({ caze, defects: { defectsId } }) => ({
        caze,
    }),
    {
        uploadFile,
        deleteFile,
        toggleModal
    },
)(Attached)

import React, { Component } from 'react';
import styled, { keyframes } from "styled-components"
import { Scrollbars } from 'react-custom-scrollbars';
import Input from "./Input"
import debounce from "lodash.debounce"
import ClickOutside from 'components/ClickOutside'

const fadeInAnimation = keyframes`
    0% {
    transform : translateY(-10px);
    opacity : 0;
    }
    100% {
        transform : translateY(0);
    opacity : 1;
    }
`

const Container = styled.div`
   position : relative;
   text-align : left;
`

const AutoCompleteText = styled.div`
   flex : 1;
   padding : 10px 10px 10px 10px;
   overflow : hidden;
   white-space: nowrap;
   text-overflow : ellipsis;
`

const AutoCompleteIcon = styled.div`
   position : absolute;
   cursor : pointer;
   right : 5px;
   top : 10px;
   padding : 0 10px;
   transition : .2s;
   opacity : .3;
`

const AutoCompleteButton = styled.div`
   box-sizing : border-box;
   transition : .2s;
   &:hover {
       ${AutoCompleteIcon} {
           ${props => !props.disabled && "opacity : 1 ;"}
       }
   }
   &.UL {
       background-color : transparent;
       color : rgba(0,0,0,.5);
       border : none;
       border-bottom : 1px solid;
       &:hover {
        background-color : transparent;
        color : rgba(0,0,0,1);
       }
       ${AutoCompleteText} {
           padding : 4px 0;
       }
       ${AutoCompleteIcon} {
            color : inherit;
        }
   }
   ${props => props.disabled && `
    background-color : #ebebe4 !important;
    color : rgb(84,84,84)`}
`

const AutoCompleteMenu = styled.div`
   position : absolute;
   box-sizing : border-box;
   width : 100%;
   top: calc(100% + 5px);
   border : 1px solid #e1e1e1 ;
   animation: .3s ${fadeInAnimation};
   z-index : 50;
`
const SelectMenuItem = styled.div`
   box-sizing : border-box;
   padding : 10px;
   font-size : 16px;
   background-color : #ffffff ;
   transition : .2s;
   cursor : pointer;
   overflow : hidden;
   text-overflow : ellipsis;
   &:hover {
       background-color : #f6f6f6 ;
   }
`

const Label = styled.label`
   font-size : 14px;
   margin-bottom :5px;
   display : block;
`

class AutoCompleteList extends React.Component {
    shouldComponentUpdate(nprops) {
        const { items = [] } = this.props
        // Renders only if the list has a diferent number of elements and starts by diferent values
        return nprops.items.length !== items.length || items[0].value !== nprops.items[0].value
    }
    render() {
        const { listElement, name } = this.props
        return this.props.items.map((item, id) => <SelectMenuItem key={item.value} data-e2b-id={name+"-list-item-"+id} onClick={() => this.props.onSelect(item)} > {listElement ? <div>{listElement(item.object)}</div> : item.label}</SelectMenuItem>)
    }
}

export class AutoComplete extends Component {

    constructor(props) {
        super(props);
        this.state = {
            open: false,
            value: "",
            lastSelected: this.props.value || ""
        }
        this.debounceOnChange = debounce((value) => this.props.onChange(value), this.props.debounce || 300)
    }

    componentDidMount() {
        this.setState({ value: this.props.value || "" })
    }

    componentDidUpdate(lprops) {
        if (lprops.value !== this.props.value) this.setState({ value: this.props.value })
    }

    handleClick = e => {
        if (this.state.open) {
            this.setState({ open: false })
            this.handleChange(this.state.lastSelected)
        }
    }

    handleSelect = item => {
        this.props.onSelect && this.props.onSelect(item.value)
        this.handleChange(item.label)
        this.setState({ open: false, lastSelected: item.label })
    }

    handleEmpty = () => {
        if (this.props.disabled) return
        this.handleSelect({ label: "", value: null })
    }

    handleChange = value => {
        this.setState({ value })
        this.debounceOnChange(value)
    }

    handleFirstClick = () => {
        this.setState({ open: true })
    }

    render() {
        let { items = [], light, ultraLight, name, disabled, type, inputProps, listElement } = this.props
        const cleanItems = items.map(item => typeof item === "object" ? item : { label: item, value: item })

        return (
            <ClickOutside trigger={this.handleClick}>
                <Container>
                    <AutoCompleteButton disabled={disabled} light={light} data-e2b-id={name+"-button"} className={ultraLight ? "UL" : null}>
                        <Input
                            type={type}
                            autocomplete={"off"}
                            disabled={disabled}
                            fullWidth
                            data-e2b-id = {name}
                            value={this.state.value}
                            onChange={this.handleChange}
                            onClick={this.handleFirstClick}
                            {...inputProps}
                        />
                        {!disabled && <AutoCompleteIcon className="icon-ICON_EDIT_05" onClick={this.handleEmpty} />}
                    </AutoCompleteButton>
                    {!disabled && this.state.open && !!items.length && <AutoCompleteMenu  data-e2b-id={name+"-list"}>
                        <Scrollbars autoHeight autoHeightMax={300}>
                            <AutoCompleteList onSelect={this.handleSelect} items={cleanItems} name={name} {...{ listElement }} />
                        </Scrollbars>
                    </AutoCompleteMenu>}
                </Container>
            </ClickOutside>
        );
    }
}

export default ({ label, style, ...rest }) => <div style={style}>
    {label && <Label>{label}</Label>}
    <AutoComplete {...rest} />
</div>

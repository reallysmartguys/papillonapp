import React from "react";
import DatePicker from "react-datepicker"
import moment from "moment"
import styled from "styled-components"

import 'react-datepicker/dist/react-datepicker.css';

const InputElement = styled(DatePicker)`
    width : 100%;
    box-sizing : border-box;
    font-size : 16px;
    padding : 10px;
    border : 1px solid #cccccc ;
    outline : none;
    display : block;
    font-family : "Read", monospace;
    &:focus {
        border-color : #ffcc33 ;
    }
`

const HelperText  = styled.div`
    position : absolute;
    bottom : -15px;
    font-size : 10px;
    color : #cccccc ;
`

const Icon  = styled.div`
    position : absolute;
    bottom : 3px;
    font-size : 25px;
    right : 10px
`

const Container = styled.div`
    position : relative;
    &.success {
        ${InputElement} {
            border : 1px solid #12AD2B ;
        }
        ${HelperText} {
            color : #12AD2B ;
        }
    }
    &.error {
        ${InputElement} {
            border : 1px solid #c80e0e ;
        }
        ${HelperText} {
            color : #c80e0e ;
        }
    }
    .react-datepicker-popper {
        z-index : 150;
    }
    .react-datepicker-wrapper, & .react-datepicker__input-container {
        width : 100%;
    }
    .react-datepicker {
        border-radius : 0;
    }
`

const Label  = styled.label`
    font-size : 14px;
    margin-bottom :5px;
    display : block;
`

export default class DatePickerComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            selectedDate: this.props.value ? moment(this.props.value) : null
        }
    }
    componentDidUpdate = (lprops) => {
        if (this.props.value !== lprops.value) this.setState({ selectedDate: this.props.value ? moment(this.props.value) : null })
    }
    selectDate = (date) => {
        const correctedDate = moment(date).get("hour") === 0 ? moment(date).set("hour", 12) : moment(date)
        console.log(moment.utc(correctedDate).toISOString())
        this.props.onChange(moment.utc(correctedDate).toISOString())
    }
    render() {
        const { name, label, helperText, disabled, dateFormat,showTimeSelect, value} = this.props

        const localValue = moment.utc(value).local().format(dateFormat || !!showTimeSelect ? "DD/MM/YYYY HH:mm" : "DD/MM/YYYY")

        return (
            <Container>
                <Label>{label}</Label>

                <InputElement
                    disabled={disabled}
                    id={name}
                    onChange={this.selectDate}
                    selected={this.state.selectedDate}
                    showTimeSelect={showTimeSelect}
                    shouldCloseOnSelect={!showTimeSelect}
                    value={!!value && localValue}
                />
                <label htmlFor={name}><Icon><span className="icon-ICON_TIME_01"/></Icon></label>
                {helperText && <HelperText>{helperText}</HelperText>}
            </Container>
        );
    }
}

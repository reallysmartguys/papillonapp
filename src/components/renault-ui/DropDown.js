import React, { Component } from 'react';
import styled, { keyframes } from "styled-components"
import ClickOutside from 'components/ClickOutside'

const fadeInAnimation = keyframes`
    0% {
        transform : translateY(-10px);
        opacity : 0;
    }
    100% {
        transform : translateY(0) scale(1);
        opacity : 1;
    }
`

const pos = 10

const DropDownMenu = styled.div`
    display : flex;
    flex-direction : column;
    position : absolute;
    ${ props => props.top ? "bottom" : "top"} : calc( 100% + 20px );
    border : 1px solid #ccc;
    background-color : white;
    z-index : 200;
    animation: .3s ${fadeInAnimation};
    right : ${props => props.right ? "0px" : "initial"};
    min-width : 100%;
    &:before{
        content: '';
        display: block;
        width: 0;
        height: 0;
        position: absolute;
        border-${ props => props.top ? "top" : "bottom"}: ${pos}px solid #ccc;
        ${props => props.right ? "right" : "left"} : ${props => props.parentWidth / 2 - pos}px;
        ${ props => props.top ? "bottom" : "top"}: ${-pos}px;
        border-right: ${pos}px solid transparent;
        border-left: ${pos}px solid transparent;
    }
    &:after{
        content: '';
        display: block;
        width: 0;
        height: 0;
        position: absolute;
        border-${ props => props.top ? "top" : "bottom"}: ${pos}px solid white;
        border-left: ${pos}px solid transparent;
        ${props => props.right ? "right" : "left"} : ${props => props.parentWidth / 2 - pos}px;
        ${ props => props.top ? "bottom" : "top"}: ${-pos + 1}px;
        border-right: ${pos}px solid transparent;
    }
`

const Container = styled.div`
    display : inline-block;
    position : relative;
`

const Title = styled.div`
    padding : 10px 0;
    text-align : center;
    background-color : #333;
    color : white;
    width : 100%;
`

class DropDown extends Component {
    state = {
        open: false
    }
    componentDidUpdate(lprops, lstate){
        if(lstate.open && !this.state.open && this.props.onClose) this.props.onClose()
    }
    onClickMenu = e => {
        !this.props.stayOpen && this.setState({ open: false })
    }
    onToggleDropDown = e => {
        this.setState({ open: !this.state.open })
    }
    render() {
        const { button, children, right, top, title, style } = this.props
        const { open } = this.state
        return (
            <ClickOutside trigger={() => this.setState({ open: false })}>
                <Container style={style} innerRef={container => this.container = container} >
                    {React.cloneElement(button, { onClick: this.onToggleDropDown })}
                    {open && <DropDownMenu onClick={this.onClickMenu} right={right} top={top} parentWidth={this.container.offsetWidth}>
                        {title && <Title>{title}</Title>}
                        {children}
                    </DropDownMenu>}
                </Container>
            </ClickOutside>
        );
    }
}

export default DropDown;

import React from 'react';
import styled from "styled-components"

const InputElement = styled.input`
   width : ${props => props.fill === false ? "auto" : "100%"};
   box-sizing : border-box;
   font-size : 16px;
   padding : 10px;
   border : 1px solid #cccccc ;
   outline : none;
   font-family : "Read", monospace;
   ${props => props.inline && "padding-left : 65px;"}
   &:focus {
        border-color : #ffcc33 ;
   }
   ${props => {
        if (props.highLight)
            return `background-color : #ffe083;
                    border-color : #ffcc33;`
        if (props.green)
            return `background-color : #c8ffd1;
                    border-color : #229222;`
        if (props.red)
            return `background-color : #ffccd0;
                    border-color : #d02433;`

    }}
`

const HelperText = styled.div`
   position : absolute;
   bottom : -15px;
   font-size : 10px;
   color : #cccccc ;
`

const RightIcon = styled.div`
    position: absolute;
    bottom: 10px;
    right: 10px;
    font-size: 16px;
`

const Container = styled.div`
    display : ${props => props.fill === false ? "inline-block" : "block"};
    position : relative;
    font-family : "Read", sans-serif;
    &.success {
        ${InputElement} {
            border : 1px solid #12AD2B ;
        }
        ${HelperText} {
            color : #12AD2B ;
        }
    }
    &.error {
        ${InputElement} {
            border : 1px solid #c80e0e ;
        }
        ${HelperText} {
            color : #c80e0e ;
        }
    }
`
const Label = styled.label`
   font-size : 14px;
   margin-bottom :5px;
   display : block;
   ${props => props.inline && "position : absolute;"}
   ${props => props.inline && "top : 12px;"}
   ${props => props.inline && "left : 10px;"}
`

export default ({ value, name, inline, onChange, helperText, label, error, success, autofocus, fill, icon, ...rest }) => {
    if (value === null) value = ""
    return (
        <Container className={`${success && "success"} ${error && "error"}`}  fill={fill}>
            {label && <Label inline={inline} for={name}>{label}</Label>}
            <InputElement inline={inline} data-e2b-id={name} onChange={e => onChange && onChange(e.target.value)} autofocus={autofocus} value={value} fill={fill} {...rest} />
            {icon && <RightIcon>{icon}</RightIcon>}
            {helperText && <HelperText>{helperText}</HelperText>}
        </Container>
    );
};

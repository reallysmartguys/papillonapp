import React, { Component } from 'react';
import tinyColors from "tinycolor2"
import styled from "styled-components"
import { colors } from "config"

const Container = styled.div`

`

const ListItemText = styled.div`

`

const EmptyMessage = styled.div`
    text-align : center;
    color : #aaa;
    padding : 20px;
    white-space : nowrap;
`

const Icon = styled.div`
    padding : 5px;
    margin-${props => props.iconRight ? "left" : "right"} : 20px;
`

const ListItem = styled.div`
    padding : 10px;
    cursor : pointer;
    display : flex;
    align-items : center;
    white-space : nowrap;
    transition : .2s;
    &:hover {
        background-color : ${props => colors[props.itemColor] ? tinyColors(colors[props.itemColor]).setAlpha(0.05).toString() : "#fbfbfb"};
    }
    ${Icon} {
        color : ${props => colors[props.itemColor] || "black"};
    }
`

class List extends Component {
    render() {
        const { items = [], iconRight, emptyMessage, ...rest } = this.props
        return (
            <Container {...rest}>
                {!!items.length && items.map((item, id) => <ListItem className='list-item' key={id} onClick={item.onClick} itemColor={item.color}>
                    {item.icon && !iconRight && <Icon className={item.icon} />}
                    <ListItemText>{item.label}</ListItemText>
                    {item.icon && iconRight && <Icon iconRight className={item.icon} />}
                </ListItem>)}
                {!items.length && emptyMessage && <EmptyMessage>{emptyMessage}</EmptyMessage>}
            </Container>
        );
    }
}

export default List;

import React from "react"
import moment from "moment"
import styled from "styled-components"

import * as globalConfig from "config"
import {formatName} from "tools"

const CloseIcon = styled.div`
    display : none;
    position : absolute;
    font-size : 10px;
    padding : 12px;
    top : 0;
    z-index : 10;
    right : 0;
    color : #aaa;
    &:hover {
        color : black;
    }
`

const Container = styled.div`
    display : flex;
    position : relative;
    padding : 10px;
    margin-bottom : 5px;
    width : 100%;
    background-color : #fbfbfb;
    box-sizing : border-box;
    cursor : pointer;
    border : 1px solid #fbfbfb;
    &:hover{
        background-color : #f1f1f1;
        border : 1px solid ${props => props.color || "#ccc"};
        ${CloseIcon} {
            display : block;
        }
    }
`

const Line = styled.div`
    white-space : nowrap;
    width : 100%;
    text-align : left;
    font-size : 13px;
    margin-bottom : 5px;
`
const Date = styled.div`
    text-align : left;
    font-size : 10px;
    opacity : .5;
    margin-top : 5px;
`

const Icon = styled.div`
    display: inline-flex;
    align-items: center;
    height: 100%;
    margin-left: 10px;
    margin-right : 15px;
    color : ${props => props.color || "black"};
    /* border: 1px solid; */
    padding: 0 10px;
    transform : scale(1.4);
    z-index : 5;
`

const getIcon = (type) => {
    switch (type) {
        case 'SHARE':
            return 'icon-ICON_OTHERS_01'
        case 'ASSIGNMENT':
            return 'icon-ICON_USER_01'
        case 'NO_FOLLOW':
            return 'icon-ICON_GPS_03'
        case 'ESCALATION' :
            return 'icon-ICON_MEDIA_03'
        case 'RECOMMENDATION':
            return 'icon-ICON_MEDIA_03'
        default:
            break;
    }
}

const getMessage = (type) => {
    switch (type) {
        case 'SHARE':
            return <span>Demande de <strong>BLOCAGE CORPORATE</strong></span>
        case 'ASSIGNMENT':
            return <span>Blocage <strong>affecté</strong></span>
        case 'NO_FOLLOW':
            return <span>Demande <strong>classé sans suite</strong></span>
        case 'ESCALATION':
            return <span>Blocage corporate <strong>pris en compte</strong></span>
        case 'RECOMMENDATION':
            return <span>La recommandation <strong>a été mise à jour</strong></span>
        default:
            break;
    }
}

export default ({ notification: { type, createdDate, meta: { caseId, criticality = null }, emitter }, onClick, onClose }) => (
    <Container color={globalConfig.colors[criticality]} onClick={onClick}>
        <CloseIcon className="icon-ICON_EDIT_05" onClick={ e => {
                e.stopPropagation()
                onClose()
            }}/>
        <div>
            <div>
                <Line>{getMessage(type)}</Line>
                <Line>Par <strong>{formatName(emitter)}</strong> - Dossier n.<strong>{caseId}</strong></Line>
            </div>
            <Date>Reçu le {moment(createdDate).format('DD/MM/YYYY')}</Date>
        </div>
        <div style={{flex:"1 1 auto", justifyContent : "right"}}>
            <Icon color={globalConfig.colors[criticality]} className={getIcon(type)}/>
        </div>
    </Container>
)

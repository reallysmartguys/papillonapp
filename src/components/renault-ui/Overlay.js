import React from 'react';
import styled, {keyframes} from "styled-components"
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'


const fadeInAnimation = keyframes`
    0% {
        transform : translateX(-50%) scale(0.9);
        opacity : 0;
    }
    100% {
        transform : translateX(-50%) scale(1);
        opacity : 1;
    }
`

const fadeOutAnimation = keyframes`
    0% {
        opacity : 1;
    }
    100% {
        opacity : 0;
    }
`

const Bg = styled.div`
    width : 100vw;
    height : 100vh;
    top : 0;
    left : 0;
    position : fixed;
    background-color : rgba(0,0,0,.4);
    z-index : 500;
`

const OverlayContainer = styled.div`
    position : fixed;
    top : 200px;
    box-shadow: 0 0 18px 0px rgba(0,0,0,.2);
    width : ${props => {
        switch (props.factor) {
            case 'wide':
                return "calc( 100vw - 100px )"
            case 'fullWidth':
                return "100vw"
            default:
                return "auto"
        }

    }};
    left : 50%;
    transform : translateX(-50%);
    z-index : 550;
    box-sizing : border-box;
    min-width : 500px;
    background-color : white;
    animation: .3s ${props => props.closing ? fadeOutAnimation : fadeInAnimation};
`

const OverlayTitle = styled.div`
    padding : 20px;
    font-size : 20px;
`

const OverlayOptions = styled.div`
    padding : 20px;
    display : flex;
    justify-content : flex-end;
`
const OverlayBody = styled.div`
    padding : 0 20px;
`

const OverlayText = styled.div`
    color : #ccc;
    margin-bottom : 20px;
`

const Button = styled.button`
    cursor : ${props => props.disabled ? "default" : "pointer"};
    padding : 10px 12px;
    display : inline-flex;
    background-color : white;
    outline : none;
    border : none;
    &:hover {
        ${props => !props.disabled && "background-color : #f1f1f1"};
    }
`

export default ({cancelLabel = "Annuler", confirmLabel = "Confirmer", children, factor, title, disabled, onClose, onConfirm, description, dataE2bId }) => [
        <Bg onClick={onClose} />,
        <ReactCSSTransitionGroup transitionName="example" transitionEnterTimeout={300} transitionLeaveTimeout={300}>
            <OverlayContainer factor={factor} data-e2b-id={dataE2bId}>
                {title && <OverlayTitle>{title}</OverlayTitle>}
                <OverlayBody>
                    {description && <OverlayText>{description}</OverlayText>}
                    {children}
                </OverlayBody>
                <OverlayOptions>
                    <Button data-e2b-id="modal-cancel" onClick={onClose}>{cancelLabel}</Button>
                    <Button data-e2b-id="modal-confirm" disabled={disabled} onClick={onConfirm}>{confirmLabel}</Button>
                </OverlayOptions>
            </OverlayContainer>
        </ReactCSSTransitionGroup>
    ]

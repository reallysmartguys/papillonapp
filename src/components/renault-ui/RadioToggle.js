import React from 'react';
import styled from "styled-components"
import tinyColors from "tinycolor2"
import { colors } from "config"

const Toggle = styled.div`
    border : 1px solid ${ props => props.selected ? props.color : "#ffcc33"};
    color : ${ props => props.selected ? "white" : props.color };
    background-color : ${ props => props.selected ? props.color : "white"};
    margin-left : -1px;
    white-space : nowrap;
    display : block;
    padding : 10px;
    cursor : pointer;
    z-index : ${props => props.selected ? 2 : 1};
    &:hover {
        color : ${ props => props.selected ? "white" : props.color };
        background-color : ${ props => props.selected ? props.color : tinyColors(props.color).setAlpha(.3).toString() };
    }
`

const ToggleContainer = styled.div`
    margin-left : 1px;
    width : 100%;
    display : flex;
`

const Container = styled.div`
    display  : inline-block;
    font-family : "Read", sans-serif;
    width : ${props => props.fullWidth ? "100%;" : "initial"};
    margin-bottom : 10px;
    ${Toggle} {
        ${props => props.fullWidth && "flex : 1;"}
    }
`

const Label = styled.label`
    font-size : 14px;
    margin-bottom :5px;
    display : block;
`

const RadioToggle = ({ options, value, onChange, label, fullWidth }) => {
    return (
        <Container fullWidth={fullWidth} >
            {label && <Label>{label}</Label>}
            <ToggleContainer>
                {options.map((option, id) => <Toggle data-e2b-id={"radio-option-"+option.value} color={option.color ? colors[option.color] : "#ffcc33"} key={id} onClick={() => onChange(option.value)} selected={value === option.value}>{option.label}</Toggle>)}
            </ToggleContainer>
        </Container>
    );
};

export default RadioToggle;

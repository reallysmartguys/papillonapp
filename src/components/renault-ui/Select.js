import React, { Component } from 'react';
import styled, { keyframes } from "styled-components"
import ClickOutside from 'components/ClickOutside'
import { Scrollbars } from 'react-custom-scrollbars';

const fadeInAnimation = keyframes`
    0% {
    transform : translateY(-10px);
    opacity : 0;
    }
    100% {
        transform : translateY(0);
    opacity : 1;
    }
`

const Container = styled.div`
   position : relative;
   text-align : left;
`

const SelectText = styled.div`
   flex : 1;
   padding : 10px 10px 10px 10px;
   overflow : hidden;
   white-space: nowrap;
   text-overflow : ellipsis;
`

const SelectIcon = styled.div`
   color : #e1e1e1 ;
   padding : 0 10px;
   transition : .2s;
`

const SelectButton = styled.div`
   align-items : center;
   font-size : 16px;
   background-color : #ffffff ;
   cursor : pointer;
   display : flex;
   box-sizing : border-box;
   border : 1px solid ${props => props.light ? "#ffffff " : "#e1e1e1 "};
   transition : .2s;
   &:hover {
       background-color : #f6f6f6 ;
       ${SelectIcon} {
           color : #000000 ;
       }
   }
   &.UL {
       background-color : transparent;
       color : rgba(0,0,0,.5);
       border : none;
       border-bottom : 1px solid;
       &:hover {
        background-color : transparent;
        color : rgba(0,0,0,1);
       }
       ${SelectText} {
           padding : 4px 0;
       }
       ${SelectIcon} {
            color : inherit;
        }
   }
   ${props=> props.disabled && `
    background-color : #ebebe4 !important;
    color : rgb(84,84,84)`}
`

const SelectMenu = styled.div`
   position : absolute;
   box-sizing : border-box;
   width : 100%;
   top: calc(100% + 5px);
   border : 1px solid #e1e1e1 ;
   animation: .3s ${fadeInAnimation};
   z-index : 50;
`
const SelecMenuItem = styled.div`
   box-sizing : border-box;
   padding : 10px;
   font-size : 16px;
   background-color : #ffffff ;
   transition : .2s;
   cursor : pointer;
   overflow : hidden;
   text-overflow : ellipsis;
   &:hover, &.selected {
       background-color : #f6f6f6 ;
   }
`

const Label = styled.label`
   font-size : 14px;
   margin-bottom :5px;
   display : block;
`


export class Select extends Component {
    state = {
        open: false
    }
    select = value => {
        this.setState({ open: false })
        this.props.onChange && this.props.onChange(value)
    }
    toggleOpen = () => this.setState({ open: !this.state.open })
    render() {
        let { value, items = [], light, ultraLight,  name, disabled, compareBy, maxHeight } = this.props
        const cleanItems = items.map(item => item.label && item.value ? item : { label: item, value: item })

        const selected = value && cleanItems.find(item => compareBy ? item.value[compareBy] === value[compareBy] : item.value === value) || {label : "--", value : "--"}

        return (
            <ClickOutside trigger={() => this.setState({ open: false })}>
                <Container>
                    <SelectButton disabled={disabled} onClick={this.toggleOpen} light={light} id={name} className={ultraLight ? "UL" : null }>
                        <SelectText>{selected.label}</SelectText>
                        <SelectIcon><span className="icon-ICON_ARROW_08"></span></SelectIcon>
                    </SelectButton>
                    {!disabled && this.state.open && <SelectMenu>
                        <Scrollbars autoHeight autoHeightMax={undefined}>
                            {cleanItems.map(item => <SelecMenuItem key={item.label} className={value === item.value && "selected"} onClick={() => this.select(item.value)} >{item.label}</SelecMenuItem>)}
                        </Scrollbars>
                    </SelectMenu>}
                </Container>
            </ClickOutside>
        );
    }
}

export default ({ label, style, ...rest }) => <div style={style}>
    {label && <Label>{label}</Label>}
    <Select {...rest} />
</div>

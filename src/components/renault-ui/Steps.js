import React from "react";
import styled from "styled-components"

const StepTitle = styled.div`
    font-size : 20px;
`

const StepNumber = styled.div`
    font-size: 20px;
    align-items: flex-start;
    display: flex;
    border: 1px solid;
    border-radius: 100%;
    align-items: center;
    height: 30px;
    padding: 0 10px;
}
`

const StepInfo = styled.div`
    flex:1 1 auto;
    padding-top : 5px;
    padding-left : 10px;
`

const Step = styled.div`
    padding : 10px 20px;
    cursor : pointer;
    display : flex;
    color : ${props=>props.done ? "black" : "#ccc"};
    ${StepNumber} {
        background-color : ${props=>props.done ? "transparent" : "#ccc"};
        color : ${props=>props.done ? "black" : "#999999"};
        border-color : ${props=>props.done ? "#ffcc33" : "#ccc"};
    }
`

const Line = styled.div`
    ${props => props.fullWidth ? "flex : 1 1 auto;" : "width : 100px"};
    height : 0;
    border-top : 1px solid ${props=>props.done ? "#ffcc33" : "#ccc"};
`

const Container = styled.div`
    display : flex;
    align-items : center;
    flex-direction : row;
    margin : 0 -20px;
`
export default ({steps, thisStep, fullWidth, centered, ...rest}) => (
    <Container>
        {steps.map( (step, index) =>[
            index > 0 && <Line key={"bar"+index} fullWidth={fullWidth} done={index <= thisStep}/>,
            <Step  key={"label"+index} done={index <= thisStep} onClick={step.onClick}>
                <StepNumber>{index+1}</StepNumber>
                <StepInfo>
                    <StepTitle>{step.title}</StepTitle>
                </StepInfo>
            </Step>
        ])}
    </Container>
)

import React from "react";
import styled from "styled-components"

const Tab = styled.div`
    width : 100%;
    display : flex;
    align-items : center;
    color : ${props => props.selected ? "black":"#999"};
    cursor : pointer;
    box-sizing : border-box;
    background-color : ${props => props.selected ? "white":"#e1e1e1"};
    &:hover {
        color : black;
    }
`

const TabHint = styled.div`
    width : 8px;
    align-self : stretch;
    background-color : ${props => props.hint || "#970076"}
`

const TabTitle = styled.h1`
    display : block;
    flex:1;
    padding : 13px;
    font-size : 18px;
    font-weight : 300;
`
export default ({label, hint, selected, style, onClick, ...rest}) => (
    <Tab selected={selected} style={style} onClick={onClick} {...rest}>
        <TabHint hint={hint}/>
        <TabTitle>{label}</TabTitle>
    </Tab>
)

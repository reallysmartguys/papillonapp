import TextareaAutosize from 'react-autosize-textarea';
import React from 'react';
import styled from "styled-components"

const InputElement = styled(TextareaAutosize)`
   width : 100%;
   box-sizing : border-box;
   font-size : 16px;
   padding : 10px;
   border : 1px solid #cccccc ;
   outline : none;
   font-family : "Read", monospace;
   resize : none;
   &:focus {
       border-color : #ffcc33 ;
   }
`

const HelperText  = styled.div`
   position : absolute;
   bottom : -15px;
   font-size : 10px;
   color : #cccccc ;
`

const Container = styled.div`
   position : relative;
   &.success {
       ${InputElement} {
           border : 1px solid #12AD2B ;
       }
       ${HelperText} {
           color : #12AD2B ;
       }
   }
   &.error {
       ${InputElement} {
           border : 1px solid #c80e0e ;
       }
       ${HelperText} {
           color : #c80e0e ;
       }
   }
`
const Label  = styled.label`
   font-size : 14px;
   margin-bottom :5px;
   display : block;
`

export default ({value, name, onChange, helperText, label, error, success, ...rest}) => {
   return (
       <Container className={`${success && "success"} ${error && "error"}`}>
           <Label for={name}>{label}</Label>
           <InputElement id={name} onChange={e=> onChange && onChange(e.target.value)} value={value} type="text" {...rest}/>
           {helperText && <HelperText>{helperText}</HelperText>}
       </Container>
   )
}

import React from 'react';
import styled from "styled-components"

const MainTitle = styled.h1`
    font-family : "Renault Life", sans-serif;
    font-weight : normal;
    font-size : 30px;
`
const Container = styled.div`
    display : flex;
    flex-direction : row;
    align-items : center;
`
const SubTitle = styled.div`
    font-size : 15px;
    color : #666;
    padding : 10px;
    margin-left : 10px;
    border-left : 1px solid;
    height : 100%;
`


const Title = ({title, subtitle, RightButton}) => (
        <Container>
            <MainTitle>{title}</MainTitle>
            { subtitle && <SubTitle>{subtitle}</SubTitle>}
            { RightButton && <div style={{marginLeft : "auto"}}>{RightButton}</div>}
        </Container>
    )

export default Title;

import moment from "moment"

export const STATUS = {
    NEW: { label: "Nouveau", value: "NEW" },
    INSTRUCTION_IN_PROGRESS: { label: "En cours d'instruction", value: "INSTRUCTION_IN_PROGRESS" },
    WAITING: { label: "Attente pièces", value: "WAITING" },
    REALIGNMENT: { label: "Réalignement en cours", value: "REALIGNMENT" },
    CLOSED: { label: "Cloturé", value: "CLOSED" },
}

export const CLASSIFICATIONS = {
    UNCLASSIFIED: { label: "A Classifier", value: "UNCLASSIFIED", icon: "icon-ICON_INFO_06" },
    NO_FOLLOW: { label: "Sans Suite", value: "NO_FOLLOW", icon: "icon-ICON_GPS_03" },
    FACTORY_LOCK: { label: "Blocage Usine", value: "FACTORY_LOCK", icon: "icon-ICON_FACTORY_01" },
    FACTORY_LOCK_KEY: { label: "Blocage Usine + Cle", value: "FACTORY_LOCK_KEY", icon: "icon-ICON_VEHICULE_10" },
    CORPORATE_FACTORY_LOCK: { label: "Blocage Usine Corporate", value: "CORPORATE_FACTORY_LOCK", icon: "icon-ICON_OTHERS_01" },
}

export const status = code => STATUS[code] || { label: "--", value: "--" }

export const classifications = code => CLASSIFICATIONS[code] || { label: "--", value: "--" }

export const caze = {
    creationDate: moment().toISOString(),
    status: STATUS.NEW.value,
    classification: CLASSIFICATIONS.UNCLASSIFIED.value,
    deadline: null,
    attached: []
}

export const colors = {
    success: "#12ad2b",
    info: "#0076CC",
    lightWarning : "#ffcc33",
    warning: "#FF8200",
    danger: "#D02433",
}


import React from 'react'
import ReactTable from 'react-table'
import { Select } from "components/renault-ui"
import Checkbox from 'material-ui/Checkbox'


class AdminPage extends React.PureComponent {
    columns = [{
        Header: 'Firstname',
        accessor: 'firstname'
    }, {
        Header: 'Lastname',
        accessor: 'lastname'
    }, {
        Header: 'Email',
        accessor: 'mail'
    }, {
        Header: 'Company',
        accessor: 'company'
    }, {
        Header: 'IPN',
        accessor: 'uid'
    }, {
        Header: 'USINE',
        accessor: 'factoryId',
        Cell: ({ original, value }) => (
            <Select
                items={this.props.factories}
                onChange={value => {
                    const chosen = this.props.factories.find(factory => factory.value === value)
                    this.props.updateUser({
                        ...original,
                        factoryId: chosen.value,
                        factoryName: chosen.label
                    })
                }}
                value={value}
            />
        )
    }, {
        Header: 'PBU',
        accessor: 'roles',
        Cell: ({ original, value }) => (
            <Checkbox
                checked={value.includes('PBU')}
                onChange={event => {
                    let roles = event.target.checked ?
                                    original.roles.concat(['PBU']).filter(role => role !== 'PBC')
                                    :
                                    original.roles.filter(role => role !== 'PBU')
                    this.props.updateUser({...original, roles})
                }}
                value="pbu"
            />
        )
    }, {
        Header: 'PBC',
        accessor: 'roles',
        Cell: ({ original, value }) => (
            <Checkbox
                checked={value.includes('PBC')}
                onChange={event => {
                    let roles = event.target.checked ?
                                    original.roles.concat(['PBC']).filter(role => role !== 'PBU')
                                    :
                                    original.roles.filter(role => role !== 'PBC')
                    this.props.updateUser({...original, roles})
                }}
                value="pbc"
            />
        )
    }, {
        Header: 'DECIDER',
        accessor: 'roles',
        Cell: ({ original, value }) => (
            <Checkbox
                checked={value.includes('DECIDER')}
                onChange={event => {
                    let roles = event.target.checked ?
                                    original.roles.concat(['DECIDER'])
                                    :
                                    original.roles.filter(role => role !== 'DECIDER')
                    this.props.updateUser({...original, roles})
                }}
                value="decider"
            />
        )
    }, {
        Header: 'ADMIN',
        accessor: 'roles',
        Cell: ({ original, value }) => (
            <Checkbox
                checked={value.includes('ADMIN')}
                onChange={event => {
                    let roles = event.target.checked ?
                                    original.roles.concat(['ADMIN'])
                                    :
                                    original.roles.filter(role => role !== 'ADMIN')
                    this.props.updateUser({...original, roles})
                }}
                value="decider"
            />
        )
    }, {
        Header: 'TECH',
        accessor: 'roles',
        Cell: ({ original, value }) => (
            <Checkbox
                checked={value.includes('TECH')}
                onChange={event => {
                    let roles = event.target.checked ?
                                    original.roles.concat(['TECH'])
                                    :
                                    original.roles.filter(role => role !== 'TECH')
                    this.props.updateUser({...original, roles})
                }}
                value="tech"
            />
        )
    }, {
        Header: 'Activate',
        accessor: 'activate',
        Cell: ({ original, value }) => (
            <Checkbox
                checked={value}
                onChange={event => this.props.updateUser({...original, activate:event.target.checked})}
                value="activate"
            />
        )
    }]


    componentDidMount() {
        this.props.fetchUsers()
        this.props.fetchFactories()
    }

    render() {
        const { users } = this.props;
        return <ReactTable
            data={users}
            columns={this.columns}
            getTdProps={() => ({style:{overflow:'visible'}})}
            getProps={() => ({style:{display:'block'}})}
        />
    }
}
export { AdminPage }

import { AdminPage } from './AdminPage'
import { connect } from 'react-redux'
import {fetchUsers, updateUser} from "state/ducks/arca"
import {fetchFactories} from 'state/ducks/factories'


export default connect(
    state => ({
        users: state.arca.users,
        factories: state.factories.map(factory => (
            {
                label: factory.plantlib,
                value: factory.plantcode.trim()
            }))
    }),
    {fetchUsers, updateUser, fetchFactories}
)(AdminPage)

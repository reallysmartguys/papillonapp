import React from 'react'
import { ConnectedRouter } from 'react-router-redux'
import Main from 'containers/Main'
import { MuiThemeProvider } from 'material-ui/styles'
import theme from "theme"
import { history } from "state/store"


const App = () => (
    <ConnectedRouter history={history}>
        <MuiThemeProvider theme={theme}>
            <Main />
        </MuiThemeProvider>
    </ConnectedRouter>
)

export default App;

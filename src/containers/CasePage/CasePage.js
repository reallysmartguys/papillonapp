import React from "react";
import {Prompt} from "react-router";
import Sticky from "react-stickynode";
import {Route, Redirect} from "react-router-dom";
import Block from "components/Block";
import CaseFormHeader from "./components/Header";
import RecoView from "./Reco";
import FileView from "./File";

const exitListener = event => (event.returnValue = "");

export default class CasePage extends React.PureComponent {
    state = {
        submitted: false,
        originalForm: {},
        selectedTab: 0,
        lastCaseId: null
    };

    componentDidMount() {
        const {match, fetchCase} = this.props;

        if (process.env.NODE_ENV === "production")
            window.addEventListener("beforeunload", exitListener);

        fetchCase(match.params.id);
    }

    componentDidUpdate(prevProps) {
        const {match, fetchCase, roles, caze, takeEdit} = this.props;

        if (match.params.id !== prevProps.match.params.id) {
            fetchCase(match.params.id);
            this.setState({selectedTab: 0});
        }

        if (caze && caze.id !== this.state.lastCaseId) {
            this.setState({lastCaseId: caze.id});
            if (
                (roles.includes("PBU") && !caze.corporate) ||
                (roles.includes("PBC") && caze.corporate)
            )
                takeEdit(caze.id);
        }

    }

    componentWillUnmount() {
        const {match, releaseEdit, caseLock, setCase} = this.props;
        if (process.env.NODE_ENV === "production")
            window.removeEventListener("beforeunload", exitListener);
        if (!caseLock.active) releaseEdit(match.params.id);
        setCase(null)
    }

    handleChange = form => {
        this.setState({originalForm: false});
        this.props.setCase(form);
    };

    handleSubmit = async(e, caze) => {
        e && e.preventDefault();
        await this.props.updateCase(caze || this.props.caze);
        this.setState({originalForm: true}, () => this.props.navigateTo("/"));
    };

    handleSave = (e, caze) => {
        this.props.updateCase(caze || this.props.caze);
        this.setState({originalForm: true});
    };

    preventEnterKey = e => {
        if (e.key === "Enter" && e.target.tagName.toLowerCase() !== "textarea") {
            e.preventDefault();
            var node = e.target.nextSibling;
            while (node) {
                if (node.tagName === "INPUT" || node.tagName === "SELECT") {
                    node.focus();
                    break;
                }
                node = node.nextSibling;
            }
        }
    };

    render() {
        const {navigateTo, loading, roles, caze, caseLock, match} = this.props;

        if (!caze && loading) return null;
        if (!caze)
            return (
                <div
                    style={{
                        color: "#ccc",
                        textAlign: "center",
                        margin: "50px 0",
                        fontSize: 40
                    }}
                >
                    Ce dossier n'existe pas ...
                </div>
            );
        const locked = caseLock.active;
        caze.corporate = true;
        const editable = true;

        return (
            <div style={{flex: 1, display: "flex", flexDirection: "column"}}>
                <Prompt when={!this.state.originalForm} message={ params => params.pathname.includes('/file/') ? true : "Votre document à été modifié. Voulez-vous réellement perdre ces modifications ?" }
                />
                <form onSubmit={this.handleSubmit} onKeyPress={this.preventEnterKey} style={{flex: 1, display: "flex", flexDirection: "column"}}>
                    <Block authRole={["PBU", "PBC"]}>
                        <Sticky innerZ={100}>
                            <CaseFormHeader
                                submitted={this.state.submitted}
                                form={caze}
                                onCancel={() => navigateTo("/")}
                                handleInputChange={this.handleChange}
                                disabled={!editable || match.params.view !== "file"}
                                view={match.params.view}
                                navigateTo={navigateTo}
                            />
                        </Sticky>
                    </Block>

                    {!match.params.view && <Redirect to={`${match.params.id}/file`}/>}

                    <Route path={`${match.path}/file/:tab?`} render={({match, location}) =>
                        <FileView
                            {
                                ...{
                                    match,
                                    location,
                                    editable,
                                    handleSave: this.handleSave,
                                    handleChange: this.handleChange,
                                    handleSubmit: this.handleSubmit,
                                    originalForm: this.state.originalForm
                                }
                            }
                        />
                    }/>
                    <Route path={`${match.path}/reco`} component={RecoView}/>f
                </form>
            </div>
        );
    }
}

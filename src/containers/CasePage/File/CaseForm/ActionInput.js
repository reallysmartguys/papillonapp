import React from "react"
import styled from "styled-components"
import {Input} from "components/renault-ui"

const ActionInputButton = styled.div`
    border-radius : 100%;
    height : 25px;
    width : 25px;
    background-color : ${props => props.color || "#ccc"};
    position : absolute;
    right : 5px;
    bottom : 7px;
    display : flex;
    align-items : center;
    justify-content : center;
    font-size : 10px;
    color : white;
`

export default ({ value, isRight, onChange, ...rest}) => (
         <div style={{position : "relative"}}>
            <Input inline {...rest} value={value} onChange={onChange}/>
            <ActionInputButton
                className={isRight && "icon-ICON_EDIT_04"}
                color={isRight ? "#12AD2B": "#D02433"}
            />
        </div>
)

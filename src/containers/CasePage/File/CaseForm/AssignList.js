import React from 'react';
import styled from "styled-components"
import { DropDown } from "components/renault-ui"
import {formatName} from "tools"

const Button = styled.div`
    cursor : ${props => props.disabled ? "default" : "pointer"};
    padding : 17px 20px;
    display : flex;
    background-color : white;
    outline : none;
    border : none;
    &:hover {
        ${props => !props.disabled && "background-color : #f1f1f1"};
    }
`
const Icon = styled.div`
    margin-right : 10px;
    transform : scale(1.2);
    display : inline-block;
`
const Flag = styled.div`
    font-size : 11px;
    color : #ccc;
    position : absolute;
    right : 10px;
    top : 10px;
`

const ListItem = styled.div`
    cursor: pointer;
    position : relative;
    padding: 10px;
    height: ${props => props.selected ? '70px' : '35px'};
    box-sizing: border-box;
    overflow: hidden;
    transition : .2s;
    &:hover {
        background-color : #f1f1f1;
    }
`

const ConfirmButton= styled.div`
    text-align : center;
    cursor : pointer;
    margin-top : 10px;
    padding : 5px;
    font-size : 12px;
    background-color : #ffcc33;
    box-sizing : border-box;
    &:hover {
        background-color : #ff9e1b;
    }
`

export default class extends React.Component{
    state = {
        selected : null
    }
    render () {
        const { caze, onShare, dibaled, PBCs, assignee, onAssign, ...rest } = this.props
        const {selected} = this.state
        return (
            <DropDown
                top
                stayOpen
                button={<Button><Icon className="icon-ICON_USER_01" />{assignee ? `Piloté par ${formatName(assignee)}` : `Pas encore attribué` }</Button>}
                {...rest}
                title={"Piloté par"}
                onClose={()=> this.setState({selected : null})}
            >
                {PBCs.map( (user, id) => {
                    const assigned = assignee && user && assignee.id === user.id
                    return <ListItem key={id} selected={!assigned && selected === id} onClick={e=>this.setState({selected : id})}>
                        <div>{formatName(user)} {assigned && <Flag>Attribué</Flag>}</div>
                        <ConfirmButton onClick={()=>onAssign(user)}>Affecter et notifier</ConfirmButton>
                    </ListItem>
                })}
            </DropDown>
        )
    }
}

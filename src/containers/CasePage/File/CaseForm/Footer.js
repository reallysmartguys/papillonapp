import React from "react";
import styled from "styled-components"

import Block from "components/Block"
import CaseFormShareButton from "./ShareButton"
import CaseFormSharePanel from "./SharePanel"
import CaseFormAssignList from "./AssignList"

import { connect } from "react-redux";
import * as globalConfig from "config"

const Container = styled.div`
    display : flex;
    justify-content : flex-end;
    z-index : 5;
    background-color : #222222;
    width : 100%;
`
const Icon = styled.div`
    margin-right : 10px;
    transform : scale(1.2);
`

const Button = styled.button`
    cursor : ${props => props.disabled ? "default" : "pointer"};
    padding : 17px 20px;
    display : flex;
    background-color : white;
    outline : none;
    border : none;
    &:hover {
        ${props => !props.disabled && "background-color : #f1f1f1"};
    }
`

const Info = styled.div`
    display : flex;
    padding : 17px 20px;
    color : ${props => props.alert ? "#ffcc33" : "#fff"};
`

const Left = styled.div`
    display : flex;
    margin-right : auto;
    flex:1;
`

class CaseFormFooter extends React.Component{
    state = {
        openMenu : false
    }
    render(){
        const { form, onChangeAssignee, onShare, onSubmitted, onNoFollow, onMakeItCorporate, onSave, touched, disabled, PBCs } = this.props
        return (
            <Container>
                <CaseFormSharePanel caze={form} open={this.state.openMenu} onClose={()=>this.setState({openMenu : false})}/>
                <Container id="test">
                    <Left>
                        {disabled && <Info alert><Icon className="icon-ICON_INFO_01" /> Vous ne pouvez pas éditer ce dossier</Info>}
                    </Left>
                    <Block authRole={["PBU","PBC"]}>
                        <Block factoryCaze>
                            <CaseFormShareButton
                                caze={form}
                                onShare={onShare}
                                onToggleMenu = { () => this.setState({openMenu : !this.state.openMenu})}/>
                            <Block authRole="PBU">
                                <Button type="button" data-e2b-id="save-case-form" disabled={!touched} onClick={onSave}><Icon className="icon-ICON_DESK_05" />Enregistrer</Button>
                                <Button type="submit" data-e2b-id="validate-case-form" onClick={onSubmitted}><Icon className="icon-ICON_EDIT_04" />Valider</Button>
                            </Block>
                        </Block>
                    </Block>
                    <Block  >

                        {!form.parent && <Block shared>
                            { form.classification !== globalConfig.CLASSIFICATIONS.NO_FOLLOW.value &&
                                <Button type="button" onClick={onNoFollow} ><Icon className="icon-ICON_GPS_03" />Classer sans suite</Button> }
                            <Button id="make-central-button" type="button" onClick={onMakeItCorporate}><Icon className="icon-ICON_COMMUNICATION_03" />Créer dossier corporate</Button>
                        </Block>}
                        <Block corporateCaze>
                            <CaseFormAssignList
                                PBCs={PBCs}
                                assignee={form.assignee}
                                onAssign={(assignee)=>onChangeAssignee({...form, assignee})}
                            />
                            <Button type="button" data-e2b-id="save-case-form" disabled={!touched} onClick={onSave}><Icon className="icon-ICON_DESK_05" />Enregistrer</Button>
                            <Button type="submit" data-e2b-id="validate-case-form" onClick={onSubmitted}><Icon className="icon-ICON_EDIT_04" />Valider</Button>
                        </Block>
                    </Block>
                </Container>
            </Container>
        )
    }
}

export default connect(
    ({arca}) => ({
        PBCs : arca.users.filter(user => user.roles.includes("PBC"))
    }), {}
)(CaseFormFooter)

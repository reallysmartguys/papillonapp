import * as React from "react";

const index = (obj, is, value) => {
    if (typeof is === 'string')
        return index(obj, is.split('.'), value);
    else if (is.length === 1 && value !== undefined)
        return obj[is[0]] = value;
    else if (is.length === 0)
        return obj;
    else
        return index(obj[is[0]], is.slice(1), value);
}

export default class FormLink extends React.PureComponent {
    state = {
        invalid: false,
        touched: false
    };

    constructor(props) {
        super(props);
        const { form, name, required } = props;
        let value = form[name];
        if (required && (!value || value.length === 0)) {
            this.setState({ invalid: true });
        }
    }

    check = (value) => {
        if (!value || (value.length === 0 && !this.state.invalid)) {
            this.setState({ invalid: true });
        } else if (value.length > 0 && this.state.invalid) {
            this.setState({ invalid: false });
        }
    };

    handleChange = (event) => {
        const { required, handleInputChange, form, name } = this.props;
        if (!this.state.touched) {
            this.setState({ touched: true })
        }
        if (required) {
            this.check(event.target.value);
        }

        let data = event.target ? event.target.value : event
        let newForm = { ...form };
        index(newForm, name, data)

        handleInputChange(newForm);
    };


    render() {
        const { helperText, submitted, name, component, required, form, disabled } = this.props;
        const { touched, invalid } = this.state;

        let error = invalid && (touched || submitted);
        let requiredHelperText = !error ? '' : helperText ? helperText : "Veuillez renseigner ce champ.";

        let value = index(form, name) || ""

        return React.cloneElement(component, { onChange: this.handleChange, helperText: requiredHelperText, name, error, required, value, disabled })
    }
}

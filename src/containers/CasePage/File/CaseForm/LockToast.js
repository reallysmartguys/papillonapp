import React from "react"
import styled, {keyframes} from "styled-components"
import {headShake} from "react-animations"
import ClickOutside from 'components/ClickOutside'
import {formatName} from "tools"

const headShakeAnimation = keyframes`${headShake}`;

const RedOut = keyframes`
    0% {
        color : #D02433;
    }
    100% {
        color : black;
    }
`;

const LockToast = styled.div`
    position : fixed;
    top : 250px;
    right : ${props => props.open ? -50 : -300}px;
    display : flex;
    align-items : center;
    flex-direction : row;
    background-color : white;
    border-left : 4px solid ${props => props.hint || "#0076CC" };
    box-shadow : 0 0 5px rgba(0,0,0,.2);
    transition : .3s;
    animation : ${props => props.animate && `.8s ${headShakeAnimation}`};
`

const LockIconWarper = styled.div`
    display : flex;
    align-items : center;
    height : 100%;
    cursor : pointer;
    flex:1 1 auto;
    padding : 10px;
`

const LockIcon = styled.div`
    font-size : 40px;
    transform: rotate(-45deg);
`
const Body = styled.div`
    width : 250px;
    box-sizing : border-box;
    padding-left : 20px;
    margin-right : 50px;
    padding : 10px;
`
const BodyText = styled.div`
    animation : ${props => props.animate && `.8s ${RedOut}`};
`

const BodySubText = styled.div`
    font-size : 11px;
`

// const Button = styled.div`
//     padding : 5px 7px;
//     font-size : 15px;
//     // background-color : #ffcc33;
//     color : #ffcc33;
//     display : inline-block;
//     cursor : pointer;
//     margin-top : 10px;
//     &:hover {
//         background-color : #ffcc33; /*#e2ae12;*/
//         color : white;
//     }
// `

export default class extends React.Component {
    state = {
        open : true,
        animate : true,
    }
    componentWillReceiveProps(lprops) {
        if(lprops.uid !== this.props.uid) this.setState({animate : false} , () => this.setState({animate : true}))
    }
    render() {
        const {owner, /*takeEdit*/ } = this.props
        const {animate, open} = this.state
        return (
            <ClickOutside trigger={() => this.setState({ open: false })}>
                <LockToast hint="#D02433" open={open} animate={animate}>
                    <LockIconWarper onClick = {()=> this.setState({open : !open})} ><LockIcon className="icon-ICON_COCKPIT_04"/></LockIconWarper>
                    <Body>
                        <BodyText animate={animate}>Lecture seule</BodyText>
                        <BodySubText animate={animate}>Modifications en cours par {formatName(owner, true)}</BodySubText>
                        {/* <div><Button onClick={takeEdit}>Prendre la main</Button></div> */}
                    </Body>
                </LockToast>
            </ClickOutside>
        )
    }
}

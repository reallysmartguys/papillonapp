import React from "react"
import { Grid } from "material-ui"
import FormLink from "../FormLink";
import { connect } from 'react-redux'
import { Input, Datepicker, AutoComplete, Title } from "components/renault-ui"

import { fetchBqvId } from "state/ducks/defects"

// TODO - BQV-FILLED FIELDS

const Blockage = ({ caze, handleChange, handleSave, editable, defectsId, fetchBqvId, }) => (
    <div>
        <Title title="Blocage" />
        <Grid container spacing={24}>
            <Grid item xs={12}>
                <Grid container spacing={24}>
                    <Grid item xs={12} sm={4}>
                        <FormLink
                            disabled={!editable}
                            form={caze}
                            name="blanSignatureDate"
                            handleInputChange={handleChange}
                            component={
                                <Datepicker
                                    showTimeSelect
                                    label="Horodatage arrêt signature en BLAN"
                                />
                            } />
                    </Grid>
                    <Grid item xs={12} sm={4}>
                        <FormLink
                            disabled={!editable}
                            form={caze}
                            name="qualityCodeNumber"
                            handleInputChange={handleChange}
                            component={<Input type="number" label="N° contrôle qualité" placeholder="Example : 842" />}
                        />
                    </Grid>
                    <Grid item xs={12} sm={4}>
                        <FormLink
                            disabled={!editable}
                            form={caze}
                            name="qualityCodeNumberDate"
                            handleInputChange={handleChange}
                            component={
                                <Datepicker
                                    label="Date code qualité"
                                />
                            } />
                    </Grid>
                    <Grid item xs={12} sm={4}>
                        <FormLink
                            disabled={!editable}
                            form={caze}
                            name="psvf"
                            handleInputChange={handleChange}
                            component={
                                <Datepicker
                                    label="Date blocage usine (PSFV)"
                                />
                            } />
                    </Grid>
                    <Grid item xs={12} sm={4}>
                        <FormLink
                            disabled={!editable}
                            form={caze}
                            name="blockedVehiclesNumber"
                            handleInputChange={handleChange}
                            component={<Input type="number" label="Nombre de véhicules bloqués usine" placeholder="Example : 77" />}
                        />
                    </Grid>
                    <Grid item xs={12} sm={4}>
                        <AutoComplete
                            label="N° de défaillance"
                            type="number"
                            name="bqvId"
                            value={caze.bqvId}
                            onChange={value => fetchBqvId(value, caze.corporate)}
                            onSelect={bqvId => handleSave(null, { ...caze, bqvId })}
                            items={defectsId}
                            inputProps={{ highLight: true }}
                            disabled={!editable}
                        />
                    </Grid>

                    {caze.bqv && [<Grid item xs={12} sm={4}>
                        <FormLink
                            disabled={true}
                            form={caze}
                            name="bqv.dcreat"
                            component={
                                <Datepicker
                                    label="Date création défaillance"
                                />
                            } />
                    </Grid>,
                    <Grid item xs={12} sm={4}>
                        <FormLink
                            disabled={true}
                            form={caze}
                            name="bqv.nom"
                            component={<Input label="Nom de la défaillance" />}
                        />
                    </Grid>,
                    <Grid item xs={12} sm={4}>
                        <FormLink
                            disabled={true}
                            form={caze}
                            name="bqv.lib"
                            component={<Input label="Libéllé de la défaillance" />}
                        />
                    </Grid>,
                    <Grid item xs={12} sm={4}>
                        <FormLink
                            disabled={true}
                            form={caze}
                            name="bqv.nbveh"
                            component={<Input type="number" label="Nombre véhicules défaillance" />}
                        />
                    </Grid>,
                    <Grid item xs={12} sm={4}>
                        <FormLink
                            disabled={!editable}
                            form={caze}
                            name="cleBlockDate"
                            component={
                                <Datepicker
                                    label="Date blocage CLE"
                                />
                            } />
                    </Grid>]}

                    <Grid item xs={12} sm={4}>
                        <FormLink
                            disabled={!editable}
                            form={caze}
                            name="blockedVinNumber"
                            handleInputChange={handleChange}
                            component={<Input type="number" label="Nombre de VIN bloqués CLE à la création du blocage" placeholder="Example : 788" />}
                        />
                    </Grid>

                    {caze.bqv && <Grid item xs={12} sm={4}>
                        <FormLink
                            disabled={true}
                            form={caze}
                            name="bqv.dprev"
                            component={
                                <Datepicker
                                    label="Date prévision de déblocage"
                                />
                            } />
                    </Grid>}

                    <Grid item xs={12} sm={4}>
                        <FormLink
                            disabled={!editable}
                            form={caze}
                            name="shippedVehiclesNumber"
                            handleInputChange={handleChange}
                            component={<Input type="number" label="Nombre de véhicules livrés" placeholder="Example : 16161" />}
                        />
                    </Grid>
                    <Grid item xs={12} sm={4}>
                        <FormLink
                            disabled={!editable}
                            form={caze}
                            name="otsId"
                            handleInputChange={handleChange}
                            component={<Input label="N° d'OTS" placeholder="Example : 16161" />}
                        />
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    </div>

)

export default connect(
    ({ defects: { defectsId } }) => ({
        defectsId
    }),
    {
        fetchBqvId
    },
)(Blockage)

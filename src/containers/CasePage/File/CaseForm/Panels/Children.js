import React from "react"
import { connect } from "react-redux";
import styled from "styled-components";

import {AutoComplete} from "components/renault-ui"

import {STATUS} from "config"

import { setParent, deleteParent, getChildren  } from "state/ducks/caze";
import { fetchCases} from "state/ducks/cases";

const TopBar = styled.div`
    display : flex;
    padding-bottom : 20px;
    margin-bottom : 20px;
    align-items : center;
    border-bottom : 1px solid #ccc;
`

const Cell = styled.div`
    margin-right : 50px;
`

const Right = styled.div`
    display : flex;
    margin-left : auto;
    &>div{
        margin-left : 10px;
    }
`

const Title = styled.div`
    font-size : 20px;
`

const Button = styled.div`
    padding : 10px 15px;
    background-color : #efece3;
    cursor : pointer;
    &:hover {
        background-color : #ffcc33;
    }
`

const Case= styled.div`
    padding : 15px;
    width : 200px;
    background-color : white;
    margin : 10px;
    box-shadow : 0 0 5px rgba(0,0,0,.1);
    transition : .2s;
    display : inline-block;
    cursor : pointer;
    position : relative;
    vertical-align : top;
    height : 130px;
    &:hover {
        box-shadow : 0 0 5px rgba(0,0,0,.3);
    }
    &>div{
        margin-bottom : 10px;
    }
`

const Body = styled.div`
    margin: 0 -10px;
`

const FactoryName = styled.div`
    color : #ffcc33;
    font-size : 20px;
    text-align : center;
`

const Defect = styled.div`
`

const DefectName = styled.div`
    color : #777;
`
const CarsNumber = styled.div`
  padding-top: 10px;
  font-size: 18px;
`
const DeleteButton = styled.div`
    position : absolute;
    top : 10px;
    font-size : 12px;
    right : 10px;
    opacity : .5;
    &:hover {
        opacity : 1;
    }
`

class ChildrenPanel extends React.Component{
    state = {
        searchFactory : "",
        selectedFactory : null,
        searchCase : "",
        selectedCaseId : null,
    }

    componentDidMount = () => {
        this.props.getChildren(this.props.caze.id)
        this.props.fetchCases()
    }

    componentDidUpdate = (lprops, lstate) => {
        if(lstate.searchFactory !== this.state.searchFactory) this.setState({searchCase :""})
    }

    addChild = async () => {
        const {caze, setParent, fetchCases} = this.props
        const {selectedCaseId} = this.state
        if(selectedCaseId) {
            await setParent(selectedCaseId, caze)
            fetchCases()
            this.setState({
                searchFactory : "",
                selectedFactory : null,
                searchCase : "",
                selectedCaseId : null,
            })
        }
    }

    render() {
        const {caze, caze : {children = []}, freeCases, deleteParent, push} = this.props
        const {searchCase, searchFactory, selectedFactory} = this.state

        const factories = [...new Set(freeCases.map(caze => caze.factoryName || "N/A"))]
        const excludedFactories = children.map(caze => caze.factoryName);
        const filteredFactories = factories.filter(factory => excludedFactories.indexOf(factory) === -1 && factory.match(new RegExp(searchFactory, 'i')))

        const cases = freeCases.filter(caze => !caze.factoryName || caze.factoryName === selectedFactory)
            .map(caze => ({
                label: caze.title,
                value: caze.id,
                object: caze
            }))
        const filteredCases = cases.filter(caze => caze.label.match(new RegExp(searchCase, 'i')))
        const total = children.map(caze => caze.defectId ? caze.bqv.nbveh : 0).reduce((nb, total) => nb + total, 0)
        return (
            <div>
                <Title title="Dossiers associés"/>
                <TopBar>
                    <Cell>
                        <Title>Regroupement n°45655</Title>
                    </Cell>
                    <Cell>
                        <Title><span className="icon-ICON_FACTORY_01"/> {children && children.length}</Title>
                    </Cell>
                    <Cell>
                        <Title><span className="icon-ICON_VEHICULE_01"/> {total}</Title>
                    </Cell>
                    <Right>
                        <AutoComplete
                            inputProps={{placeholder: "Usine"}}
                            items={filteredFactories}
                            value={searchFactory}
                            debounce={0}
                            onChange={searchFactory => this.setState({searchFactory})}
                            onSelect={selectedFactory => this.setState({selectedFactory})}
                        />
                        <AutoComplete
                            inputProps={{placeholder: "Dossier"}}
                            value={searchCase}
                            items={filteredCases}
                            debounce={0}
                            onChange={searchCase => this.setState({searchCase})}
                            onSelect={selectedCaseId => this.setState({selectedCaseId})}
                            listElement={item => <div>
                                <div>{item.title}</div>
                                {item.defectId && <div style={{color: "#0076CC"}}>def.{item.defectId}</div>}
                            </div>}
                        />
                        <Button onClick={this.addChild}>Ajouter</Button>
                    </Right>
                </TopBar>
                <Body>
                {children.map(caze => (
                    <Case key={caze.id} onClick={() => push(`/cases/${caze.id}`)}>
                        <DeleteButton onClick = {e => {
                            e.stopPropagation()
                            deleteParent(caze.id, caze.parent.id)
                        }} className="icon-ICON_EDIT_05"/>
                        <FactoryName>{caze.factoryName || "Unknown"}</FactoryName>
                        <div>{caze.title}</div>
                        {caze.defectId &&
                        <Defect>
                            <DefectName>{caze.defectId} - {caze.bqv.nom}<br/>{caze.bqv.lib}</DefectName>
                            <CarsNumber><span className="icon-ICON_VEHICULE_01"/> {caze.bqv.nbveh}</CarsNumber>
                        </Defect>}

                    </Case>
                ))}
                </Body>
            </div>
        )
    }
}

export default connect(
    ({ caze, cases }) => ({
        caze,
        freeCases :  cases.filter(caze => !caze.corporate && !caze.parent && caze.status!== STATUS.CLOSED.value),
    }),
    {
        setParent,
        deleteParent,
        fetchCases,
        getChildren
    }
)(ChildrenPanel);

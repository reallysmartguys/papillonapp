import React from "react"
import { Grid } from "material-ui"
import { Input, Datepicker, Title } from "components/renault-ui"
import FormLink from "../FormLink"
import { formatDate } from "tools"

export default ({ caze, handleChange, editable, push }) => (
    <div>
        <Title title="Communication" />
        <Grid container spacing={24}>
            <Grid item xs={12} sm={6}>
                <Input
                    disabled
                    label="Date diffusion recommandation"
                    value={formatDate(caze.recommendationSentDate)}
                />
            </Grid>
            <Grid item xs={12} sm={6}>
                <Input
                    disabled
                    label="Date diffusion recommandation"
                    value={formatDate(caze.noteDiffusionDate)}
                />
            </Grid>
            <Grid item xs={12} sm={6}>
                <FormLink
                    disabled={!editable}
                    form={caze}
                    name="blockOrderDate"
                    handleInputChange={handleChange}
                    component={
                        <Datepicker
                            label="Date diffusion ordre de blocage"
                        />
                    } />
            </Grid>
            <Grid item xs={12} sm={6}>
                <FormLink
                    disabled={!editable}
                    form={caze}
                    name="technicalDiffusionDate"
                    handleInputChange={handleChange}
                    component={
                        <Datepicker
                            label="Date diffusion directive technique"
                        />
                    } />
            </Grid>
        </Grid>
    </div>
)

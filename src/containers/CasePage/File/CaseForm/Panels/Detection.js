import React from "react";
import {Grid} from "material-ui";
import FormLink from "../FormLink";
import {Datepicker, Input, TextArea, Select, Title, Attached} from "components/renault-ui";
import Block from "components/Block";
import {formatName} from "tools";



const Detection = ({caze, handleChange, editable, handleSave, ...props}) => (
    <div>
        <Title title="Infos Générale"/>
        <Grid container spacing={24}>
            <Grid item xs={12} sm={5} md={2}>
                <Grid container spacing={24}>
                    <Grid item xs={12}>
                        <Input name="id" label="N° Dossier"
                               value={caze.id} disabled/>
                    </Grid>

                    <Block corporateCaze>
                        <Grid item xs={12}>
                            <Input
                                label="Dossier Créé par"
                                disabled
                                value={caze.assignee ? formatName(caze.assignee) : ""}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <FormLink disabled={!editable} form={caze} name="firstMeegingDate"
                                      handleInputChange={handleChange}
                                      component={
                                          <Datepicker
                                              label="Date de Première Rencontre"/>
                                      }/>
                        </Grid>
                        <Grid item xs={12}>
                            <FormLink disabled={!editable} form={caze} name="teacherPresentationDate"
                                      handleInputChange={handleChange}
                                      component={
                                          <Datepicker
                                              label="Date présentation Enseignant"
                                          />
                                      }/>
                        </Grid>
                        <Grid item xs={12}>
                            <FormLink disabled={!editable} form={caze} name="signatureDate"
                                      handleInputChange={handleChange}
                                      component={
                                          <Datepicker
                                              label="Date Signature Contrat"
                                          />
                                      }/>
                        </Grid>
                        <Grid item xs={12}>
                            <FormLink disabled={!editable} form={caze} name="courseStartDate"
                                      handleInputChange={handleChange}
                                      component={
                                          <Datepicker
                                              label="Date Début du cours"
                                          />
                                      }/>
                        </Grid>
                    </Block>


                </Grid>
            </Grid>
            <Grid item xs={12} sm={7} md={5}>

                <Grid container spacing={24}>
                    <Grid item xs={12}>

                            <FormLink disabled={!editable} form={caze} name="clientLastfName"
                                      component={<Input label="Nom du client" value={caze.clientLastName}/>}
                            />

                            <FormLink disabled={!editable} form={caze} name="clientFirstfName"
                                      component={<Input label="Prénoms" value={caze.clientFirstName}/>}
                            />

                    </Grid>

                    <Grid item xs={12}>
                        <FormLink
                            disabled={!editable}
                            form={caze}
                            name="commune"
                            handleInputChange={handleChange}
                            component={<Select label="Commune" items={communes.map(com=>com.nom)} />}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <FormLink
                            disabled={!editable}
                            form={caze}
                            name="quartier"
                            handleInputChange={handleChange}
                            component={<Select label="Quartier" items={communes.map(com=>com.nom)} />}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <FormLink disabled={!editable} form={caze} name="cite" handleInputChange={handleChange}
                                  component={ <Input label="Citée" value={caze.cite}/>}
                        />

                    </Grid>
                    <Grid item xs={12}>
                        <FormLink disabled={!editable}
                            form={caze}
                            name="adresse"
                            handleInputChange={handleChange}
                            component={<TextArea rows={2} label="Détails Adresse"
                                                 placeholder="Détailler le plus possible la localisation du domicile"/>}
                        />
                    </Grid>



                </Grid>
            </Grid>
            <Grid item xs={12} sm={7} md={5}>
                <Grid container spacing={24}>

                    <Grid item xs={12}>
                        <Attached
                            label="Pieces jointes"
                            files={caze.files}
                            caseId={caze.id}
                            disabled={!editable}
                        />
                    </Grid>
                </Grid>


            </Grid>
        </Grid>
    </div>
)


const communes = [
    {
        nom:"Abobo",
        quartiers:[]
    },
    {
        nom:"Koumassi",
        quartiers:[
            {nom:"Remblais"},
            {nom:"Prodomo"}
        ]
    },
    {nom:"Cocody", quartiers:[
        {nom:"Angré"},
        {nom:"2 Plateaux"},
        {nom:"Saint Jean"}
    ]},
    {
        nom:"Marcory",
        quartiers:[
            {nom:"Remblais"},
            {nom:"Ibiscus"},
            {nom:"Sainte Bernadette"},
            {nom:"Zone 4"},
            {nom:"Zone 3"},
            {nom:"Bietry"}
        ]
    },
    {nom:"Yopougon",quartiers:[]},
    {nom:"Plateau",quartiers:[]},
    {
        nom:"Vridi",
        quartiers:[]
    },
]

export default Detection

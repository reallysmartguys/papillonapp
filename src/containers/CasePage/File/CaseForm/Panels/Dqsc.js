import React from "react"
import { Grid } from "material-ui"
import FormLink from "../FormLink";
import { Datepicker, Input, TextArea, Title } from "components/renault-ui"

export default ({ caze, handleChange, editable }) => (
    <div>
        <Title title="DQSC-CAE et iDAV" />
        <Grid container spacing={24}>
            <Grid item xs={12} sm={6}>
                <FormLink
                    disabled={!editable}
                    form={caze}
                    name="dqsccaeDate"
                    handleInputChange={handleChange}
                    component={<Datepicker
                        label="Date info DQSC-CAE"
                    />}
                />
            </Grid>
            <Grid item xs={12} sm={6}>
                <FormLink
                    disabled={!editable}
                    form={caze}
                    name="pots"
                    handleInputChange={handleChange}
                    component={
                        <Input
                            label="POTS"
                            placeholder="Example : V. Hollande"
                        />
                    } />
            </Grid>
            <Grid item xs={12} sm={6}>
                <FormLink
                    disabled={!editable}
                    form={caze}
                    name="idavDate"
                    handleInputChange={handleChange}
                    component={<Datepicker
                        label="Info iDAV"
                    />}
                />
            </Grid>
            <Grid item xs={12} sm={6}>
                <FormLink
                    disabled={!editable}
                    form={caze}
                    name="cit"
                    handleInputChange={handleChange}
                    component={
                        <Input
                            label="CIT (chargé d'information technique iDAV)"
                            placeholder="Example : C. Chevrier"
                        />
                    } />
            </Grid>
        </Grid>
    </div>

)

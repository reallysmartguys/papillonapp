import React from "react"
import { Grid } from "material-ui"
import FormLink from "../FormLink";
import { Input, TextArea, Select, Attached, Title } from "components/renault-ui"
import Block from "components/Block"

const imputation = {
    "Blocage annulé": [
        "Blocage annulé"
    ],
    "Fournisseur": [
        "Advanced Comfort Systems",
        "AKA",
        "AKOM",
        "Allevard",
        "AMIS",
        "Antolin",
        "Autoliv",
        "Autoliv, Bourbon et Trecar",
        "autoneum",
        "Avon",
        "Bant Boru",
        "BATZ",
        "Birlik",
        "Bitron",
        "Borg-Warner",
        "Bosch",
        "Bourbon",
        "Brakes India PL",
        "Brembo",
        "Bronze-Alu",
        "Calsonic",
        "CIE Vilanova",
        "Continental",
        "Cooper",
        "CTS",
        "Dana",
        "Defontaine",
        "Delphi",
        "Denso",
        "DKA",
        "Dura",
        "Dytech Tecalon",
        "Eberspächer",
        "Edscha",
        "Euro Aps",
        "Euro Business Group",
        "FATE",
        "Faurécia",
        "Federal Mogul",
        "FICOSA",
        "Filtrauto",
        "Fremax",
        "Gestamp",
        "Getrag",
        "Gic Nosac",
        "GKN",
        "GMD",
        "Hankook",
        "Hanlim Intech",
        "Hella",
        "Heuliez",
        "Humax",
        "Hutchinson",
        "Iberofon",
        "Il-Heung",
        "Inergy",
        "INTEVA",
        "ITW",
        "Ixetic",
        "Johnson Control",
        "JTEKT",
        "J-Ushin",
        "Kiekert",
        "Kostal",
        "KSS",
        "Küster",
        "Kyb",
        "La Dauphinoise",
        "Landirenzo",
        "Le Bélier",
        "Lear",
        "Lear et Valeo",
        "LG",
        "LISI",
        "Lovato",
        "Luk",
        "Maflow",
        "Magal",
        "Magna",
        "Magnetti-Marelli",
        "Mahle",
        "Mando",
        "Martek",
        "Martur",
        "Mate",
        "MCSyncro Vigo",
        "Mebant",
        "Metalchrome",
        "Metaldyne",
        "MGI Coutier",
        "Minda Furukawa",
        "Mirgor",
        "MMM",
        "Mubea",
        "Nedschroef",
        "Nexen",
        "Nobel Teknik",
        "NSK",
        "NTN",
        "Omron",
        "Orhan Oto",
        "Panasonic",
        "Pernat",
        "Pilkington",
        "Pilkinton",
        "PKG",
        "Plastic Omnium",
        "PPG",
        "Precision Tech Enterprise",
        "Rieter Argentine",
        "Saint-Gobain",
        "Samkyoung",
        "SAPA",
        "SAS",
        "SBFM",
        "Scattolini",
        "Sealynx",
        "SEWS",
        "Shindaelim",
        "Siemar",
        "SMC",
        "SMR",
        "Sumitomo",
        "SY",
        "Takata",
        "Tefas",
        "Temic",
        "Thyssen Krupp",
        "Tom Tom",
        "Trakya Döküm",
        "Trèves",
        "TRW",
        "UAMT",
        "Ushin Kosice",
        "Valéo",
        "Viauro",
        "Vimercati",
        "Visteon",
        "Vitemco",
        "Wagon",
        "Webasto",
        "Xiang Yang Guang Rui",
        "Yazaki",
        "ZF",
    ],
    "Ingénierie Process": [
        "DI-PG",
        "DIP-V",
        "DIVD",
    ],
    "Ingénierie Produit": [
        "2ASDU",
        "DE-L",
        "DE-M",
        "DE-P",
        "DE-S",
        "DE-T",
        "DE-V",
        "DIAM",
        "DICAP",
        "DIEC",
        "DIESC",
        "DIESE",
        "DIM",
        "DPC",
        "DRTH",
        "IDVU",
        "RNTCI",
        "RST",
        "RTA",
        "RTE",
        "RTK",
        "RTR",
    ],
    "Partenaire": [
        "Avtovaz Togliatti",
        "GM Luton",
        "Ingénierie Nissan",
        "Nissan Barcelone",
        "Nissan Chennai",
        "Nissan Cuernavaca",
        "Nissan Europe",
        "Nissan Rosslyn",
        "Tan Chong",
    ],
    "UCM": [
        "UCM d Oran",
        "UCM de Batilly",
        "UCM de Bursa",
        "UCM de Casablanca",
        "UCM de Chennai",
        "UCM de Cordoba",
        "UCM de Curitiba",
        "UCM de Curitiba VU",
        "UCM de Dieppe",
        "UCM de Douai",
        "UCM de Envigado",
        "UCM de Flins",
        "UCM de Maubeuge",
        "UCM de Moscou",
        "UCM de Novo mesto",
        "UCM de Palencia",
        "UCM de Pitesti",
        "UCM de Pusan",
        "UCM de Sandouville",
        "UCM de Tanger",
        "UCM de Togliatti",
        "UCM de Valladolid",
        "UCM de Wuhan        ",
    ],
    "UFM": [
        "Fonderie de Bretagne",
        "UFM de Bursa",
        "UFM de CACIA",
        "UFM de Cléon",
        "UFM de Curitiba",
        "UFM de Douvrin",
        "UFM de Los Andes",
        "UFM de Pitesti",
        "UFM de Ruitz",
        "UFM de Séville",
        "UFM de Valladolid Motores",
        "UFM de Villeurbanne",
        "UFM du Mans        ",
    ],
    "Autre": [
        "DIPA",
        "Direction du Produit",
        "DSCM",
        "Marketing",
        "non affectée",
        "Projet",
        "RSM",
        "Transporteur",
    ],
}

export default ({ caze, handleChange, editable, ...props }) => (
    <div>
        <Title title="Instruction" />
        <Grid container spacing={24}>
            <Grid item xs={12} md={6}>
                <Grid container spacing={24}>
                    <Grid item xs={12}>
                        <FormLink
                            disabled={!editable}
                            form={caze}
                            name="clientImpact"
                            handleInputChange={handleChange}
                            component={<TextArea rows={2} label="Effet client" placeholder="Example : Risque de perte de direction" />}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Attached
                            label="Pieces jointes"
                            files={caze.files}
                            caseId={caze.id}
                            disabled={!editable}
                            type="clientImpact"
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <FormLink
                            disabled={!editable}
                            form={caze}
                            name="defaultDescription"
                            handleInputChange={handleChange}
                            component={<TextArea rows={2} label="Mode de défaillance" placeholder="Example : Casse de l'armature du volant de direction" />}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <FormLink
                            disabled={!editable}
                            form={caze}
                            name="cause"
                            handleInputChange={handleChange}
                            component={<TextArea rows={2} label="Cause de défaillance" placeholder="Example : Modification des moules d'injection de l'armature par le fournisseur (risque de présence d'un fissure en sortie de moule)" />}
                        />
                    </Grid>

                    <Block corporateCaze>
                        <Grid item xs={12}>
                            <FormLink
                                disabled={!editable}
                                form={caze}
                                name="controlDefectRate"
                                handleInputChange={handleChange}
                                component={<Input icon="‰" label="Taux de défaillance issu des contrôle" type="number" />}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <FormLink
                                disabled={!editable}
                                form={caze}
                                name="responsibleDefectRate"
                                handleInputChange={handleChange}
                                component={<Input icon="‰" label="Taux de défaillance annoncé par entité responsable" type="number" />}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <FormLink
                                disabled={!editable}
                                form={caze}
                                name="finalDefectRate"
                                handleInputChange={handleChange}
                                component={<Input icon="‰" label="Taux de défaillance retenu en synthèse" type="number" />}
                            />
                        </Grid>
                    </Block>

                </Grid>
            </Grid>
            <Grid item xs={12} md={6}>
                <Grid container spacing={24}>
                    <Grid item xs={12}>
                        <FormLink
                            disabled={!editable}
                            form={caze}
                            name="imputation"
                            handleInputChange={handleChange}
                            component={<Select label="Imputation" items={Object.keys(imputation)} />}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <FormLink
                            disabled={!editable}
                            form={caze}
                            name="imputationDetails"
                            handleInputChange={handleChange}
                            component={<Select label="Imputation Fine" items={imputation[caze.imputation]} maxHeight={300} />}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <FormLink
                            disabled={!editable}
                            form={caze}
                            name="finalCause"
                            handleInputChange={handleChange}
                            component={<Select
                                label="Cause finale"
                                items={["Défaillance moyen",
                                    "En cours d'analyse",
                                    "Robustesse conception et/ou validation",
                                    "Respect processus",
                                    "Traçabilité composants",
                                    "Fiabilité de la chaine documentaire",
                                ]}
                            />}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <FormLink
                            disabled={!editable}
                            form={caze}
                            name="severity"
                            handleInputChange={handleChange}
                            component={<Select label="Gravité" items={[
                                "EICPS",
                                "Réglementaire",
                                "Panne immobilisante",
                                "Défaut de fonctionnement",
                                "Aspect",
                                "Autre",
                            ]} />}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <FormLink
                            disabled={!editable}
                            form={caze}
                            name="severityComment"
                            handleInputChange={handleChange}
                            component={<TextArea label="Commentaire gravité" rows={2} placeholder="Example : A confirmer par l'ingénierie" />}
                        />
                    </Grid>

                    <Grid item xs={12}>
                        <FormLink
                            disabled={!editable}
                            form={caze}
                            name="gqeId"
                            handleInputChange={handleChange}
                            component={<Input label="Numero de dossier GQE" placeholder="Example : 201700507651" />}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <FormLink
                            disabled={!editable}
                            form={caze}
                            name="lup"
                            handleInputChange={handleChange}
                            component={<Input label="LUP" placeholder="Example : QMXX006070" />}
                        />
                    </Grid>

                    <Block corporateCaze>
                        <Grid item xs={12}>
                            <FormLink
                                disabled={!editable}
                                form={caze}
                                name="backToNormal"
                                handleInputChange={handleChange}
                                component={<Input label="Retour à la conformité de production assuré" type="number" />}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <FormLink
                                disabled={!editable}
                                form={caze}
                                name="backToNormalDate"
                                handleInputChange={handleChange}
                                component={<Input label="Date de retour à conformité de production" type="number" />}
                            />
                        </Grid>
                    </Block>

                </Grid>
            </Grid>
        </Grid>
    </div>
)

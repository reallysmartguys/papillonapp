import React from "react"
import { Grid } from "material-ui"
import FormLink from "../FormLink";
import { Datepicker, Input, TextArea, Title } from "components/renault-ui"

export default ({ caze, handleChange, editable }) => (
    <div>
        <Title title="Tracabilité" />
        <Grid container spacing={24}>
            <Grid item xs={12} sm={6}>
                <FormLink
                    disabled={!editable}
                    form={caze}
                    name="firstDefectiveVehicle"
                    handleInputChange={handleChange}
                    component={<Input
                        label="1er véh. douteux ou non conforme (début tranche à risque)"
                        placeholder="Example : 1926248"
                    />}
                />
            </Grid>
            <Grid item xs={12} sm={6}>
                <FormLink
                    disabled={!editable}
                    form={caze}
                    name="firstDefectiveVehicleDate"
                    handleInputChange={handleChange}
                    component={
                        <Datepicker
                            showTimeSelect
                            label="Horodatage TCM - 1er véh. douteux (début tranche à risque)"
                        />
                    } />
            </Grid>
            <Grid item xs={12} sm={6}>
                <FormLink
                    disabled={!editable}
                    form={caze}
                    name="lockDescription"
                    handleInputChange={handleChange}
                    component={<TextArea
                        rows={2}
                        label="Description du verrouillage"
                        placeholder="Example : Contrôle des pièces avant montage"
                    />}
                />
            </Grid>
            <Grid item xs={12} sm={6}>
                <FormLink
                    disabled={!editable}
                    form={caze}
                    name="lockDate"
                    handleInputChange={handleChange}
                    component={
                        <Datepicker
                            showTimeSelect
                            label="Horodatage verrouillage"
                        />
                    } />
            </Grid>
            <Grid item xs={12} sm={6}>
                <FormLink
                    disabled={!editable}
                    form={caze}
                    name="firstValidVehicle"
                    handleInputChange={handleChange}
                    component={<Input
                        label="1er véh. Post verrouillage (fin tranche à risque)"
                        placeholder="Example : 2560252"
                    />}
                />
            </Grid>
            <Grid item xs={12} sm={6}>
                <FormLink
                    disabled={!editable}
                    form={caze}
                    name="firstValidVehicleDate"
                    handleInputChange={handleChange}
                    component={
                        <Datepicker
                            showTimeSelect
                            label="Horodatage TCM - 1er véh. Post verrouillage (fin tranche à risque)"
                        />
                    } />
            </Grid>
            <Grid item xs={12}>
                <FormLink
                    disabled={!editable}
                    form={caze}
                    name="temporarySolution"
                    handleInputChange={handleChange}
                    component={<TextArea rows={2} label="Solution Provisoire" />}
                />
            </Grid>
            {!!caze.temporarySolution && <Grid item xs={12} sm={6}>
                <FormLink
                    disabled={!editable}
                    form={caze}
                    name="firstFixedVehicle"
                    handleInputChange={handleChange}
                    component={<Input
                        label="1er véh. avec solution provisoire"
                        placeholder="Example : VF1FL000057821842"
                    />}
                />
            </Grid>}
            {!!caze.temporarySolution && <Grid item xs={12} sm={6}>
                <FormLink
                    disabled={!editable}
                    form={caze}
                    name="firstFixedVehicleDate"
                    handleInputChange={handleChange}
                    component={
                        <Datepicker
                            showTimeSelect
                            label="Horodatage TCM 1er véh.  avec solution provisoire"
                        />
                    } />
            </Grid>}
        </Grid>
    </div>

)

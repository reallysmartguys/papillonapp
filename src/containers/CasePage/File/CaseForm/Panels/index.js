import Detection from "./Detection";
import Instruction from "./Instruction"
import Tracking from "./Tracking"
import Blockage from "./Blockage";
import Communication from "./Communication";

import Dqsc from "./Dqsc";
import Children from "./Children"

export default {
    Detection,
    Instruction,
    Tracking,
    Blockage,
    Communication,
    Dqsc,
    Children
}

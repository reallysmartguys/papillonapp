import React from 'react';
import styled from "styled-components"

const SharedButton = styled.div`
    cursor : pointer;
    display : flex;
    padding : 17px 20px;
    background-color : #0076cc;
    &:hover {
        background-color : #0b5e9a;
    }
`

const SharedButtonOrange = SharedButton.extend`
    background-color : #ff8200;
    &:hover {
        background-color : #ab5700;
    }
`

const SharedButtonGreen = SharedButton.extend`
    background-color : #c3d51f;
    &:hover {
        background-color : #7a8611;
    }
`

const Icon = styled.div`
    margin-right : 10px;
    transform : scale(1.2);
`
export default ({ caze, onShare, onToggleMenu }) => {
    if(caze.parent) return <SharedButtonGreen disabled onClick={onToggleMenu}><Icon className="icon-ICON_OTHERS_01" />Blocage corporate</SharedButtonGreen>
    if (caze.shared) return <SharedButton disabled onClick={onToggleMenu}><Icon className="icon-ICON_OTHERS_01" />Blocage corporate</SharedButton>
    return <SharedButtonOrange data-e2b-id="share-case-button" onClick={onShare} >
                <Icon className="icon-ICON_OTHERS_01"/>Demander le blocage corporate
        </SharedButtonOrange >
}

import React from 'react';
import styled from "styled-components"
import moment from "moment"

import {CLASSIFICATIONS} from "config"
import {formatName} from "tools"

import Link from "components/Link"

const Pannel = styled.div`
    width : 100%;
    background-color : #222;
    position : absolute;
    bottom : 100%;
    left : 0;
    z-index : 1;
    transition : .3s;
    transform : ${props => props.open ? '' : "translateY(100%)"};
    padding : 20px;
    box-sizing : border-box;
`

const CloseButton = styled.div`
    position : absolute;
    top : 15px;
    right : 15px;
    color : white;
    cursor : pointer;
    font-size : 15px;
`

const Title = styled.div`
    font-size : 20px;
    color : ${props => props.color || "#ccc"}
`

const Text = styled.div`
    font-size : 15px;
    color : white;
    margin-top : 20px;
    &>div {
        margin-bottom : 10px;
    }
`

const CaseLink = styled.span`
    color : #222;
    background-color : white;
    padding : 5px 10px;
    text-decoration : none;
    margin : 0 10px;
    cursor : pointer;
`

const title = (caze) => {
    if(caze.parent && caze.parent.assignee) return <Title color="#c3d51f">Demande de blocage corporate assignée</Title>
    if(caze.parent) return <Title color="#c3d51f" >Demande de blocage corporate prise en compte</Title>
    if(caze.shared && caze.classification === CLASSIFICATIONS.NO_FOLLOW.value) return <Title color="#d02433">Demande de blocage classée sans suite</Title>
    if(caze.shared) return <Title>En attente</Title>
}

const text = (caze) => {
    if(caze.parent && caze.parent.assignee) return <span>Un dossier de blocage corporate à été ouvert, piloté par {
        (caze.parent.assignee, true)} <CaseLink><Link to={`/cases/${caze.parent.id}`}> Voir le dossier</Link></CaseLink></span>
    if(caze.parent) return <span>Un dossier de blocage corporate à été ouvert <CaseLink><Link to={`/cases/${caze.parent.id}`}>Voir le dossier</Link></CaseLink></span>
    if(caze.shared && caze.classification === CLASSIFICATIONS.NO_FOLLOW.value) return <Text>
        <div>Par {formatName(caze.closedBy, true)} le {moment(caze.closureDate).format("DD/MM/YYYY")}</div>
        <div>Commentaire : </div>
        <div>{caze.commentOnNoFollowUp}</div>
    </Text>
    if(caze.shared) return "Demande de blocage corporate en attente de prise en compte"
}

const SharePanel = ({caze, open, onClose}) => (
    <Pannel open={open}>
        <CloseButton className="icon-ICON_EDIT_05" onClick={onClose} />
        {title(caze)}
        <Text>{text(caze)}</Text>
    </Pannel>
)

export default SharePanel;

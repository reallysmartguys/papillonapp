import React from "react"
import styled from "styled-components"
import { Tab } from "components/renault-ui"


const Categories = styled.div`
    width: 200px;
    border-right: 1px solid #e4e4e4;
    overflow: hidden;
    min-height : 100%;
`

const config = {
    rubriqueEmployee: [
        { label: "Infos Générales", hint: "#FFCC33", link:"detection" },
        { label: "Enfants", hint: "#D02433" , link:"tracking"},
        { label: "Contrat", hint: "#FF9E1B" , link:"communication"}
    ],


    rubriqueManager : [
        { label: "Detection", hint: "#FFCC33", link:"detection" },
        { label: "Instruction", hint: "#FF9E1B" , link:"instruction"},
        { label: "Blocage", hint: "#0076CC", link:"blockage" },
        { label: "Communication", hint: "#00997a", link:"communication" },
        { label: "DQSC-CAE et iDAV", hint: "pink", link:"dqsc" },
        { label: "Dossiers associés", hint: "#ccc", link:"children" }
    ],
}

export default ({ corporate, thisTab, navigateTo }) => {
   // let tabs = corporate ? config.rubriqueManager : config.rubriqueEmployee
    let tabs =  config.rubriqueEmployee

    return (
    <Categories>
        {tabs.map((cat, index) => <Tab {...cat} key={cat.label}
            data-e2b-id={`tabs-${cat.link}`}
            selected={cat.link === thisTab}
            onClick={() => navigateTo(cat.link)} />)}
    </Categories>
)}

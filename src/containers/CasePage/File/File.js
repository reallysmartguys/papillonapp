import React from "react";
import styled from "styled-components";
import {Redirect} from "react-router-dom";
import Panels from "./CaseForm/Panels";
import TabBar from "./CaseForm/TabBar";
import Footer from "./CaseForm/Footer";
import LockToast from "./CaseForm/LockToast";
import * as globalConfig from "config";

const FormFooter = styled.div`
    position: fixed;
    bottom: 0;
    width: 100%;
`;
const Main = styled.div`
    display: flex;
    flex: 1 1 auto;
    padding-bottom: 48px;
`;

const Form = styled.div`
    flex: 1;
    padding: 20px 20px 70px;
    background-color: ${({locked}) => (locked ? "#f2f2f2" : "#fbfbfb")};
`;


export default class File extends React.PureComponent {
    preventEnterKey = e => {
        if (e.key === "Enter" && e.target.tagName.toLowerCase() !== "textarea") {
            e.preventDefault();
        }
    };

    render() {
        const {
            editable, caze, locked, toggleModal, handleSave, handleChange, caseLock, handleShare, makeItCorporate, handleSubmit, originalForm, push, match
        } = this.props;

        // Redirect to first tab if no one's sepcified
        if (!match.params.tab) return <Redirect to={`${match.url}/detection`}/>

        // Render File
        return (
            <div style={{flex: 1, display: "flex", flexDirection: "column"}}>
                <Main key={"main"}>
                    <TabBar
                        corporate={caze.corporate}
                        id="tabBar"
                        thisTab={match.params.tab}
                        navigateTo={push}
                    />

                    <Form locked={locked}>
                        {React.createElement(Panels[match.params.tab[0].toUpperCase() + match.params.tab.slice(1)], {
                                caze,
                                push,
                                editable,
                                handleChange,
                                handleSave,
                            }
                        )
                        }
                    </Form>

                    {locked && (
                        <LockToast
                            owner={caseLock.owner}
                            uid={caseLock.uid}
                        />
                    )}
                </Main>

                <FormFooter key={"formFooterr"}>
                    <Footer
                        form={caze}
                        touched={!originalForm}
                        disabled={!editable}
                        onChangeAssignee={assignee => handleSave(null, assignee)}
                        onNoFollow={() =>
                            toggleModal({
                                show: true,
                                type: "noFollowModal",
                                props: {
                                    title: "Classer sans suite",
                                    required: true,
                                    onConfirm: comment =>
                                        handleSubmit(null, {
                                            ...caze,
                                            classification: globalConfig.CLASSIFICATIONS.NO_FOLLOW.value,
                                            commentOnNoFollowUp: comment
                                        }),
                                    confirmLabel: "Classer sans suite"
                                }
                            })}
                        onMakeItCorporate={() => makeItCorporate(caze.id)}
                        onShare={() =>
                            toggleModal({
                                show: true,
                                type: "beforeShare",
                                props: {
                                    confirmLabel: "Faire la demande",
                                    title: "Demande de blocage corporate",
                                    description: "Veuillez définir la criticité de la demande de blocage",
                                    onConfirm: criticality =>
                                        handleShare(caze.id, criticality),
                                    required: true
                                }
                            })}
                        onSave={handleSave}
                        onSubmitted={() => this.setState({submitted: true})}
                    />
                </FormFooter>
            </div>
        );
    }
}

import File from "./File"

import { connect } from 'react-redux'
import { push } from 'react-router-redux';

import { handleShare, makeItCorporate} from "state/ducks/caze"
import { toggleModal } from "state/ducks/ui"

export default connect(
    ({ caze, ui : { roles, caseLock}, defects : {defectsId} }) => ({
        caze,
        roles,
        caseLock
    }),
    {
        handleShare,
        makeItCorporate,
        toggleModal,
        push,
    },
)(File)

import React from "react";
import styled from "styled-components";

import showdown from "showdown"
import Sticky from "react-stickynode";

import Section from "./components/Section";

const Empty = styled.div`
text-align : center;
font-size : 20px;
opacity : .5;
margin : 50px 0;
`

const Button = styled.div`
padding : 7px;
display : inline-block;
border : 1px solid #ffcc33;
color : #ffcc33;
border-radius : 5px;
cursor : pointer;
&:hover {
    background-color : #ffcc33;
    color : black;
}
`


const mdConverter = new showdown.Converter({ extensions: [ ()=> {
    var myext1 = {
      type: 'lang',
      regex: /\+\+(.*?)\+\+/g,
      replace: '<u>$1</u>'
    };
    var myext2 = {
        type: 'output',
        regex: /<p>/g,
        replace: '<p style="margin : 0;">'
      };
    return [myext1, myext2];
  }] })


const ActionButton = styled.div`
  font-size: 20px;
  padding: 10px;
  border: 1px solid #ffcc33;
  border-radius: 100%;
  margin-left : 10px;
  &:hover {
    background-color: rgba(255, 204, 51, 0.2);
  }
`;

const Container = styled.div`display: flex;`;
const Reco = styled.div`
  margin: 20px;
  box-shadow: 0 0 5px rgba(0, 0, 0, 0.5);
  flex: 5 1 auto;
`;

const Side = styled.div`
  width: 261px;
  min-width: 261px;
  margin: 20px;
  margin-left: 0;
  height: 100%;
`;

const SideBox = styled.div`
    box-shadow: 0 0 5px rgba(0, 0, 0, 0.5);
    margin-bottom : 10px;
`;

const SideBody = styled.div`
    padding: 20px;
`;

const SideTitle = styled.div`
  font-size: 20px;
  text-align: center;
  padding: 20px;
`;

const TopBar = styled.div`
  padding: 10px 20px;
  font-size: 20px;
  border-bottom: 2px solid #222;
  display: flex;
  align-items: center;
`;

const Thumb = styled.div`
  width: 75px;
  height: 75px;
  margin: 5px;
  background-color: #ccc;
  border: 1px solid #ccc;
  display: inline-block;
  background-size: cover;
  background-position: center;
  opacity: 0.5;
  background-image: url(${props => props.imageUrl});

  &:hover {
    opacity: 1;
  }
`;
const Right = styled.div`
  display: flex;
  margin-left: auto;
  cursor: pointer;
`;

const Input = styled.input`
  font-size: 20px;
  border: none;
  outline: none;
  width: 100%;
  display: block;
  margin-left: 20px;
  box-sizing: border-box;
`;

const SubjectBox = styled.div`
  display: flex;
  font-size: 20px;
  align-items: center;
  white-space: nowrap;
  font-weight: bold;
  padding: 10px;
  border: 1px dashed transparent;
  &:hover,
  &:focus {
    border-color: #ccc;
  }
`;

export default class EditReco extends React.Component {
    state = {
        isDragged: null
    };

    postReco = async () => {
        const { caze, reco, postReco} = this.props;
        reco.html = this.generateHtml(reco)
        await postReco(caze.id, reco);
    };
    sendReco = () => {
        const { caze, reco, sendReco } = this.props;
        reco.html = this.generateHtml(reco)
        sendReco(caze.id, reco);
    };
    generateHtml = (reco) => {
        const { caze } = this.props;
        return `<p class="toHide">
                    💬 <a href="${window.location.hostname}/cases/${caze.id}/reco">Répondre à la recommandation</a>
                </p>
                <hr/>
                <table style="width:100%">`
            + reco.content.map(section => {
                    return `
                    <tr>
                        ${ section.title ? `<td style="padding-bottom: 25px;font-size : 13px; font-weight : bold; text-decoration : underline; width: 1px; white-space : nowrap; vertical-align:top;">
                            ${section.title}
                        </td>` : ``}
                        <td style="vertical-align:top; padding-bottom: 25px;">
                            ${mdConverter.makeHtml(section.content) +
                            "<div class='images'>" +
                            section.attached
                                .map(file => {
                                    let url = `${window.location.protocol}//${window.location.hostname}/api/files?caseId=${caze.id}&name=${file.name}`;
                                    return `<img style="margin-right: 10px;" src="${url}" height="150"/>`;
                                })
                                .join("") +
                            "</div>"}
                        </td>
                    </tr>
                    `
                })
                .join("") +
                `</table>
                <hr/>
                <p class="toHide">
                    💬 <a href="${window.location.hostname}/cases/${caze.id}/reco">Répondre à la recommandation</a>
                </p>`
    }
    setSection = (id, section) => {
        const { reco, setReco} = this.props
        let content = [...reco.content];
        content.splice(id, 1, section);
        setReco({ ...reco, content });
    };
    createReco = () => {
        const { caze, setReco, user, reco } = this.props;
        const newReco = {
            content: [
                {
                    title : "Recommandation",
                    content: "N/A",
                    attached: []
                },
                {
                    title : "Effet Client",
                    content: caze.clientImpact || "N/A",
                    attached: []
                },
                {
                    title : "Détection",
                    content:  caze.reportedWhen || "N/A",
                    attached: []
                },
                {
                    title : "Mode de défaillance",
                    content: caze.defaultDescription || "N/A",
                    attached: []
                },
                {
                    title : "Cause",
                    content: (caze.cause || "N/A"),
                    attached: []
                },
                {
                    title : "Responsabilité",
                    content: caze.responsability || "N/A",
                    attached: []
                },
                {
                    title : "Véh. concernés",
                    content: caze.impactedVehicle || "N/A",
                    attached: []
                },
                {
                    title : "Répartition à date",
                    content: "N/A",
                    attached: []
                },
                {
                    title : "Taux de non conformité",
                    content:caze.defaultDescription || "N/A",
                    attached: []
                },
                {
                    title : "Gamme de réalignement",
                    content: "N/A",
                    attached: []
                },
                {
                    title : "Pièces nécessaires",
                    content: caze.defaultDescription || "N/A",
                    attached: []
                },
                {
                    title : "Informations complémentaires",
                    content: "N/A",
                    attached: []
                },
                {
                    content: "Cordialement,\n\n"+user.firstname+" "+user.lastname.toUpperCase(),
                    attached: []
                }
            ]
        };
        if(reco) newReco.id = reco.id
        setReco(newReco, true);

    };

    render() {
        const { caze, uid, reco, setReco } = this.props;

        if (!reco) return <div style={{textAlign : "center"}}>
            <Empty>Ce dossier n'a pas de recommendation associée.</Empty>
            <Button onClick={()=>this.createReco()}>Créer une recomendation</Button>
        </div>

        return (

            <Container>
                <Reco>
                    <TopBar>
                        Aperçu de la recommandation
                        <Right>

                            <ActionButton onClick={this.postReco}>
                                <div className="icon-ICON_DESK_05" />
                            </ActionButton>
                            <ActionButton onClick={this.createReco}>
                                <div className="icon-ICON_DESK_07" />
                            </ActionButton>
                            <ActionButton onClick={this.sendReco}>
                                <div className="icon-ICON_COMMUNICATION_02" />
                            </ActionButton>

                        </Right>
                    </TopBar>
                    <div style={{ padding: 10 }}>
                        <SubjectBox>
                            OBJET :{" "}
                            <Input
                                value={reco.subject || ""}
                                onChange={e =>
                                    setReco({ ...reco, subject: e.target.value })}
                            />
                        </SubjectBox>
                        {reco.content.map((section, id) => (
                            <Section
                                key={uid + "" + id}
                                onChange={changedSection => this.setSection(id, changedSection)}
                                section={section}
                                cazeId={caze.id}
                                isDragged={this.state.isDragged}
                            />
                        ))}
                    </div>
                </Reco>
                <Side>
                    {reco.sent && <SideBox>
                        <SideBody>
                            <span>Recommendation envoyée</span>
                        </SideBody>
                    </SideBox>}
                    <Sticky top={100}>
                        <SideBox>
                        <SideTitle>Images du dossier</SideTitle>
                        {caze.files.map((file, id) => {
                            const url = `"/api/files?caseId=${caze.id}&name=${file.name}&type=${file.type}"`;
                            return (
                                <Thumb
                                    key={id}
                                    imageUrl={url}
                                    onDragStart={e => this.setState({ isDragged: file })}
                                    onDragEnd={e => this.setState({ isDragged: null })}
                                    draggable="true"
                                />
                            );
                        })}
                    </SideBox>
                    </Sticky>
                </Side>
            </Container>
        );
    }
}

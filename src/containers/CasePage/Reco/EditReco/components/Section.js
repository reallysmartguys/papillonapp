import React from "react"
import styled from "styled-components"
import RichTextEditor from 'react-rte';
import showdown from "showdown"
import {SortableContainer, SortableElement, arrayMove} from 'react-sortable-hoc';

import { Input } from "components/renault-ui"

const mdConverter = new showdown.Converter({
    extensions: [() => {
        var myext1 = {
            type: 'lang',
            regex: /\+\+(.*?)\+\+/g,
            replace: '<u>$1</u>'
        };
        return [myext1];
    }]
})

const EditButton = styled.div`
    position : absolute;
    top: 5px;
    right : 5px;
    display : none;
    cursor : pointer;
    opacity : .5;
    border-bottom : 1px dashed;
    &:hover {
        opacity : 1;
    }
`


const SectionStyle = styled.div`
    position : relative;
    h2 {
        font-size : 14px;
    }
    .RichTextEditor__root___2QXK-{
        font-family : "Read";
    }
    .RichTextEditor__paragraph___3NTf9{
        margin : 0;
    }
    p {
        margin : 0;
    }
    border : 1px dashed ${props => props.edit ? "#000 !important" : "transparent"};
    &:hover{
        border-color : #ccc;
        ${EditButton} {
            display : block;
        }
        .EditorToolbar__root___3_Aqz {
            opacity : 1;
        }
    }
`

const DeleteButton = styled.div`
    padding : 5px;
    background-color : #222;
    color : white;
    font-size : 10px;
    top : 0;
    right : 0;
    position : absolute;
    cursor : pointer;
    display : none;
`
const ImgBlock = styled.div`
    display : inline-flex;
    flex-direction : column;
    position : relative;
    text-align : center;
    border : 1px solid transparent;
    &:hover {
        ${DeleteButton} {
            display : block;
        }
        border : 1px solid #222;
    }
`
const AttachedBar = styled.div`
    display : flex;
    ${ImgBlock} {
        img {
            height : 150px;
            margin-bottom : 5px;
        }
        margin-right : 10px;
    }
`

class Section extends React.Component {
    state = {
        title: this.props.section.title,
        value: RichTextEditor.createValueFromString(this.props.section.content, 'markdown'),
        edit: false
    }
    onChangeContent = value => {
        const { onChange, section } = this.props
        this.setState({ value })
        onChange && onChange({
            ...section,
            content: this.state.value.toString('markdown'),
        })
    }
    onChangeTitle = title => {
        const { onChange, section } = this.props
        this.setState({ title })
        onChange && onChange({
            ...section,
            title,
        })
    }
    componentDidMount() {
        document.addEventListener('click', this.onClickOutside, false);
    }
    componentWillUnmount() {
        document.removeEventListener('click', this.onClickOutside, false);
    }
    onClickOutside = e => {
        if (this.state.edit && this.container && !this.container.contains(e.target)) {
            this.setState({ edit: false })
        }
    }
    onDeleteImage = id => {
        const { section: { attached = [] }, onChange, section } = this.props
        attached.splice(id, 1)
        onChange && onChange({ ...section, attached })
    }
    onAddImage = file => {
        const { section: { attached = [] }, onChange, section } = this.props
        attached.push(file)
        onChange && onChange({ ...section, attached })
    }
    onDrop = e => {
        const { isDragged } = this.props
        isDragged && this.onAddImage(isDragged)
    }
    onEdit = () => {
        this.setState({ edit: true })
    }
    render() {
        const { section: { attached = [], content, title }, cazeId } = this.props
        return (
            <SectionStyle
                onDragOver={e => e.preventDefault()}
                onDragEnter={e => e.preventDefault()}
                onDrop={this.onDrop}
                edit={this.state.edit}
                innerRef={container => this.container = container}
            >
                <EditButton onClick={this.onEdit}>Editer</EditButton>

                {this.state.edit &&
                    <div style={{ display: "flex", padding: 10 }}>
                        <div style={{ width: "200px", paddingRight: 10 }}><Input onChange={this.onChangeTitle} value={title} /></div>
                        <div style={{ flex: "1 1 auto" }} onClick={this.onEdit}>
                            <RichTextEditor
                                value={this.state.value}
                                onChange={this.onChangeContent}
                                toolbarConfig={{
                                    display: ['INLINE_STYLE_BUTTONS', 'BLOCK_TYPE_BUTTONS', 'BLOCK_TYPE_DROPDOWN', 'HISTORY_BUTTONS'],
                                    INLINE_STYLE_BUTTONS: [
                                        { label: 'Bold', style: 'BOLD', className: 'custom-css-class' },
                                        { label: 'Italic', style: 'ITALIC' },
                                        { label: 'Underline', style: 'UNDERLINE' }
                                    ],
                                    BLOCK_TYPE_DROPDOWN: [
                                        { label: 'Normal', style: 'unstyled' },
                                        { label: 'Heading Large', style: 'header-one' },
                                        { label: 'Heading Medium', style: 'header-two' },
                                        { label: 'Heading Small', style: 'header-three' }
                                    ],
                                    BLOCK_TYPE_BUTTONS: [
                                        { label: 'UL', style: 'unordered-list-item' },
                                        { label: 'OL', style: 'ordered-list-item' }
                                    ]
                                }}
                                handleDrop={e => true}
                            />
                        </div>
                    </div>
                }

                {!this.state.edit &&
                    <div style={{ display: "flex", padding: 10 }}>
                        {title && <div style={{ width: "200px", paddingRight: 10, fontSize: 13, fontWeight: "bold", textDecoration: "underline" }}>{title}</div>}
                        <div style={{ flex: "1 1 auto" }} dangerouslySetInnerHTML={{ __html: mdConverter.makeHtml(content) }} onClick={this.onEdit} />
                    </div>
                }

                <AttachedBar >
                    {attached.map((file, id) => <ImgBlock key={id}>
                        <DeleteButton onClick={() => this.onDeleteImage(id)}>Supprimer</DeleteButton>
                        <img src={`/api/files?caseId=${cazeId}&name=${file.name}&type=${file.type}`} alt={file.title} />
                        {file.title}
                    </ImgBlock>)}
                </AttachedBar>
            </SectionStyle>
        )
    }
}

export default Section

import EditReco from "./EditReco"
import { connect } from "react-redux";
import { postReco, fetchReco, setReco, sendReco } from "state/ducks/reco";

export default connect(
    ({ reco, caze, arca : {user} }) => ({
        reco: reco.data,
        uid: reco.uid,
        caze,
        user
    }),
    {
        postReco,
        fetchReco,
        setReco,
        sendReco
    }
)(EditReco);

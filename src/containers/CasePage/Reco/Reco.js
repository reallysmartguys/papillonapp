import React from "react"
import { Route, Redirect } from "react-router-dom";

import EditReco from "./EditReco"
import RecoAdvice from "./RecoAdvice"

import Block from "components/Block"
import StepBar from "./components/StepBar"

export default class RecoView extends React.Component{
    componentDidMount() {
        const { caze, fetchReco} = this.props;
        fetchReco(caze.id);
    }

    componentWillUnmount(){
        const {setReco } = this.props;
        setReco(null, true)
    }

    render(){
        const { match, reco } = this.props
        return (
            <div>
                <Block authRole={["PBC"]}>
                    <StepBar thisStep={reco && reco.sent ? 1 : 0} match={match}/>
                </Block>
                <Route exact path={`${match.url}/edit`} component={EditReco} />
                {reco && <Route exact path={`${match.url}/advice`} component={RecoAdvice} />}
                <Route exact path={ match.url } render={()=> reco && reco.sent ? <Redirect to={`${match.url}/advice`}/> : <Redirect to={`${match.url}/edit`}/>} />
            </div>
        )
    }
}

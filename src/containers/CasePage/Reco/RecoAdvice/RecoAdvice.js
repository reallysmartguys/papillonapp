import React from "react";
import styled from "styled-components";

import { TextArea } from "components/renault-ui"
import { formatName, formatDate } from "tools"

const offsetTop = (el) => {
    var rect = el.getBoundingClientRect(),
        scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    return rect.top + scrollTop
}

const Container = styled.div`
    display: flex;
`
const Side = styled.div`
    margin: 20px;
    flex : 1 1 auto;
    flex-basis : 0;
`
const Box = styled.div`
    border : 1px solid #979797;
    margin-bottom : 20px;
`
const BoxHeader = styled.div`
    border-bottom : 1px solid #979797;
    padding : 20px;
    display : flex;
    font-size : 15px;
`
const BoxBody = styled.div`
    display : flex;
    flex-direction : column;
    background-color : #f6f6f6;
`

const BoxHeaderYellow = BoxHeader.extend`
    background-color : #ffcc33;
`
const Row = styled.div`
    border-bottom : 1px solid #ccc;
    padding : 10px 20px;
    font-size : 15px;
    display : flex;
    align-items :center;
    background-color : ${props => props.dark ? "#ccc":"#f6f6f6"};
`
const Name = styled.div`
    flex : 1 1 auto;
`

const Col = styled.div`
    width : 50px;
    text-align : center;
    color : ${props => props.active ? "#0076cc" : "#999999"};
`
const Date = styled.div`
    width : 150px;
    text-align : right;
`
const Messages = styled.div`
    flex : 1 1 auto;
    padding : 20px;
    max-height : ${props => props.thisComponent && window.innerHeight - offsetTop(props.thisComponent) - 160}px;
    overflow : auto;
`
const InputArea = styled.div`
    position : relative;
    padding : 20px;
`
const SendButton = styled.div`
        background-color: #ffcc33;
        color: white;
        padding: 8px;
        display: inline-block;
        cursor: pointer;
        position: absolute;
        right: 25px;
        bottom: 29px;
`

const MessageLine = styled.div`
    padding-${props => props.fromMe ? "left" : "right"} : 100px;
    margin-top : 20px;
`
const Message = styled.div`
    background-color :white;
    border : 1px solid #e1e1e1;
    padding : 10px;
    white-space: pre;
`
const Right = styled.div`
    margin-left : auto;
`
const Button = styled.div`
    padding : 10px;
    color : black;
    border : 1px solid #ffcc33;
    margin : 5px;
    cursor : pointer;
    &:hover {
        background-color : #ffcc33;
    }
`

const Link = styled.div`
    cursor : pointer;
    border-bottom : 1px dashed;
`

export default class EditReco extends React.Component {
    state = {
        message: "",
        ShoxReco : false
    }

    componentDidMount() {
        const { reco, fetchAdviceList, fetchMessages } = this.props;
        if (reco) {
            fetchAdviceList(reco.id)
            fetchMessages(reco.id)
        }
    }

    componentDidUpdate(lprops) {
        if (this.messages) this.messages.scrollTop = this.messages.scrollHeight;
        // const { reco, fetchAdviceList} = this.props;
        // if(lprops.reco != reco) fetchAdviceList(reco.id)

    }

    sendMessage = () => {
        const { reco, postMessage } = this.props
        postMessage(reco.id, this.state.message)
        this.setState({ message: "" })
    }

    update = () => {
        const { reco, fetchAdviceList, fetchMessages } = this.props;
        fetchAdviceList(reco.id)
        fetchMessages(reco.id)
    }

    render() {
        const { reco, user, postAgreement } = this.props;
        if (!reco || !reco.id) return null

        return (
            <Container>
                <Side>
                    <Box>
                        <BoxHeader>Collecte avis DSC <Right onClick={this.update}><Link>Actualiser</Link></Right></BoxHeader>
                        {reco.advices.map(advice => {
                            if (user.roles.includes("DECIDER") && advice.decisionMaker.id === user.id && advice.agree === null)
                                return (
                                    <Row key={advice.id}>
                                        <Name>{formatName(advice.decisionMaker)}</Name>
                                        <div><Button onClick={()=>postAgreement(reco.id, advice.id, true)}>OK</Button></div>
                                        <div><Button onClick={()=>postAgreement(reco.id, advice.id, false)}>NOK</Button></div>
                                        <Col active={reco.messages && reco.messages.find( message => message.author.id === advice.decisionMaker.id)}><span className="icon-ICON_INFO_05" /></Col>
                                        <Date>{advice.agree !== null && formatDate(advice.createdDate,"[Le] DD/MM/YYYY à HH:mm")}</Date>
                                    </Row>
                                )

                            return (
                                <Row key={advice.id} dark={advice.agree === null && (!reco.messages || !reco.messages.find( message => message.author.id === advice.decisionMaker.id)) } >
                                    <Name>{formatName(advice.decisionMaker)}</Name>
                                    <Col active={advice.agree === true}>OK</Col>
                                    <Col active={advice.agree === false}>NOK</Col>
                                    <Col active={reco.messages && reco.messages.find( message => message.author.id === advice.decisionMaker.id)}><span className="icon-ICON_INFO_05" /></Col>
                                    <Date>{advice.agree !== null && formatDate(advice.createdDate,"[Le] DD/MM/YYYY à HH:mm")}</Date>
                                </Row>
                            )
                        })}
                    </Box>
                    <Box>
                        <BoxHeader>Recommandation <Right onClick={()=> this.setState({showReco : !this.state.showReco})}><Link> {this.state.showReco ? "Masquer" : "Afficher"} </Link></Right></BoxHeader>
                    {this.state.showReco &&  <BoxBody style={{padding : 10}} dangerouslySetInnerHTML={{__html : reco.html.replace(/\<p class\=\"toHide\"\>([\s\S]*?)\<\/p\>/g,"")}}/> }
                    </Box>
                </Side>
                <Side>
                    <Box >
                        <BoxHeaderYellow>Demande de précisions  <Right onClick={this.update}><Link>Actualiser</Link></Right></BoxHeaderYellow>
                        <BoxBody>
                            <Messages innerRef={messages => this.messages = messages} thisComponent={this.messages}>

                                {( !reco.messages || !reco.messages.length ) && <span style={{ opacity: .5 }}>Aucune conversation à afficher</span>}
                                {!!reco.messages && reco.messages.map(message => (
                                    <MessageLine key={message.id} fromMe={message.author.id === user.id}>
                                        <div style={{ display: "flex" }}>
                                            <span>{formatName(message.author)}</span>
                                            <span style={{ marginLeft: "auto" }}>{formatDate(message.createdDate,"[Le] DD/MM/YYYY à HH:mm")}</span>
                                        </div>
                                        <Message>
                                            {message.content}
                                        </Message>
                                    </MessageLine>
                                ))}
                            </Messages>
                            <InputArea>
                                <TextArea onChange={value => this.setState({ message: value })} value={this.state.message} style={{ paddingRight: 50 }} />
                                <SendButton className="icon-ICON_COMMUNICATION_02" onClick={this.sendMessage} />
                            </InputArea>
                        </BoxBody>
                    </Box>
                </Side>
            </Container>
        );
    }
}

import RecoAdvice from "./RecoAdvice"
import { connect } from "react-redux";
import { fetchAdviceList, postMessage, postAgreement, fetchMessages} from "state/ducks/reco";

export default connect(
    ({ reco, caze, arca : {user} }) => ({
        reco: reco.data,
        uid: reco.uid,
        caze,
        user
    }),
    {
        fetchAdviceList,
        fetchMessages,
        postMessage,
        postAgreement
    }
)(RecoAdvice);

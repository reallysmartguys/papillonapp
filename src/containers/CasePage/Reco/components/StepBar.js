import React from "react"
import { connect } from "react-redux";
import { push } from 'react-router-redux';

import { Steps } from "components/renault-ui"

const StepBar = ({thisStep, match, push}) => (
    <div style={{ padding: 20 }}>
        <Steps
            steps={[
                {
                    title: "Recommandation",
                    onClick : () => push(`${match.url}/edit`)
                },
                {
                    title: "Collecte",
                    onClick : () => push(`${match.url}/advice`)
                },
            ]}
            thisStep = {thisStep}
        />
    </div>
)

export default connect(
    () => ({
    }),
    {
        push
    },
)(StepBar)

import RecoView from "./Reco"
import { connect } from "react-redux";

import { fetchReco, setReco } from "state/ducks/reco";

export default connect(
    ({ reco, caze, arca : {user} }) => ({
        reco: reco.data,
        uid: reco.uid,
        caze,
        user
    }),
    {
        fetchReco,
        setReco
    }
)(RecoView);

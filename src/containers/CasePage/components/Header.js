import React from "react";
import styled from "styled-components"

import FormLink from "../File/CaseForm/FormLink";
import Select from "components/renault-ui/Select"

import Block from "components/Block"

import * as config from "config"


const Container = styled.div`
    background-color: ${props => props.corporate ? '#0076CC' : '#ffcc33'};
    display : flex;
`

const Sides = styled.div`
    flex: 1;
    display: flex;
    align-items: center;
    width: 100%;
    padding : 10px 0;
`

const MainIcon = styled.div`
    font-size: 35px;
    margin-left : 20px;
    display: inline-block;
    text-align: center;
`

const Icon = styled.div`
`
const Button = styled.div`
    padding : 20px;
    cursor : pointer;
    margin-right : 10px;
    &:hover {
        background-color : rgba(0,0,0,.1);
    }
`

const IconSubText = styled.div`
    font-size : 10px;
    margin-top : 5px;
`

const InputField = styled.input`
    outline: none;
    border: none;
    margin-left : 20px;
    border-bottom: 2px solid ${props => props.error ? "#D02433" : "rgba(255,255,255,.5)"};
    background-color: transparent;
    padding: 10px 0;
    font-size: 26px;
    font-family : "Read", monospace;
    &:hover, &:focus {
        border-bottom: 2px solid ${props => props.error ? "#D02433" : "rgba(255,255,255,1)"};
    }
`

const HelperText = styled.div`
    position : absolute;
    padding : 3px;
    font-size : 12px;
    color : white;
    background-color : #D02433;
`

const HeaderTabButton = styled.div`
    width : 50px;
    height : 50px;
    padding : 10px;
    background-color : rgba(255,255,255,.5);
    display : flex;
    justifyContent : center;
    flex-direction : column;
    align-items : center;
    font-size : 11px;
    cursor : pointer;
    margin-left : 10px;
    border-radius : 5px;
    &:hover, &.selected {
        background-color : rgba(255,255,255,.8);
    }
`

const HeaderTabIcon = styled.div`
    font-size : 35px;
    margin-bottom : 5px;
`


const HeaderTab = ({ icon, text, selected, ...rest}) => (
    <HeaderTabButton className={selected && "selected"} {...rest}>
        <HeaderTabIcon className={icon}/>
        <div>{text}</div>
    </HeaderTabButton>
)

const Input = ({ error, helperText, ...rest }) => (
    <div>
        <InputField error={error} {...rest} />
        {error && helperText && <HelperText>{helperText}</HelperText>}
    </div>
)

export default ({ form, onConfirm, onCancel, handleInputChange, submitted, disabled, view, navigateTo }) => (
    <Container corporate = {form.corporate}>
        <Sides>
            <MainIcon>
                <Icon className={form.corporate ? "icon-ICON_OTHERS_01" : "icon-ICON_FACTORY_01"} />
                <IconSubText>{form.corporate ? "CORPORATE" : form.factoryName}</IconSubText>
            </MainIcon>
            {form.parent && <MainIcon>
                <Icon className="icon-ICON_OTHERS_01" />
                <IconSubText>CORPORATE</IconSubText>
            </MainIcon>}
            <FormLink form={form} name={"title"} submitted={submitted} disabled={disabled} required handleInputChange={handleInputChange} component={
                <Input placeholder="Example : Sujet"/>
            } />
            <Block corporateCaze authRole="PBC">
                    <HeaderTab text="Dossier" icon="icon-ICON_MEDIA_03" selected={view === "file"} onClick={()=>navigateTo(`/cases/${form.id}/file`)}/>
                    <HeaderTab text="Reco" icon="icon-ICON_MEDIA_04" selected={view === "reco"} onClick={()=>navigateTo(`/cases/${form.id}/reco`)}/>
            </Block>
        </Sides>
        <Sides style={{ justifyContent: "flex-end", marginLeft: 50 }}>
            <FormLink form={form} name={"status"} submitted={submitted} handleInputChange={handleInputChange} disabled={disabled} component={
                <Select
                    light
                    style={{ paddingRight: 10, width: 250 }}
                    label='Statut'
                    name="status"
                    items={Object.values(config.STATUS)}
                />
            } />
            <FormLink form={form} name={"classification"} submitted={submitted} handleInputChange={handleInputChange} disabled={disabled} component={
                <Select
                    style={{ paddingRight: 10, width: 250 }}
                    name="classification"
                    label="Classification"
                    light
                    items={Object.values(config.CLASSIFICATIONS)}
                />
            } />
            <Button
                onClick={onCancel}
            >
                <Icon className="icon-ICON_EDIT_05" />
            </Button>
            <div />
        </Sides>
    </Container>
)

import CasePage from './CasePage'
import { connect } from 'react-redux'
import { push } from 'react-router-redux';

import {updateCase, fetchCase, setCase, takeEdit, releaseEdit } from "state/ducks/caze"

export default connect(
    ({ router, cases, caze={}, ui : {loading, roles, caseLock} }) => ({
        router: router,
        location: router.location,
        caze,
        loading,
        roles,
        caseLock
    }),
    {
        updateCase,
        fetchCase,
        setCase,

        takeEdit,
        releaseEdit,

        navigateTo: push,

    },
)(CasePage)

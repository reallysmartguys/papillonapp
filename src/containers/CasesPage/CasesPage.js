import React from 'react'
import CaseTable from 'components/CaseTable'
import styled from "styled-components"

import { RadioToggle } from "components/renault-ui"

import Block from "components/Block"

import * as globalConfig from "config"

const Title = styled.h1`
    font-size : 30px;
    font-weight: normal;
    font-family: "Renault Life";
`

const Frame = styled.div`
    padding : 10px 20px;
`

class CasesPage extends React.Component {
    state = {
        filterClosed: "live"
    }

    componentDidMount() {
        this.props.fetchCases()
    }

    render() {
        const { cases, navigateTo } = this.props
        const { filterClosed } = this.state
        const filteredCases = !cases || cases.length===0 ? [] : cases.filter(caze => {
            return (
                (filterClosed === "live" && caze.status !== globalConfig.STATUS.CLOSED.value) ||
                (filterClosed === "closed" && caze.status === globalConfig.STATUS.CLOSED.value) ||
                (filterClosed === "all")
            )
        })
        return (
            <div>

                <Frame>
                    <Title>Dossiers</Title>

                    <RadioToggle
                        options={[
                            { label: "En cours", value: "live" },
                            { label: "Cloturés", value: "closed" },
                            { label: "Tous", value: "all" }
                        ]}
                        value={this.state.filterClosed}
                        onChange={value => this.setState({ filterClosed: value })}
                    />

                    <Block authRole="PBU">
                        <CaseTable cases={filteredCases} navigateTo={navigateTo} />
                    </Block>

                    <Block authRole="PBC">
                        <CaseTable cases={filteredCases} view={'PBC'} navigateTo={navigateTo} />
                    </Block>
                </Frame>

            </div>
        )
    }
}

export default CasesPage;

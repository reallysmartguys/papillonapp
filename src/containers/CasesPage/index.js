import CasesPage from './CasesPage'
import { connect } from 'react-redux'
import {push} from 'react-router-redux';

import {fetchCases} from "state/ducks/cases"


export default connect(
    ({router, cases=[]}) => ({router, cases}),
    {
        fetchCases,
        navigateTo : push
    },
)(CasesPage)

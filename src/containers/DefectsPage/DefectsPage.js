import React from 'react'
import DefectsTable from './components/DefectsTable'
import styled from "styled-components"

import {Title} from "components/renault-ui"

// TODO call fetch defects

const Frame = styled.div`
    padding: 10px 20px;
`

const DownloadButton = styled.div`
    font-size: 20px;
    border: 1px solid;
    padding: 5px;
    cursor: pointer;
    &:hover {
        background-color : #ffcc33;
    }
`


class defectsTable extends React.Component {

    componentDidMount() {
        this.props.fetchMyDefects()
    }

    downloadFile = () => {
        const { user } = this.props
        window.location.assign("/api/defects/stats/export?token=" + user.id);
    }

    render() {
        const { myDefects, user } = this.props

        return (
                <Frame>
                    <Title title='Defaillances actives' subtitle={user.factoryName} RightButton={<DownloadButton onClick={this.downloadFile} className="icon-ICON_DESK_03" />}/>
                    <DefectsTable shortView defects={ myDefects } />
                </Frame>
        )
    }
}

export default defectsTable;

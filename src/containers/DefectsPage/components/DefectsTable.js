import React from 'react'
import ReactTable from 'react-table'
import styled from "styled-components"

import { Select } from "components/renault-ui"

import 'react-table/react-table.css'

const offsetTop = (el) => {
    var rect = el.getBoundingClientRect(),
    scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    return rect.top + scrollTop
}

const CellDownloadButton = styled.div`
    cursor: pointer;
    position: absolute;
    top: 5px;
    right: 5px;
    display: none;
    color: #777;
    &:hover {
        color: black;
    }
`


const VinCell = (number, defectId, filter) => {

    const downloadFile = () => {
        window.location.assign(`/api/defects/${defectId}/vin?export=true&filter=${filter}`);
    }

    return (
        <div>
            <CellDownloadButton className="icon-ICON_DESK_03" onClick={downloadFile}/>
            {number}
        </div>
    )
}


const Table = styled.div`
    .ReactTable {
        border: none;
        display: block;
        margin-top: -8px;

        .rt-thead.-headerGroups {
            background-color: transparent;
            .rt-th {
                text-align: center;
                padding: 10px;
                color: white;
                font-weight: bold;
                &.black {
                    background-color: #333;
                }

                &.blue {
                    background-color: #0076cc;
                }
            }
        }

        .ReactTable .rt-thead.-header .rt-th{
            &:after {
                content: "\\E9FD";
                font-family: 'icomoon' !important;
                font-size : 15px;
                position: absolute;
                right: 10px;
                display: block;
                top: 7px;
            }
        }

        .ReactTable .rt-thead .rt-th.-sort-asc{
            box-shadow : none;
            &:after {
                content: "\\E9FD";
                font-family: 'icomoon' !important;
                font-size : 15px;
                position: absolute;
                right: 10px;
                display: block;
                top: 7px;
            }
        }

        .ReactTable .rt-thead .rt-th.-sort-desc{
            box-shadow : none;
            &::after {
                content: "\\e9ff";
                font-family: 'icomoon' !important;
                font-size : 15px;
                position: absolute;
                right: 10px;
                display: block;
                top: 7px;
            }
        }

        .rt-tbody {
            ${props => !props.expand && props.thisComponent && `max-height: ${window.innerHeight - offsetTop(props.thisComponent) - 160}px;`}
            ${props => !props.expand && "border: 1px solid #777;"}
        }

    }
    .rt-tr{
        text-align: left !important;
    }
    .rt-tr-group .rt-tr{
        &:hover {
            color: #0076cc;
            .grey {
                background-color: transparent;
            }
        }
    }
    .rt-th.-sort-asc {
        box-shadow: inset 0 -3px 0 0 rgba(255,255,255,0.6);
    }
    .rt-thead.-header{
        color: black;
    }
    .rt-td{
        align-items: center;
        display: flex;
        padding: 5px 10px;
        position: relative;
        &:hover ${CellDownloadButton} {
            display: block;
        }
    }

    .rt-thead.-filters{
        background-color: white;
        box-shadow: 0px 3px 3px rgba(0,0,0,.2);
        z-index: 10;
        .rt-th {
            padding: 5px 15px;
            overflow: visible !important;
        }
        input {
            background-color: transparent;
            outline: none;
            border: none;
            border-bottom : 1px solid rgba(0,0,0,1);
            padding: 5px 0;
            font-family: "Read", monospace;
            &:hover {
                border-bottom: 1px solid rgba(0,0,0,1);
            }
            &:focus {
                border-bottom: 1px solid rgba(0,0,0,1);
            }
        }
    }

    .rt-table {
        overflow: visible !important;
        font-size: 15px;
    }

    .ReactTable .rt-thead.-header .rt-th{
        &:after {
            content: "\\E9FD";
            font-family: 'icomoon' !important;
            font-size : 15px;
            position: absolute;
            right: 10px;
            display: block;
            top: 7px;
        }
    }

    .ReactTable .rt-thead .rt-th.-sort-asc{
        box-shadow : none;
        &:after {
            content: "\\E9FD";
            font-family: 'icomoon' !important;
            font-size : 15px;
            position: absolute;
            right: 10px;
            display: block;
            top: 7px;
        }
    }

    .ReactTable .rt-thead .rt-th.-sort-desc{
        box-shadow : none;
        &::after {
            content: "\\e9ff";
            font-family: 'icomoon' !important;
            font-size : 15px;
            position: absolute;
            right: 10px;
            display: block;
            top: 7px;
        }
    }

    .rt-tbody{
        margin-right: ${props => props.expand !== true && "-15px"};
    }

    .white{
        background-color: white;
    }

    .grey {
        background-color: #f7f7f7;
    }

    .dark-grey {
        background-color: #e6e6e6;
    }
`

const columns = [
    {
        Header: '',
        headerClassName: 'hidden',
        columns: [
            {
                Header: 'Id',
                id: "id",
                accessor: "defectId",
                Cell: ({value}) => `${value}`,
                style:{fontSize: 12},
                width: 60,
                filterable: true
            },
            {
                Header: 'Nom',
                id: "defectName",
                accessor: 'defectName',
                Cell: ({ original: {defectName} }) => `${defectName}`,
                style:{fontSize: 13},
                width: 100,
                filterable: true,
                filterMethod: (filter, {_original}) => {
                    return _original[filter.id] !== undefined ?
                        String(_original[filter.id]).match(new RegExp(filter.value, 'i') ): true
                }
            },
            {
                Header: 'Libellé',
                id: "defectLabel",
                accessor: 'defectLabel',
                Cell: ({ original }) => TitleCell(original),
                filterable: true,
                filterMethod: (filter, {_original}) => {
                    return _original[filter.id] !== undefined ?
                        String(_original[filter.id]).match(new RegExp(filter.value, 'i') ): true
                }
            },
            {
                Header: 'Nombre de véhicules',
                id: "total",
                accessor: "total",
                className: "grey",
                headerClassName:"grey",
                Cell: ({ value, original: {defectId} }) => TotalCell(value, defectId) },
        ]
    },
    {
        Header: 'Etat de véhicules sur CLE', headerClassName: 'blue', columns: [
            { Header: 'Demande Blocage', id: "dbl", style: { color: "#777", fontSize: 18, justifyContent: "center" }, accessor: "dbl", Cell: ({value, original: {defectId}}) => VinCell(value, defectId, "blocked")},
            { Header: 'Demande Déblocage', id: "ddb", className: "grey", headerClassName:"grey", style: { color: "#777", fontSize: 18, justifyContent: "center" }, accessor: "ddb", Cell: ({value, original: {defectId}}) => VinCell(value, defectId, "unblocked") },
            {
                Header: 'A Débloquer',
                id: "adbl",
                Cell: ({value, original: {defectId}}) => VinCell(value, defectId, "toRelease"),
                style: { color: "#d02433", fontSize: 18, justifyContent: "center" },
                accessor: "adbl",
                filterable: true,
                filterMethod: (filter, { _original: { adbl } }) => filter.value === "all" || (filter.value === "0" && adbl === 0) || (filter.value === "1" && adbl > 0),
                Filter: ({ filter, onChange }) =>
                    <Select
                        onChange={value => onChange(value)}
                        value={filter ? filter.value: "all"}
                        ultraLight
                        items={[
                            { label: "Tous", value: "all" },
                            { label: "Vide", value: "0" },
                            { label: "Non vide", value: "1" },
                        ]} />
            },
        ]
    },
    {
        Header: 'Hors CLE', headerClassName: 'black', columns: [
            {
                Header: 'Expédiés',
                id: "sent",
                className: "dark-grey",
                headerClassName:"dark-grey",
                Cell: ({value, original: {defectId}}) => VinCell(value, defectId, "sent"),
                accessor: "sent",
                style: { fontSize: 18, justifyContent: "center" },
                filterable: true,
                filterMethod: (filter, { _original: { sent } }) => filter.value === "all" || (filter.value === "0" && sent === 0) || (filter.value === "1" && sent > 0),
                Filter: ({ filter, onChange }) =>
                    <Select
                        onChange={value => onChange(value)}
                        value={filter ? filter.value: "all"}
                        ultraLight
                        items={[
                            { label: "Tous", value: "all" },
                            { label: "Vide", value: "0" },
                            { label: "Non vide", value: "1" },
                        ]} />
            },
        ]
    }
];


const TitleCell = ({ defectName, defectLabel, defectId, ddb }) => (
    <div style={{ width: "100%" }}>
        <div style={{ color: "#0076cc", fontSize: 17, fontFamily: "Renault Life" }} >{defectLabel}</div>
    </div>
)

const TotalCell = (number, defectId) => (
    <div style={{ fontSize: 20, paddingLeft: 20, width: "100%", display: "flex" }}>
        <span style={{
            fontSize: 30,
            marginRight: "auto",
            transform: "translateY(-5px)",
            display: "inline-block"
        }} className="icon-ICON_VEHICULE_01" />
        <span style={{ color: "#0076cc", marginRight: 20, marginTop: 5 }}>{VinCell(number, defectId, "total")}</span>
    </div>
)


const SeeMore = styled.div`
    font-size: 15px;
    padding: 9px;
    color: #777;
    border: 1px solid;
    border-radius: 100%;
    left: 50%;
    transform: translateX(-50%);
    position: absolute;
    background-color: white;
    cursor: pointer;
    margin-top: -10px;
    margin-bottom: 10px;
    &:hover {
        color: black;
    }
`

export default class extends React.Component {
    state = {
        expand: false
    }

    render(){
        const {defects, shortView} = this.props
        const { expand } = this.state
        const curatedDefects = defects.map(defect => ({...defect, adbl: defect.dbl - defect.ddb}) )
        return (
            <Table expand={shortView ? expand: true} innerRef={component => this.component = component} thisComponent={this.component}>
                <ReactTable
                    filterable={false}
                    className="-highlight"
                    data={curatedDefects}
                    columns={columns}
                    showPagination={false}
                    defaultSorted={[{ id: "adbl", desc: true }]}
                    minRows={0}
                />
                { shortView && <SeeMore onClick={e => this.setState({ expand: !expand })} className={expand ? "icon-ICON_ARROW_06": "icon-ICON_ARROW_08"} />}
            </Table>
        )
    }
}

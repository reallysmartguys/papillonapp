import DefectsPage from './DefectsPage'
import { connect } from 'react-redux'

import { fetchMyDefects } from "state/ducks/defects"

export default connect(
    ({ defects: { myDefects }, arca: { user } }) => ({
        myDefects,
        user
    }),
    {
        fetchMyDefects
    },
)(DefectsPage)

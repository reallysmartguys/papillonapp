import React from 'react'
import CaseTable from 'components/CaseTable'
import styled from "styled-components"

import Block from "components/Block"
import * as globalConfig from "config"

const Title = styled.h1`
    font-size : 30px;
    font-weight: normal;
    font-family: "Renault Life";
`

const Frame = styled.div`
    padding : 10px 20px;
`

class HomePage extends React.Component {
    componentDidMount() {
        this.props.fetchCases()
    }

    render() {
        const { cases, navigateTo } = this.props
        const filteredCases = cases.filter(caze => caze.status !== globalConfig.STATUS.CLOSED.value)
        return (
            <div>
                <Block authRole="PBU">
                    <Frame>
                        <Title>Dossiers en cours d'instruction</Title>
                        <CaseTable cases={filteredCases} navigateTo={navigateTo} shortView={7} />
                    </Frame>
                </Block>

                <Block authRole="PBC">
                    <Frame>
                        <Title>Dossiers corporate en cours d'instructions</Title>
                        <CaseTable cases={filteredCases}  view={'PBC'} navigateTo={navigateTo} shortView={7} />
                    </Frame>

                </Block>
            </div>
        )
    }
}

export default HomePage;

import HomePage from './HomePage'
import { connect } from 'react-redux'
import {push} from 'react-router-redux';

import * as casesActions from "state/ducks/cases"

const filterCases = (cases, roles) => {
    return cases.filter(caze => roles.includes('PBU') && !caze.corporate || roles.includes('PBC') && caze.corporate)
}

export default connect(
    ({router, cases, ui}) => ({
        location: router.location,
        cases : filterCases(cases, ui.roles)
    }),
    {fetchCases : casesActions.fetchCases,
    navigateTo : push}
)(HomePage)


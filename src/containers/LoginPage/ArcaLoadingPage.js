import React from 'react'
import {CircularProgress} from 'material-ui/Progress'
import { connect } from 'react-redux'
import {login} from "state/ducks/arca"
import { push } from 'react-router-redux'


class ArcaLoadingPage extends React.PureComponent {
    componentDidMount() {
        const {location} = this.props;
        fetch(`/api/auth/userInfoByCode${location.search}`)
            .then(resp => resp.json())
            .then(user => {
                this.props.login(user.id);
                this.props.navigateTo('/');
            })
    }

    render(){
        return [
            'ARCA authentication processing....',
            <CircularProgress color="accent" />
        ]
    }
}

export default connect(
    state => ({}),
    {
        login,
        navigateTo: push
    }
)(ArcaLoadingPage)

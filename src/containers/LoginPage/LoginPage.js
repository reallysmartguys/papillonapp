import React from 'react'
import { Paper, List, ListItem, ListItemIcon, ListItemText, Icon } from 'material-ui'
import { withStyles } from "material-ui/styles"
import loginBg from "assets/images/loginBg.jpg"
import {formatName} from 'tools'

const styles = theme => ({
    paper: {
        width: 300,
        margin: [200, "auto"]
    }

});


const idpLogin = () => {
    let token = localStorage.getItem('token');
    if(token){
        console.log(`token ${token} already exist !!`)
    }
    else {
        window.location.href = '/'
    }
}

class LoginPage extends React.Component {

    componentDidMount() {
        const locationState = this.props.location.state
        if (this.props.user) this.props.navigateTo(locationState ? locationState.from.pathname : '/')
        this.props.fetchUsers() // Delete when login done
    }
    componentDidUpdate() {
        const locationState = this.props.location.state
        if (this.props.user) this.props.navigateTo(locationState ? locationState.from.pathname : '/')
    }

    render() {
        const { classes, login, user, users } = this.props

        if (user) return null
        return (
            <div style={{width : "100vw", height : "100vh", position : "fixed", top : 0, left : 0, backgroundImage : `url(${loginBg})`, backgroundSize : "cover", backgroundPosition : "center"}}>
                <Paper className={classes.paper}>
                    <List data-e2b-id="loginList">
                        <ListItem button key='idp-renault' onClick={idpLogin}>
                                <ListItemIcon>
                                    <Icon className="icon-ICON_USER_01" />
                                </ListItemIcon>
                                <ListItemText primary="login" />
                        </ListItem>

                    </List>
                </Paper>
            </div>
        )
    }
}

export default withStyles(styles)(LoginPage);

import LoginPage from './LoginPage'
import ArcaLoadingPage from './ArcaLoadingPage'
import { connect } from 'react-redux'
import { push } from 'react-router-redux';

import {login, fetchUsers} from "state/ducks/arca"

export {ArcaLoadingPage}

export default connect(
    ({ router }) => ({
        location: router.location
    }),
    {
        navigateTo: push,
        login,
        fetchUsers
    }
)(LoginPage)


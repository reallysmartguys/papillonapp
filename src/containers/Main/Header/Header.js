import React from 'react';
import { withStyles, Typography } from "material-ui";
import TabsHeader from "./TabsHeader"

import * as globalConfig from "config"
import styled from "styled-components"

import Block from "components/Block"

import HeaderButton from "./HeaderButton"
import { DropDown, List, Notification } from "components/renault-ui"

import {formatName} from "tools"

const styles = theme => ({
    wrap: {
        display: "flex",
        flexDirection: "row",
        padding: [10, 0]
    },
    left: {
        textAlign: "left",
        width: "100%",
        flex: 1,
        paddingLeft: 20
    },
    right: {
        flex: 1,
        display : "flex",
        flexDirection : "row",
        justifyContent : "flex-end"
    },
    center: {
        width: 200,
        textAlign: "center"
    }
})

const PlusButton = styled.div`
    font-size: 20px;
    padding: 10px;
    border: 1px solid #ffcc33;
    color: #222;
    cursor: pointer;
    transition : .2s;
    &:hover {
        border: 1px solid #222;
        background-color : #ffcc33;
    }
`

const NotifMenu = styled.div`
    padding : 5px;
`

const EmptyMessage = styled.div`
    padding : 15px;
    color : #ccc;
    text-align : center;
    white-space : nowrap;

`

class Header extends React.Component {

    render() {
        const { classes, notifications, user = {}, toggleModal, addCase, navigateTo, deleteNotification } = this.props
        return (
            <div>
                <header className={classes.wrap}>
                    <div className={classes.left}>
                        <DropDown button={<PlusButton data-e2b-id="plus-button" className="icon-ICON_MENU_15" />}>
                            <List data-e2b-id="plus-button-list" iconRight items={[
                                {
                                    label: "Nouveau Dossier Client", icon: "icon-ICON_FACTORY_01", onClick: () => toggleModal({
                                        show: true,
                                        type: "newCazeModal",
                                        props: {
                                            required: true,
                                            title: "Nouveau Dossier Client",
                                            onConfirm: title => addCase({ ...globalConfig.caze, clientFirstName: title })
                                        }
                                    })
                                }
                            ]} />
                        </DropDown>
                    </div>
                    <div className={classes.center}>
                        <Typography type="title" color="inherit">E.E. - CRM Papillon</Typography>
                        <Typography type="subheading" color="inherit" data-e2b-id="greetings-text">Bonjour {formatName(user)}</Typography>
                    </div>
                    <div className={classes.right}>
                        <HeaderButton label="Message" icon="icon-ICON_COMMUNICATION_02" />
                        <DropDown right button={<HeaderButton label="Notifications" count={notifications.length} icon="icon-ICON_TIME_03" />} >
                            <NotifMenu>
                                {!notifications.length && <EmptyMessage>Aucune notification</EmptyMessage>}
                                {notifications.map((notif, id) => <Notification key={id} onClick={(e) => {
                                    deleteNotification(notif)
                                    navigateTo(`/cases/${notif.meta.caseId}`)
                                }}
                                    onClose={() => deleteNotification(notif)}
                                    notification={notif} />)}
                            </NotifMenu>
                        </DropDown>
                        <HeaderButton label="Profil" icon="icon-ICON_USER_01" />
                        <HeaderButton label="Reglage" icon="icon-ICON_MECHANICAL_01" />
                        <HeaderButton label="Help" icon="icon-ICON_INFO_03" />
                    </div>
                </header>
                <div>
                    <Block authRole={["PBC","PBU","ADMIN"]}>
                        <TabsHeader roles={user.roles} />
                    </Block>
                </div>
            </div>
        )
    }
}


export default withStyles(styles)(Header);

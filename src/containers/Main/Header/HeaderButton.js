import React from "react"
import { headShake } from 'react-animations'
import styled, { keyframes } from "styled-components"

const HeadShake = keyframes`${headShake}`

const Wrap = styled.div`
    text-align: center;
    display: inline-block;
    cursor: pointer;
    margin: 0px 20px;
    font-family: "Read",sans-serif;
`

const Icon = styled.div`
    font-size: 20px;
    display: inline-block;
    position: relative;
    &.animate {
        animation : .5s ${HeadShake};
    }
`

const Label = styled.div`
    font-size : 12px;
    margin-top : 5px;
    color : #1f77de
`

const Badge = styled.div`
    font-size: 9px;
    padding: 4px 6px 2px 6px;
    color: white;
    border-radius: 100%;
    border: 1px solid white;
    position: absolute;
    bottom: 0;
    right: -7px;
    background-color: #d02433;
    font-family: inherit;
`

class HeaderButton extends React.Component {
    state = {
            animated: false,
            animation: null
        }

    componentDidUpdate(lprops) {
        const { count } = this.props
        const { animated } = this.state

        if (!animated) this.setState({ animated: true })
        else if (count && animated && lprops.count < count) this.setState({ animation: true },
            () => setTimeout(
                () => this.setState({ animation: false }), 1000
            )
        )
    }
    render() {
        const { icon, count, label, onClick } = this.props
        return (
            <Wrap onClick={onClick}>
                <Icon className={this.state.animation && "animate"}>
                    {typeof icon === "string" ? <span className={icon} /> : icon}
                    {count ? <Badge>{count}</Badge> : null}
                </Icon>
                <Label>{label}</Label>
            </Wrap>
        )
    }
}

export default HeaderButton;

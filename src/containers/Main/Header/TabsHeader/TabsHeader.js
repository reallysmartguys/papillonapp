import React from 'react';
import {Tab, Tabs, withStyles} from "material-ui";

const styles = theme => ({
    tabs : {
        backgroundColor : "#333333",
        color : "white",
        borderTop : "2px solid #ffcc33",
        borderBottom : "2px solid #ffcc33"
    }
})

const TabsHeader = ({tab, roles, changePage, classes}) => {
    let value = /\/cases\/.+/.test(tab) ? "/cases" : tab;
    return <div>
        <Tabs id="TabsHeader" className={classes.tabs} value={value} indicatorColor="#ffcc33" onChange={(event, value) => changePage(value)}>
            <Tab data-e2b-id="top-tab-dashboard" value="/" label="Dashboard"/>
            {roles && roles.includes("PBC") && <Tab data-e2b-id="top-tab-requests" value="/requests" label="Demandes"/>}
            {roles && roles.includes("PBU") && <Tab data-e2b-id="top-tab-defects" value="/defects" label="Défailllances"/>}
            {roles && roles.includes("ADMIN") && <Tab data-e2b-id="top-tab-admin" value="/admin" label="Administration"/>}
            <Tab data-e2b-id="top-tab-cases" value="/cases" label="Dossiers"/>
        </Tabs>
    </div>
}


export default withStyles(styles)(TabsHeader);

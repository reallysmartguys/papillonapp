import TabsHeader from './TabsHeader';
import {connect} from "react-redux";
import {push} from 'react-router-redux';

export default connect(
    ({router}) => ({
        tab: router.location.pathname,
    }),
    {changePage: push}
)(TabsHeader)

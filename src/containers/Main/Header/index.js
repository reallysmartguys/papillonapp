import Header from './Header';
import { connect } from "react-redux";
import { push } from 'react-router-redux';

import { fetchNotifications, deleteNotification } from "state/ducks/notifications"
import { toggleModal } from "state/ducks/ui"
import { addCase } from "state/ducks/caze"

const connectedUser= {}
export default connect(
    ({ router, notifications, arca }) => ({
        tab: router.location.pathname,
        notifications: notifications.filter(notif => !notif.read),
        user: connectedUser
    }),
    {
        navigateTo: push,
        fetchNotifications,
        deleteNotification,
        toggleModal,
        addCase
    }
)(Header)

import React from 'react';
import styled, {keyframes} from "styled-components"

const spinAnimation = keyframes`
0% {
    transform : rotate(0deg) ;
}
100% {
    transform : rotate(360deg) ;
}
`

const LoadingScreen = styled.div`
    position : fixed;
    top : 0;
    left : 0;
    width : 100vw;
    height : 100vh;
    z-index : 1000;
    background-color : rgba(255,255,255,.5);
    display : flex;
    justify-content : center;
    align-items : center;
`

const Loader = styled.div`
    font-size : 70px;
    animation: 2s ${spinAnimation} linear infinite;
`

const LoadingContainer = ({loading}) => {
    return loading ? <LoadingScreen data-e2b-id="loading-screen">
        <Loader className="icon-ICON_WHEELS_01"></Loader>
    </LoadingScreen> :
    null
};

export default LoadingContainer;

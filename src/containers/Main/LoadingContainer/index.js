import LoadingContainer from './LoadingContainer'
import { connect } from 'react-redux'

export default connect(
    ({ ui }) => ({
        loading : ui.loading
    })
)(LoadingContainer)

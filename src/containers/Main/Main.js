import React from "react";
import { Route, Redirect, Switch } from "react-router-dom";
import HomePage from "containers/HomePage";
import RequestPage from "containers/RequestPage";
import DefectsPage from "containers/DefectsPage";
import CasesPage from "containers/CasesPage";
import CasePage from "containers/CasePage";
import AdminPage from 'containers/AdminPage';

import Header from "./Header";

import { logout } from "state/ducks/arca";
import LoginPage, { ArcaLoadingPage } from "containers/LoginPage";
import AuthRoute from "./components/AuthRoute";
import ModalContainer from "./ModalContainer";
import LoadingContainer from "./LoadingContainer";

const Main = () => (
    <div
        style={{
            fontFamily: '"Read"',
            minHeight: "100vh",
            display: "flex",
            flexDirection: "column"
        }}
    >
        <ModalContainer />
        <LoadingContainer />
        <Switch>

            <Route exact path="/login" component={LoginPage} />
            <Route
                exact
                path="/logout"
                component={() => {
                    logout();
                    return <Redirect to={"/login"} />;
                }}
            />

            <Route
                path="/"
                render={() => (
                    <div style={{ flex: 1, display: "flex", flexDirection: "column" }}>
                        <Header />
                        <main style={{ flex: 1, display: "flex", flexDirection: "column" }}>
                            <AuthRoute authRole={["ADMIN"]} exact path="/admin" component={AdminPage} />
                            <Route exact path="/authenticated" component={ArcaLoadingPage} />
                            <Route exact path="/" component={HomePage} />
                            <Route authRole={["PBC"]} exact path="/requests" component={RequestPage} />
                            <Route authRole={["PBU"]} exact path="/defects" component={DefectsPage} />
                            <Route exact path="/cases" component={CasesPage} />
                            <Route path="/cases/:id" component={CasePage} />
                        </main>
                    </div>
                )}
            />
        </Switch>
    </div>
);

export default Main;

import React, { Component } from 'react';
import Modal from "components/renault-ui/Overlay"
import * as modals from "./components/modals"

class ModalContainer extends Component {
    state = {
        value: "",
    }

    onClose = () => {
        this.setState({ value: "" })
        this.props.toggleModal(false)
    }

    onConfirm = () => {
        const {props = {}} = this.props.modal
        props.onConfirm && props.onConfirm(this.state.value)
        this.onClose();
    }

    onKeyDown = event => {
        if (event.key === 'Enter' ) {
            const {props = {}} = this.props.modal;
            if (this.state.value || !props.required)
                this.onConfirm();
        }
    }

    render() {
        if (!this.props.modal.show) return null;

        const { props = {}, type = "" } = this.props.modal;
        const { value } = this.state;
        const TargetModal = modals[type];

        return (
            <Modal {...props} disabled={props.required && !value} onClose={this.onClose} onConfirm={this.onConfirm} dataE2bId="modal" >
                {
                    TargetModal ?
                    <TargetModal value={value} onChange={value => this.setState({ value })} onKeyDown={this.onKeyDown} />
                    : null
                }
            </Modal>
            )
    }
}

export default ModalContainer;

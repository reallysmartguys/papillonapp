import React from "react"
import { Input, RadioToggle, TextArea } from "components/renault-ui"

export const beforeShare = ({ value, onChange }) => (
     <div style={{ textAlign: "center", marginTop: 20 }}>
        <RadioToggle
            options={[
                { label: "faible", value: "info", color : "lightWarning" },
                { label: "haute", value: "warning", color : "warning"  },
                { label: "maximale", value: "danger", color : "danger"  }
            ]}
            value={value}
            onChange={onChange}
            fullWidth
        />
    </div>
)

export const noFollowModal = ({ value, onChange }) => (
    <TextArea
        label="Commentaire"
        value={value}
        onChange={onChange}
        rows={4}
    />
)

export class newCazeModal extends React.PureComponent {
    componentDidMount() {
        this.input.focus();
    }

    render() {
        const {value, onChange, onKeyDown} = this.props;
        return <Input
            label="Sujet du dossier"
            value={value}
            onChange={onChange}
            onKeyDown={onKeyDown}
            innerRef={input => this.input = input}
        />
    }
}

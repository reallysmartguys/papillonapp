import ModalContainer from "./ModalContainer"
import { connect } from 'react-redux'

import { toggleModal } from "state/ducks/ui"
import { addCase } from "state/ducks/caze"

export default connect(
    ({ router, ui : {modal} }) => ({
        location: router.location,
        modal
    }),
    {
        toggleModal,
        addCase
    }
)(ModalContainer)


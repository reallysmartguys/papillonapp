import React from 'react';
import { connect } from 'react-redux';
import { Route, Redirect, withRouter } from 'react-router-dom';


const AuthRoute = ({ component: Component, user, authRole, ...rest }) => (
    <Route
        {...rest}
        render={props => {
            if(!user) return <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
            else if(authRole){
                if(authRole.some(role => user.roles.includes(role))) return <Component {...props} />
                else return <Redirect to={{ pathname: '/'}} />
            }
            return <Component {...props} />
        }}
    />
)

export default withRouter(
    connect(
        ({user}) => ({user}),
         null
        )(AuthRoute)
);

import React from 'react'
import styled from "styled-components"

import SharedTable from "./components/SharedTable"
import {Title} from "components/renault-ui"

const Frame = styled.div`
    padding : 20px;
`

class RequestPage extends React.Component {
    componentDidMount() {
        this.props.fetchCases()
    }

    render() {
        const { cases, navigateTo } = this.props
        return (
            <div>
                    <Frame>
                        <Title title="Demandes blocage corporate" subtitle="Usines et OTS"/>
                        <SharedTable cases={cases} goTo={id => navigateTo("cases/"+id)}/>
                    </Frame>
            </div>
        )
    }
}

export default RequestPage;

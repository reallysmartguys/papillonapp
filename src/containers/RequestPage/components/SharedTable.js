import React from 'react'
import ReactTable from 'react-table'
import styled from "styled-components"
import moment from "moment"

import * as globalConfig from "config"
import { formatName } from "tools"
import { Select } from "components/renault-ui"

import 'react-table/react-table.css'

const Table = styled.div`
    .ReactTable {
        border : none;
        display : block;
    }
    .rt-tr{
        text-align : left !important;
    }
    .rt-tr-group .rt-tr{
        padding : 5px 0px;
        &:hover {
            color : #0076cc;
        }
    }
    .rt-th.-sort-asc {
        box-shadow: inset 0 -3px 0 0 rgba(255,255,255,0.6);
    }
    .rt-thead.-header{
        background-color : #e1e1e1;
        border-top : 1px solid #ccc;
        border-bottom : 1px solid #ccc;
        color : black;
        .rt-tr {
            padding : 5px 0px;
        }
    }
    .rt-td{
        align-items : center;
        display : flex;
    }

    .rt-thead.-filters{
        background-color : white;
        border-bottom : 1px solid #ccc;
        .rt-th {
            padding : 5px 15px;
            overflow : visible !important;
        }
        input {
            background-color : transparent;
            outline : none;
            border : none;
            border-bottom : 1px solid rgba(0,0,0,1);
            padding : 5px 0;
            font-family : "Read", monospace;
            &:hover {
                border-bottom : 1px solid rgba(0,0,0,1);
            }
            &:focus {
                border-bottom : 1px solid rgba(0,0,0,1);
            }
        }
    }

    .rt-table {
        overflow : visible !important;
        font-size : 15px;
    }

    .ReactTable .rt-thead.-header .rt-th{
        &:after {
            content: "\\E9FD";
            font-family: 'icomoon' !important;
            font-size : 15px;
            position: absolute;
            right: 10px;
            display: block;
            top: 7px;
        }
    }

    .ReactTable .rt-thead .rt-th.-sort-asc{
        box-shadow : none;
        &:after {
            content: "\\E9FD";
            font-family: 'icomoon' !important;
            font-size : 15px;
            position: absolute;
            right: 10px;
            display: block;
            top: 7px;
        }
    }

    .ReactTable .rt-thead .rt-th.-sort-desc{
        box-shadow : none;
        &::after {
            content: "\\e9ff";
            font-family: 'icomoon' !important;
            font-size : 15px;
            position: absolute;
            right: 10px;
            display: block;
            top: 7px;
        }
    }
`

const CaseTitle = styled.div`
    cursor : pointer;
    font-size : 18px;
    margin-bottom : 5px;
`
const CaseNumber = styled.div`
    font-size : 13px;
    color : #0076cc;
`
const AuthorRole = styled.div`
    font-size : 12px;
    color : #999;
`
const Icon = styled.span`
    padding : 10px;
    color : ${props => props.color || "inherit"};
    font-size : 20px;
`
const ActionColumnCell = styled.div`
    display : flex;
    justify-content : flex-end;
    width : 100%;
    font-size : 20px;
    ${Icon} {
        color : #ccc;
        cursor : pointer;
        &:hover {
            color : black;
        }
    }
`
const Factory = styled.div`
    color : #000;
`

const TitleCell = styled.div`
    display : flex;
    flex-direction : column;
    cursor : pointer;
    &>div {
        margin-right : 10px;
    }
`

const ActionColumn = () => (
    <ActionColumnCell>
        <Icon className="icon-ICON_INFO_06" />
    </ActionColumnCell>
)

export default ({ cases, goTo }) => {

    const columns = [
        { Header: '', accessor: 'id', Cell: ({ value: id }) => <Icon className="icon-ICON_FACTORY_01"/>, width: 70, filterable: false },
        {
            Header: 'Dossier', Cell: ({ original: { title, id } }) => <TitleCell onClick={() => goTo(id)}>
                    <CaseTitle>{title}</CaseTitle>
                    <CaseNumber>n°{id}</CaseNumber>
            </TitleCell>,
            filterMethod: ({ value }, { original: { title, id } }) => !value || (title + id).match(new RegExp(value, 'i'))
        },
        { id : 'factory' , accessor: 'factoryName', Cell: ({ value }) => <Factory>{value}</Factory> },
        {
            accessor: 'author', Header: 'PBU', Cell: ({ value }) => <div>
                <div>{formatName(value)}</div>
                <AuthorRole>{value && value.roles}</AuthorRole>
            </div>
        },
        { id: "creation", Header: 'Création', accessor: row => moment(row.creationDate).format('DD/MM/YYYY') },
        {
            accessor: 'criticality',
            Header: 'Criticité',
            Cell: ({ value }) => {
                return <Icon color={globalConfig.colors[value]} className="icon-ICON_OTHERS_01" />
            },
            width: 150,
            filterMethod: (filter, row) => filter.value === "all" || filter.value === row[filter.id],
            Filter: ({ filter, onChange }) =>
                <Select
                    onChange={value => onChange(value)}
                    value={filter ? filter.value : "all"}
                    ultraLight
                    items={[
                        { label: "Tous", value: "all" },
                        { label: "Maximale", value: "danger" },
                        { label: "Haute", value: "warning" },
                        { label: "Faible", value: "lightWarning" },
                    ]} />
        },
        { Header: '', accessor: 'id', Cell: ({ value: id }) => ActionColumn(), width: 100, filterable: false },

    ];

    return (
        <Table>
            <ReactTable
                filterable={true}
                className="-striped -highlight"
                data={cases}
                columns={columns}
                showPagination={false}
                minRows={0}
                defaultFilterMethod={(filter, row, column) => {
                    const id = filter.pivotId || filter.id
                    return row[id] !== undefined ?
                        String(row[id]).match(new RegExp(filter.value, 'i')) : true
                }}
            />
        </Table>
    )
}

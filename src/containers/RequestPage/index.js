import RequestPage from './RequestPage'
import { connect } from 'react-redux'
import {push} from 'react-router-redux';

import {STATUS} from "config"

import * as casesActions from "state/ducks/cases"

export default connect(
    ({router, cases, ui}) => ({
        location: router.location,
        cases : cases.filter(caze => !caze.corporate && caze.shared && !caze.parent && caze.status!== STATUS.CLOSED.value)
    }),
    {fetchCases : casesActions.fetchCases,
    navigateTo : push}
)(RequestPage)

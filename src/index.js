import 'core-js'
import React from 'react'
import ReactDOM from 'react-dom'
import App from 'containers/App'
import { Provider } from 'react-redux'
import configureStore from "state/store"
//import {init as initWS} from 'sock-stomp'
// import registerServiceWorker from './registerServiceWorker';

import 'assets/renault-icon-font/style.css'
import 'assets/renault-font-read/style.css'
import 'assets/renault-font-life/style.css'
import 'assets/reset.css'

const store = configureStore({});

const rootEl = document.getElementById('root');
const ProviderApp = () => (
    <Provider store={store}>
        <App />
    </Provider>
)

ReactDOM.render(<ProviderApp/>, rootEl);
//initWS(store);
// registerServiceWorker();

if (process.env.NODE_ENV === 'development' && module.hot) {
    module.hot.accept('containers/App', () => {
        ReactDOM.render(<ProviderApp/>, rootEl);
    })
  }

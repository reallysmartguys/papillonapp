import { toggleLoading, setRole } from "state/ducks/ui"
import {fetchNotifications} from "state/ducks/notifications"
import { client as stompClient} from 'sock-stomp'

const LOGIN = "app/arca/LOGIN"
const LOGOUT = "app/arca/LOGOUT"
const FETCH_USERS = "app/arca/FETCH_USERS"
const UPDATE_USER = "app/arca/UPDATE_USER"

export const ACTIONS = { LOGIN, LOGOUT, FETCH_USERS }

export default function reducer(state = { users: []}, action = {}) {
    switch (action.type) {
        case LOGIN:
            return { ...state, user: action.user};
        case LOGOUT:
            return { ...state, user: undefined};
        case FETCH_USERS:
            return { ...state, users: action.users };
        case UPDATE_USER:
            return {
                ...state,
                users: state.users.map(user => user.id === action.user.id ? action.user: user)
            }
        default:
            return state;
    }
}

export const login = id => ({
    type: "API",
    payload: {
        url: `/api/users/${id}`
    },
    then: user => dispatch => {
        localStorage.setItem('papillonToken', user.id);
        dispatch( setRole(user.roles || ["PBU"]) )
        dispatch({ type: LOGIN, user })
        dispatch( fetchNotifications() )
        stompClient.send("/connected",{},user.id);

    }
})


export const logout = id => dispatch => {
    localStorage.removeItem('papillonToken');
    dispatch({ type: LOGOUT })
}

export const updateUser = user => ({
    type: "API",
    payload: {
        url: `/api/users/${user.id}`,
        method: 'PUT',
        body: user
    },
    then: user => dispatch => {
        dispatch({ type: UPDATE_USER, user })
    }
})

// Delete once user login done
export const fetchUsers = () => ({
    type: "API",
    payload: {
        url: `/api/users`
    },
    sides: {
        progress: toggleLoading(true),
        error: toggleLoading(false),
        success: toggleLoading(false),
    },
    then: users => ({ type: FETCH_USERS, users : users || [] })
})

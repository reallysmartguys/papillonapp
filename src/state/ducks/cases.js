import {casesList} from "./caseDao";

const FETCH_CASES = "app/cases/FETCH_CASES";

export const ACTIONS = {FETCH_CASES}

export default function reducer(state = [], action = {}) {
    switch (action.type) {
        case FETCH_CASES:
            return action.cases;
        default:
            return state;
    }
}

export const fetchCases = () =>
    ({type: FETCH_CASES, cases: casesList})
import {push} from "react-router-redux";
import {toggleLoading, setCaseLock} from "state/ducks/ui";
import {addCase as cases_addCase, findCase}  from "./caseDao";

const FETCH_CASE = "FETCH_CASE";
const UPDATE_FILES = "UPDATE_FILES";
const SET_CHILDREN = "SET_CHILDREN";

export const ACTIONS = {FETCH_CASE, UPDATE_FILES, SET_CHILDREN};

export default function reducer(state = null, action = {}) {
  let {type, caze, files, children, parentId} = action;
  switch (type) {
    case FETCH_CASE:
      return caze;
    case UPDATE_FILES:
      return {...state, files};
    case SET_CHILDREN:
      if (parentId === state.id) return {...state, children};
    default:
      return state;
  }
}



export const addCase = newCaze => {

  return dispatch => {
    let caze = cases_addCase(newCaze);
    dispatch(push("/cases/" + caze.id));
    return(setCase(caze));
  }
};
export const setCase = caze => ({type: FETCH_CASE, caze});

export const updateCase = updatedCaze => dispatch =>
    new Promise((res, rej) => dispatch({
      type: "API",
      payload: {
        url: `/api/cases/${updatedCaze.id}`,
        method: "PUT",
        body: updatedCaze
      },
      sideActions: {
        progress: toggleLoading(true),
        error: toggleLoading(false),
        success: toggleLoading(false)
      },
      then: caze => {
        res()
        return setCase(caze)
      }
    }))

export const fetchCase = id => dispatch => {
  // dispatch(setCase(null));

  let caze = findCase(id);

  dispatch(
      setCase(caze)
  );
};

// Custom Case Actions

export const handleSubmit = caze => dispatch => {
  dispatch(updateCase(caze));
};

export const handleShare = (id, criticality) => ({
  type: "API",
  payload: {
    url: `/api/cases/${id}/share`,
    method: "POST",
    body: {criticality}
  },
  then: caze => setCase(caze)
});

export const makeItCorporate = id => ({
  type: "API",
  payload: {
    url: `/api/cases/${id}/escalate`
  },
  then: caze => push(`/cases/${caze.parent.id}`)
});

export const takeEdit = id => ({
  type: "API",
  payload: {
    url: `/api/cases/${id}/take`
  },
  sideActions: {
    error: setCaseLock({active: true})
  },
  then: response => dispatch => {
    if (response.success) {
      dispatch(setCaseLock({active: false}));
      //dispatch( fetchCase(id) )
    } else dispatch(setCaseLock({active: true, owner: response.owner}));
  }
});

export const releaseEdit = id => ({
  type: "API",
  payload: {
    url: `/api/cases/${id}/release`
  },
  then: caze => setCaseLock({active: false})
});

// Child Actions

export const getChildren = id => ({
  type: "API",
  payload: {
    url: `/api/cases/${id}/children`
  },
  sideActions: {
    progress: toggleLoading(true),
    error: toggleLoading(false),
    success: toggleLoading(false)
  },
  then: children => ({type: SET_CHILDREN, children, parentId: id})
});

export const setParent = (childId, parent) => dispatch =>
    new Promise((resolve, reject) =>
        dispatch({
          type: "API",
          payload: {
            url: `/api/cases/${childId}/parent`,
            method: `POST`,
            body: parent
          },
          sideActions: {
            error: () => reject()
          },
          then: child => dispatch => {
            resolve(child);
            dispatch(getChildren(parent.id));
          }
        })
    );

export const deleteParent = (childId, parentId) => ({
  type: "API",
  payload: {
    url: `/api/cases/${childId}/parent`,
    method: `DELETE`
  },
  then: child => dispatch => dispatch(getChildren(parentId))
});

// Files Actions

export const uploadFile = (id, file, type = "common", fileId) => ({
  type: "API",
  payload: {
    url: `/api/files/presign?caseId=${id}&name=${file.name}&type=${type}`
  },
  sideActions: {
    progress: toggleLoading(true),
    error: toggleLoading(false)
  },
  then: putUrl => ({
    type: "FILE",
    payload: {
      url: putUrl,
      mode: "cors",
      body: file
    },
    then: () => {
      let extension = file.name.match(/\.([^.]+$)/);
      if (extension.length < 2) {
        return "error";
      }
      return {
        type: "API",
        payload: {
          url: `/api/cases/${id}/files${fileId ? "/" + fileId : ""}`,
          method: fileId ? "PUT" : "POST",
          body: {name: file.name, extension: extension[1], type}
        },
        sideActions: {
          error: toggleLoading(false),
          success: toggleLoading(false)
        },
        then: files => ({type: UPDATE_FILES, files})
      };
    }
  })
});

export const deleteFile = (id, fileId) => ({
  type: "API",
  payload: {
    url: `/api/cases/${id}/files/${fileId}`,
    method: "DELETE"
  },
  then: files => ({type: UPDATE_FILES, files})
});

import { toggleLoading } from "state/ducks/ui"

const FETCH_MY_DEFECTS = "app/defects/FETCH_MY_DEFECTS";
const FETCH_DEFECTS_ID = "app/defects/FETCH_DEFECTS_ID";
const FETCH_GROUPINGS_ID = "app/defects/FETCH_GROUPINGS_ID";

export const ACTIONS = {FETCH_MY_DEFECTS, FETCH_DEFECTS_ID, FETCH_GROUPINGS_ID}

export default function reducer(state = {myDefects : [], defectsId:[]}, action = {}) {
  switch (action.type) {
    case FETCH_MY_DEFECTS:
      return {...state, myDefects :  action.defects};
    case FETCH_DEFECTS_ID:
      return {...state, defectsId :  action.defects};
    default:
      return state;
  }
}

export const fetchMyDefects = () => ({
    type: "API",
    payload: {
        url: `/api/defects/stats`
    },
    sideActions: {
        progress: toggleLoading(true),
        error: toggleLoading(false),
        success: toggleLoading(false),
    },
    then: defects => ({ type: FETCH_MY_DEFECTS, defects })
})

export const fetchBqvId = (search, isCorporate) => {
    let path = isCorporate ? "groupings" : "defects"
    return {
        type: "API",
        payload: {
            url: !search ? `/api/${path}/id` : `/api/${path}/id?startWith=${search}`
        },
        then: defects => ({type: FETCH_DEFECTS_ID, defects})
    }
}

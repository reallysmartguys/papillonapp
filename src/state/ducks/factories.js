const FETCH_FACTORIES = 'FETCH_FACOTRIES'


export default function reducer(state = [], action = {}) {
    switch (action.type) {
      case FETCH_FACTORIES:
        return action.factories;
      default:
        return state;
    }
  }

export const fetchFactories = () => ({
    type: "API",
    payload: {
        url: `/api/factories`
    },
    then: factories => ({ type: FETCH_FACTORIES, factories })
})

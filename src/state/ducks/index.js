import { routerReducer as router } from 'react-router-redux'
import { combineReducers } from 'redux'

import cases from "./cases"

import caze from "./caze"
import arca from "./arca"
import notifications from "./notifications"
import ui from "./ui"
import defects from "./defects"
import reco from "./reco"
import factories from './factories'

export default combineReducers({
    cases,
    caze,
    notifications,
    arca,
    router,
    ui,
    defects,
    factories,
    reco
})

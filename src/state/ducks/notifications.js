const FETCH_NOTIFICATIONS = "app/cases/FETCH_NOTIFICATIONS";

export const ACTIONS = { FETCH_NOTIFICATIONS }

export default function reducer(state = [], action = {}) {
    switch (action.type) {
        case FETCH_NOTIFICATIONS:
            return action.notifications;
        default:
            return state;
    }
}

export const fetchNotifications = () => ({
    type: "API",
    payload: {
        url: "/api/notifications"
    },
    then: notifications => ({ type: FETCH_NOTIFICATIONS, notifications : notifications || [] })
})

export const deleteNotification = notification => ({
    type: "API",
    payload: {
        url: `/api/notifications/${notification.id}`,
        method: "PUT",
        body : {...notification, read : true}
    },
    then: content => fetchNotifications()
})

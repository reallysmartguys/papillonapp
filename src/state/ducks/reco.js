import { toggleLoading } from "state/ducks/ui";
import { fetchCase } from "state/ducks/caze";

const SET_RECO = "app/reco/SET_RECO";
const SET_SENT_TO_TRUE = "app/reco/SET_SENT_TO_TRUE"
const SET_ADVICE_LIST = "app/reco/SET_ADVICE_LIST"
const SET_ADVICE = "app/reco/SET_ADVICE"
const SET_MESSAGES = "app/reco/SET_MESSAGES"

export const ACTIONS = { SET_RECO, SET_SENT_TO_TRUE, SET_ADVICE_LIST, SET_ADVICE, SET_MESSAGES };

export default function reducer(state = {}, action = {}) {
    switch (action.type) {
        case SET_RECO:
            return { ...state, data: action.reco, uid: action.uid || state.uid };
        case SET_SENT_TO_TRUE:
            return { ...state, data: { ...state.data, sent: true } };
        case SET_ADVICE_LIST:
            return { ...state, data: { ...state.data, advices : action.adviceList } };
        case SET_MESSAGES:
            return { ...state, data: { ...state.data, messages : action.messages } };
        case SET_ADVICE:
            const adviceList = [...state.data.advices]
            const adviceId = adviceList.findIndex( advice => advice.id === action.advice.id )
            adviceList.splice(adviceId,1,action.advice)
            return { ...state, data: { ...state.data, advices : adviceList } };
        default:
            return state;
    }
}

export const fetchReco = caseId => ({
    type: "API",
    payload: {
        url: `/api/cases/${caseId}/recommendation`
    },
    sideActions: {
        progress: toggleLoading(true),
        error: toggleLoading(false),
        success: toggleLoading(false)
    },
    then: reco => {
        if(reco) reco.content = JSON.parse(reco.content);
        return { type: SET_RECO, reco, uid: Date.now() };
    }
});

export const setReco = (reco, makeNew) => ({
    type: SET_RECO,
    reco,
    uid: makeNew ? Date.now() : null
});

export const postReco = (caseId, recoRaw) => dispatch => new Promise( (res, rej) =>  {
    const reco = { ...recoRaw, content: JSON.stringify(recoRaw.content) }
    dispatch({
        type: "API",
        payload: {
            url: `/api/cases/${caseId}/recommendation`,
            method: reco.id ? "PUT" : "POST",
            body: reco
        },
        sideActions: {
            progress: toggleLoading(true),
            error: toggleLoading(false),
            success: toggleLoading(false)
        },
        then: reco => {
            res(reco)
            fetchCase(caseId)
            if(reco) reco.content = JSON.parse(reco.content);
            return { type: SET_RECO, reco, uid: Date.now() };
        }
    })})

export const sendReco = (caseId, recoRaw) => async dispatch => {

    const reco = await postReco(caseId, recoRaw)(dispatch)

    dispatch( {
        type: "API",
        payload: {
                    url: `/api/recommendations/${reco.id}/send`
        },
        sideActions: {
                    progress: toggleLoading(true),
                    error: toggleLoading(false),
                    success: toggleLoading(false)
        },
        then: success => {
                return { type: success ? SET_SENT_TO_TRUE : "" }
            }
        })
    }

/* Advices actions */

export const fetchAdviceList = (recommendationId) => {
    return {
        type: "API",
        payload: {
            url: `/api/recommendations/${recommendationId}/advices`,
        },
        sideActions: {
            progress: toggleLoading(true),
            error: toggleLoading(false),
            success: toggleLoading(false)
        },
        then: adviceList => {
            return { type: SET_ADVICE_LIST, adviceList };
        }
    };
};

export const postAgreement = (recommendationId, adviceId, agree ) => {
    return {
        type: "API",
        payload: {
            url: `/api/recommendations/${recommendationId}/advices/${adviceId}`,
            method : "PATCH",
            body : {
                agree
            }
        },
        sideActions: {
            progress: toggleLoading(true),
            error: toggleLoading(false),
            success: toggleLoading(false)
        },
        then: advice => {
            return { type: SET_ADVICE, advice };
        }
    };
};

/* Messages actions */

export const fetchMessages = (recommendationId ) => {
    return {
        type: "API",
        payload: {
            url: `/api/recommendations/${recommendationId}/messages`
        },
        sideActions: {
            progress: toggleLoading(true),
            error: toggleLoading(false),
            success: toggleLoading(false)
        },
        then: messages => {
            return { type: SET_MESSAGES, messages };
        }
    };
};

export const postMessage = (recommendationId, content ) => {
    return {
        type: "API",
        payload: {
            url: `/api/recommendations/${recommendationId}/messages`,
            method : "POST",
            body : {
                content
            }
        },
        sideActions: {
            progress: toggleLoading(true),
            error: toggleLoading(false),
            success: toggleLoading(false)
        },
        then: messages => {
            return { type: SET_MESSAGES, messages };
        }
    };
};

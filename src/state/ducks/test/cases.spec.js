import reducer from "../cases"
import { Reducer } from 'redux-testkit';
import { ACTIONS } from '../cases';

const initialState = [];

describe('Cases reducer', () => {

    it('should have initial state', () => {
        expect(reducer()).toEqual(initialState);
    });

    it('should not affect state', () => {
        Reducer(reducer).expect({ type: 'NOT_EXISTING' }).toReturnState(initialState);
    });

    it('should store fetched cases', () => {
        const cases = [{ subject: "one" }, { subject: "two" }];
        const action = { type: ACTIONS.FETCH_CASES, cases };
        Reducer(reducer)
            .expect(action)
            .toReturnState(cases);
    });

    it('should store fetched cases and override existing cases', () => {
        const state = [{ subject: "three" }, { subject: "four" }]
        const cases = [{ subject: "one" }, { subject: "two" }];
        const action = { type: ACTIONS.FETCH_CASES, cases };
        Reducer(reducer)
            .withState(state)
            .expect(action)
            .toReturnState(cases);
    });

})

import reducer from "../caze"
import { Reducer } from 'redux-testkit';
import { ACTIONS } from '../caze';

const initialState = null;

describe('Case reducer', () => {

    it('should have initial state', () => {
        expect(reducer()).toEqual(initialState);
    });

    it('should not affect state', () => {
        Reducer(reducer).expect({ type: 'NOT_EXISTING' }).toReturnState(initialState);
    });

    it('should fetch a case', () => {
        const caze = { subject: "test", id: 3 }
        const action = { type: ACTIONS.FETCH_CASE, caze };
        Reducer(reducer)
            .expect(action)
            .toReturnState(caze);
    });

    it('should override a case', () => {
        const state = { subject: "wrong test", id: 2 }
        const caze = { subject: "test", id: 3 }
        const action = { type: ACTIONS.FETCH_CASE, caze };
        Reducer(reducer)
            .withState(state)
            .expect(action)
            .toReturnState(caze);
    });

    it('should override a case', () => {
        const state = { subject: "wrong test", id: 2 }
        const caze = { subject: "test", id: 3 }
        const action = { type: ACTIONS.FETCH_CASE, caze };
        Reducer(reducer)
            .withState(state)
            .expect(action)
            .toReturnState(caze);
    });

})

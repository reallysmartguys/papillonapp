import reducer from "../notifications"
import { Reducer } from 'redux-testkit';
import { ACTIONS } from '../notifications';

const initialState = [];

describe('Cases reducer', () => {

    it('should have initial state', () => {
        expect(reducer()).toEqual(initialState);
    });

    it('should not affect state', () => {
        Reducer(reducer).expect({ type: 'NOT_EXISTING' }).toReturnState(initialState);
    });

    it('should store fetched notifications', () => {
        const notifications = [{ subject: "one" }, { subject: "two" }];
        const action = { type: ACTIONS.FETCH_NOTIFICATIONS, notifications };
        Reducer(reducer)
            .expect(action)
            .toReturnState(notifications);
    });

    it('should store fetched notifications and override existing notifications', () => {
        const state = [{ subject: "three" }, { subject: "four" }]
        const notifications = [{ subject: "one" }, { subject: "two" }];
        const action = { type: ACTIONS.FETCH_NOTIFICATIONS, notifications };
        Reducer(reducer)
            .withState(state)
            .expect(action)
            .toReturnState(notifications);
    });

})

import reducer from "../ui"
import { Reducer } from 'redux-testkit';
import { ACTIONS } from '../ui';

const initialState = {
    modal: {},
    roles: ["PBU"],
    loading: false,
    caseLock: {
        active: false,
        owner: {}
    }
};

describe('Ui reducer', () => {

    it('should have initial state', () => {
        expect(reducer()).toEqual(initialState);
    });

    it('should not affect state', () => {
        Reducer(reducer).expect({ type: 'NOT_EXISTING' }).toReturnState(initialState);
    });

    it('should toggle on loading', () => {
        const action = { type: ACTIONS.TOGGLE_LOADING, loading: true };
        Reducer(reducer)
            .expect(action)
            .toReturnState({ ...initialState, loading: true });
    });

    it('should toggle off loading', () => {
        const action = { type: ACTIONS.TOGGLE_LOADING, loading: false };
        Reducer(reducer)
            .withState({ loading: true })
            .expect(action)
            .toReturnState({ loading: false });
    });

    it('should toggle on modal', () => {
        const options = { modal: { show: true, type: "newCazeModal", props: { title: "Hello" } } }
        const action = { type: ACTIONS.TOGGLE_MODAL, ...options };
        Reducer(reducer)
            .expect(action)
            .toReturnState({ ...initialState, ...options });
    });

    it('should toggle off modal without blanking it', () => {
        const state = { modal: { show: true, type: "newCazeModal", props: { title: "Hello" } } }
        const action = { type: ACTIONS.TOGGLE_MODAL, modal: false };
        Reducer(reducer)
            .withState(state)
            .expect(action)
            .toReturnState({ ...state, modal: { ...state.modal, show: false } });
    });


})

import { Reducer } from 'redux-testkit';
import reducer, { ACTIONS } from '../arca';

const initialState = {
    users: []
};

describe('Cases reducer', () => {

    it('should have initial state', () => {
        expect(reducer()).toEqual(initialState);
    });

    it('should not affect state', () => {
        Reducer(reducer).expect({ type: 'NOT_EXISTING' }).toReturnState(initialState);
    });

    it('should login', () => {
        const user = { firstname: "john", id: 3 }
        const action = { type: ACTIONS.LOGIN, user };
        Reducer(reducer)
            .expect(action)
            .toReturnState({ ...initialState, user });
    });

    it('should logout', () => {
        const state = {
            users: [],
            user: { firstname: "john", id: 3 }
        };
        const action = { type: ACTIONS.LOGOUT };
        Reducer(reducer)
            .withState(state)
            .expect(action)
            .toReturnState({ ...initialState });
    });

})

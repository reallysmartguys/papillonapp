const TOGGLE_MODAL = "app/interface/TOGGLE_MODAL";
const TOGGLE_LOADING = "app/interface/TOGGLE_LOADING";
const SET_ROLE = "app/interface/SET_ROLE"
const SET_CASE_LOCK = "app/interface/SET_LOCK"

export const ACTIONS = { TOGGLE_MODAL, TOGGLE_LOADING, SET_CASE_LOCK }

const defaultState = {
    modal : {},
    roles: ["PBU"],
    loading: false,
    caseLock : {
        active : false,
        owner : {}
    }
}

export default function reducer(state = defaultState, action = {}) {
    let { type, modal = {}, loading, roles, caseLock } = action
    if(modal === false || modal.show === false) modal = {...state.modal, show : false}
    switch (type) {
        case TOGGLE_MODAL:
            return { ...state, modal };
        case TOGGLE_LOADING:
            return { ...state, loading };
        case SET_ROLE:
            return { ...state, roles };
        case SET_CASE_LOCK:
            return { ...state, caseLock : {...caseLock, uid : Date.now()} };
        default:
            return state;
    }
}

export const toggleModal = (modal) => ({ type: TOGGLE_MODAL, modal })

var loadingRequests = 0

export const toggleLoading = (loading) => {
    if (loading === true) {
        loadingRequests++
        return { type: TOGGLE_LOADING, loading }
    }
    if (loading === false) {
        loadingRequests--
        if (loadingRequests < 0) loadingRequests = 0
        if(loadingRequests === 0 ) return { type: TOGGLE_LOADING, loading }
    }
}

export const setRole = (roles) => ({ type: SET_ROLE, roles })

export const setCaseLock= caseLock => ({type : SET_CASE_LOCK, caseLock})

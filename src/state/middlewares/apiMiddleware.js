import { push } from "react-router-redux"

export default store => next => async action => {

    if (action.type === 'API') {
        let { url, method = "GET", body, headers } = action.payload
        const {sideActions = {}} = action

        // fetch setup
       // const {arca} = store.getState()
        let reqHeaders = new Headers();
        if (headers) {
            for (let key in headers) {
                reqHeaders.append(key, headers[key])
            }
        }
        reqHeaders.append('Content-Type', 'application/json');
        //if (arca.user) reqHeaders.append('token', arca.user.id || -1);

        // Sending

        // Progress action
        sideActions.progress && store.dispatch(sideActions.progress)

        // Response
        body = typeof body === "object" ? JSON.stringify(body) : body
        let response

        try {
            response = await fetch(url, { method, headers: reqHeaders, body });
        }catch(err){
            response = {ok : false}
        }

        // 401 logout
        if (response.status === 401) store.dispatch(push('/logout'))

        if (response.status === 404){
            // Ressource is not available
            // Success action
            sideActions.success && store.dispatch(sideActions.success)
            // execute then when request is successful
            if (action.then) store.dispatch(action.then(null))
        }

        let content;

        // Response is ok
        if (response.ok) {
            let contentType = response.headers.get("Content-Type");

            try {
                if (contentType && contentType.indexOf("json") !== -1) {
                    content = await response.json();
                } else {
                    content = await response.text();
                }
            } catch (err) {
                console.error(err)
                content = null
            }

            // Success action
            sideActions.success && store.dispatch(sideActions.success)

            // execute then when request is successful
            if (action.then) store.dispatch(action.then(content))

        } else {
            console.error(`Tryied to ${method}, got a ${response.status}`)
            content = null
            // Error action
            sideActions.error && store.dispatch(sideActions.error)
        }

    }

    else next(action)
};

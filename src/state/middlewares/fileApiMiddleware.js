export default store => next => async action => {

    if (action.type === 'FILE') {
        let { url, method = "PUT", body } = action.payload
        let response = await fetch(url, { method, body });
        await response.status;
        if (action.then) store.dispatch(action.then())
    }

    else next(action)
}

import {applyMiddleware, compose, createStore} from 'redux'
import { routerMiddleware } from 'react-router-redux'
import {createBrowserHistory } from 'history';

import apiMiddleware from "state/middlewares/apiMiddleware"
import fileApiMiddleware from "state/middlewares/fileApiMiddleware"

import thunk from 'redux-thunk'
import rootReducer from "state/ducks";

export const history = createBrowserHistory();

//const middleware = [fileApiMiddleware, apiMiddleware, thunk, routerMiddleware(history)]
const middleware = [thunk, routerMiddleware(history)]

const composeEnhancers =
    process.env.NODE_ENV !== 'production' &&
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ :
        compose;

export default function configureStore(initialState){
    let store = createStore(
        rootReducer,
        initialState,
        composeEnhancers(applyMiddleware(...middleware))
    );

    if(process.env.NODE_ENV === 'development' && module.hot){
        module.hot.accept("./ducks", () =>
            store.replaceReducer(rootReducer)
        );
    }

    return store;
}

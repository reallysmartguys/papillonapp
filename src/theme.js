import { createMuiTheme } from 'material-ui/styles';

export default createMuiTheme({
    palette: {
        type: 'light',
    },
    typography: {
        fontFamily:
        '"Read", sans-serif'
    }
});

import moment from "moment"

export const formatName = (user, showRole) => {
    if (!user) return null
    const {firstname = "john", lastname="doe", roles} = user
    return firstname.toUpperCase().slice(0,1) +
    ". " +
    lastname.toUpperCase() +
    ( showRole ? ` (${roles.join(' | ')})` : '' )
}

export const formatDate = (date, format = "DD/MM/YYYY") => {
    return date ? moment.utc(date).local().format(format) : null
}
